<?php
/**
 * Debug [插件开发/调试]
 * 
 * @author edikud
 * @package Widget
 * @version 1.2.0
 * @copyright Copyright (c) 2020 F3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */
 
namespace Plugin\Debug;

use W3\Config;
use W3\Request;
use W3\Plugins;
use W3\Html;

class Plugin implements Plugins
{
	
    /**
     * 执行应用程序
     * @access public
     * @return void 
     */
    public static function execute()
    {
		
		Config::instance()->debug = \plugin('Debug')->debug;

        add_action("admin.init", function ($widget) 
		{
		    $widget->addMenu('debug', 'Debug', '<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-tool"><path d="M14.7 6.3a1 1 0 0 0 0 1.4l1.6 1.6a1 1 0 0 0 1.4 0l3.77-3.77a6 6 0 0 1-7.94 7.94l-6.91 6.91a2.12 2.12 0 0 1-3-3l6.91-6.91a6 6 0 0 1 7.94-7.94l-3.76 3.76z"></path></svg>');
		    $widget->addPanel('debug', 'Debug', 'Debug', $widget->adminUrl('plugin/config', 'name=Debug2&c=通过2', false), 'admin' );
        });	

        add_action(['admin.start.after', 'start.after'], function ($widget) 
		{
			Plugin::show();
        });	

    }
	
    /**
     * 激活插件方法,如果激活失败,直接抛出异常
     * 
     * @access public
     * @return void
     * @throws Plugins_Exception
     */
    public static function activate(){
		
		return __('插件已经激活，请正确设置插件');
	}
    
    /**
     * 禁用插件方法,如果禁用失败,直接抛出异常
     * 
     * @static
     * @access public
     * @return void
     * @throws Plugins_Exception
     */
    public static function deactivate(){}
    
    /**
     * 获取插件配置面板
     * 
     * @access public
     * @param Form $form 配置面板
     * @return void
     */
    public static function config(\W3\Element\Form $form)
    {
        $debug= Html::radio('debug',
            array( '0' => __('线上模式'),
                   '1' => __('调试模式')
                ),
            0, __('Debug'));
        $form->addInput($debug);

	}
    
    /**
     * 插件实现方法
     * 
     * @access public
     * @return void
     */
    public static function show()
    {
		if(!Request::instance()->isGet() || \plugin('Debug')->debug==0) return ;
	$runtime = number_format(microtime(1) - Config::instance()->startTime, 4);

echo '<style type="text/css">
#cola_trace_win{display:none;z-index:999;position:fixed;left:1%;bottom:10px;width:98%;min-width:300px;border-radius:5px;box-shadow:-2px 2px 20px #555;background:#fff;border:1px solid #ccc}
#cola_trace_win,#cola_trace_win,#cola_trace_win div,#cola_trace_win h6,#cola_trace_win ol,#cola_trace_win li{margin:0;padding:0;font:14px/1.6 \'Microsoft YaHei\',Verdana,Arial,sans-serif}
#cola_trace_open{display:none;z-index:999;position:fixed;right:5px;bottom:5px;width:80px;height:24px;line-height:24px;text-align:center;border:1px solid #ccc;border-radius:5px;background:#eee;cursor:pointer;box-shadow:0 0 12px #555}
#cola_trace_size,#cola_trace_close{float:right;display:inline;margin:3px 5px 0 0!important;border:1px solid #ccc;border-radius:5px;background:#eee;width:24px;height:24px;line-height:24px;text-align:center;cursor:pointer}
#cola_trace_title{height:32px;overflow:hidden;padding:0 3px;border-bottom:1px solid #ccc}
#cola_trace_title h6{float:left;display:inline;width:100px;height:32px;line-height:32px;font-size:16px;font-weight:700;text-align:center;color:#999;cursor:pointer;text-shadow:1px 1px 0 #F2F2F2}
#cola_trace_cont{width:100%;height:240px;overflow:auto}
#cola_trace_cont ol{list-style:none;padding:5px;overflow:hidden;word-break:break-all}
#cola_trace_cont ol.ktun{display:none}
#cola_trace_cont ol li{padding:0 3px}
#cola_trace_cont ol li span{float:left;display:inline;width:70px}
#cola_trace_cont ol li.even{background:#ddd}
</style>
<div id="cola_trace_open">' . $runtime .'</div>
<div id="cola_trace_win">
	<div id="cola_trace_title">
		<div id="cola_trace_close">关</div>
		<div id="cola_trace_size">大</div>
		<h6 style="color:#000">基本信息</h6>
		<h6>SQL</h6>
		<h6>$_GET</h6>
		<h6>$_POST</h6>
		<h6>$_COOKIE</h6>
		<h6>包含文件</h6>
	</div>
	<div id="cola_trace_cont">
		<ol>
			<li><span>当前页面:</span> '. Request::instance()->root() .'</li>
			<li><span>当前时间:</span> '. date('Y-m-d H:i:s', \W3\Timer::instance()->time()) .'</li>
			<li><span>当前网协:</span> '. Request::instance()->ip() .'</li>
			<li><span>请求路径:</span> '. Request::instance()->url() .'</li>
			<li><span>运行时间:</span> '. $runtime .'</li>
			<li><span>内存开销:</span> '. self::humanSize(memory_get_usage() - Config::instance()->startMemory) .'</li>
		</ol>
		<ol class="ktun">'. self::arr2str(\W3\Db::$log, 1, FALSE) .'</ol>
		<ol class="ktun">'. self::arr2str($_GET) .'</ol>
		<ol class="ktun" style="white-space:pre">'. self::arr2str(\W3\Util::safeHtml($_POST), 1) .'</ol>
		<ol class="ktun">'. self::arr2str($_COOKIE) .'</ol>
		<ol class="ktun">'. self::arr2str(get_included_files(), 1) .'</ol>
	</div>
</div>
<script type="text/javascript">
(function(){
var isIE = !!window.ActiveXObject;
var isIE6 = window.VBArray && !window.XMLHttpRequest;
var isQuirks = document.compatMode == \'BackCompat\';
var isDisable = (isIE && isQuirks) || isIE6;
var win = document.getElementById(\'cola_trace_win\');
var size = document.getElementById(\'cola_trace_size\');
var open = document.getElementById(\'cola_trace_open\');
var close = document.getElementById(\'cola_trace_close\');
var cont = document.getElementById(\'cola_trace_cont\');
var tab_tit = document.getElementById(\'cola_trace_title\').getElementsByTagName(\'h6\');
var tab_cont = document.getElementById(\'cola_trace_cont\').getElementsByTagName(\'ol\');
var cookie = document.cookie.match(/cola_trace_page_show=(\d\|\d\|\d)/);
var history = (cookie && typeof cookie[1] != \'undefined\' && cookie[1].split(\'|\')) || [0,0,0];
var is_size = 0;
var set_cookie = function() {
	document.cookie = \'cola_trace_page_show=\' + history.join(\'|\');
}
open.onclick = function() {
	win.style.display=\'block\';
	this.style.display=\'none\';
	history[0] = 1;
	set_cookie();
}
close.onclick = function() {
	win.style.display = \'none\';
	open.style.display = \'block\';
	history[0] = 0;
	set_cookie();
}
size.onclick = function() {
	if(is_size == 0) {
		this.innerHTML = "小";
		//win.style.top = "10px";
		var H = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight;
		H = H > window.screen.availHeight ? window.screen.availHeight - 200 : H;
		H = H < 350 ? 350 : H;
		cont.style.height = H - 63 +"px";
		is_size = 1;
		history[1] = 1;
	}else{
		this.innerHTML = "大";
		//win.style.top = "auto";
		cont.style.height = "240px";
		is_size = 0;
		history[1] = 0;
	}
	set_cookie();
}
for(var i = 0; i < tab_tit.length; i++) {
	tab_tit[i].onclick = (function(i) {
		return function() {
			for(var j = 0; j < tab_cont.length; j++) {
				tab_cont[j].style.display = \'none\';
				tab_tit[j].style.color = \'#999\';
			}
			tab_cont[i].style.display = \'block\';
			tab_tit[i].style.color = \'#000\';
			history[2] = i;
			set_cookie();
		};
	})(i);
}
if(!isDisable) {
	open.style.display = \'block\';

	if(typeof open.click == \'function\') {
		parseInt(history[0]) && open.click();
		parseInt(history[1]) && size.click();
		tab_tit[history[2]].click();
	}else{
		parseInt(history[0]) && open.onclick();
		parseInt(history[1]) && size.onclick();
		tab_tit[history[2]].onclick();
	}
}
})();
</script>';
		

    }

    public static function humanSize($num)
    {
        if ($num > 1073741824) {
            return number_format($num / 1073741824, 2, '.', '') . 'G';
        } elseif ($num > 1048576) {
            return number_format($num / 1048576, 2, '.', '') . 'M';
        } elseif ($num > 1024) {
            return number_format($num / 1024, 2, '.', '') . 'K';
        } else {
            return $num . 'B';
        }
    }
	
	
	public static function arr2str($arr, $type = 2, $html = TRUE) {
		$s = '';
		$i = 0;
		foreach($arr as $k => $v) {
			switch ($type) {
				case 0:
					$k = ''; break;
				case 1:
					$k = "#$k "; break;
				default:
					$k = "#$k => ";
			}

			$i++;
			$c = $i%2 == 0 ? ' class="even"' : '';
			$html && is_string($v) && $v = htmlspecialchars($v);
			if(is_array($v) || is_object($v)) {
				$v = gettype($v);
			}
			$s .= "<li$c>$k$v</li>";
		}
		return $s;
	}
}
