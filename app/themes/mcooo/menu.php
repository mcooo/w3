<?php  ?>

<div class="cola-header">
    <div class="container">
        <div class="header-navCont">
            <a class="header-brand" href="<?php W3::siteUrl(); ?>" title="<?php $this->config->title() ?>"><img src="<?php $this->config->themeUrl('img/logo.png'); ?>" alt="<?php $this->config->title() ?>"></a>

	<div class="header-nav">
	<ul>
	    <li<?php if($this->is('index')): ?> class="current"<?php endif; ?>><a href="<?php $this->config->siteUrl(); ?>"><?php __('首页'); ?></a></li>

		<li class="header-nav-dropdown">
		<a href="javascript:;">分类</a>
		<div class="dropdown" style="display: none;">
			<div class="category-dropdown clearfix">
                <ul class="category-list">
                    <?php widget('Widget_Metas_Category_List@categories')->to($categories); ?>
                    <?php $close = 0 ; while($categories->next()): ?>
					    <?php if($close && !$categories->level): $close = 0;?></li><?php endif; ?><?php if(!$categories->level): $close = 1; ?><li<?php if($this->is('category', $categories->cid)): ?> class="current"<?php endif; ?>><?php endif; ?>
						
						<div class="category-item"><a href="<?php $categories->permalink(); ?>"><?php $categories->name(); ?></a><span><?php $categories->contentsNum(); ?></span></div>
						
                    <?php endwhile; ?>
                </ul>
			</div>
		</div>
		</li>
<?php 
widget('Widget_Page_List')->to($pages); 
while($pages->next()): ?>
<li<?php if($this->is('page', $pages->alias)): ?> class="current"<?php endif; ?>><a href="<?php $pages->permalink(); ?>" title="<?php $pages->title(); ?>"><?php $pages->title(); ?></a></li>
<?php endwhile; ?>
	</ul>
	</div>
	<div class="header-right">

	</div>
</div></div></div>
