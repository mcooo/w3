<?php  ?>
<?php $this->need('header.php'); ?>

<?php $this->need('menu.php'); ?>

<div class="container">
    <div class="row">
	    <div class="col-lg-8">
        <div class="breadCrumbs">
            <li><a class="indexHref" href="<?php $this->config->siteUrl(); ?>">首页</a>&nbsp;&gt;&nbsp;</li>
            <?php widget('Theme_Breadcrumbs', array('archive'=> $this))->parse('<li><a href="{permalink}">{name}</a>&nbsp;&gt;&nbsp;</li>'); ?>
            <?php if($this->is('single')): ?><li><a href="<?php $this->permalink(); ?>"><?php $this->title() ?></a>&nbsp;&gt;&nbsp;</li><?php endif; ?>
        </div>
<?php if ($this->have()): ?>
		
            <div class="box post">
	            <div class="box-header pl-0">
		            <div class="box-title">
					
<?php $this->siteTitle(array(
            'category'  =>  __('分类 %s 下的文章'),
            'search'    =>  __('包含关键字 %s 的文章'),
            'tag'       =>  __('标签 %s 下的文章'),
            'author'    =>  __('%s 发布的文章')
        ), '', ''); ?>
					
					</div>
		        </div>
	            <?php while($this->next()): ?>
                <div class="cell">
                    <div class="cell-left">
			            <a href="<?php $this->author->permalink(); ?>">
						    <div class="cell-img">
				            <img src="<?php $this->author->avatar(); ?>">
							</div>
				        </a>
			        </div>
                    <div class="cell-right">
				        <div class="cell-title">
					        <a href="<?php $this->permalink() ?>"><?php $this->title() ?></a>
				        </div>
                        <div class="cell-meta">
					        <a href="<?php $this->author->permalink(); ?>"><?php $this->author(); ?></a> 于 <?php $this->date(); ?> 发表
                            <div class="float-right">
			                    <a href="<?php $this->permalink() ?>#comments"><i class="icon-comment"></i> <?php $this->commentsNum('0', '1', '%d'); ?></a>
						    </div>
				        </div>
                    </div>
                </div>
	            <?php endwhile; ?>
            <?php $this->pageNav('&laquo; 前一页', '后一页 &raquo;'); ?>
            </div>
			
        <?php else: ?>
            <article class="post">
                <h2 class="post-title"><?php __('没有找到内容'); ?></h2>
            </article>
        <?php endif; ?>
			
			
	    </div>
<?php $this->need('sidebar.php'); ?>
	</div>
</div>
<?php $this->need('footer.php'); ?>
