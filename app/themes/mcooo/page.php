<?php  ?>

<?php $this->need('header.php'); ?>
<?php $this->need('menu.php'); ?>

<div class="container">
<div class="row">
	<div class="col-lg-8">
        <div class="breadCrumbs">
            <li><a class="indexHref" href="<?php $this->config->siteUrl(); ?>">首页</a>&nbsp;&gt;&nbsp;</li>
            <?php widget('Theme_Breadcrumbs', array('archive'=> $this))->parse('<li><a href="{permalink}">{name}</a>&nbsp;&gt;&nbsp;</li>'); ?>
            <?php if($this->is('single')): ?><li><a href="<?php $this->permalink(); ?>"><?php $this->title() ?></a>&nbsp;&gt;&nbsp;</li><?php endif; ?>
        </div>
	<div class="box">
		<div class="cell">
		    <a class="float-right cell-img" href="<?php $this->author->permalink(); ?>" ><img src="<?php $this->author->avatar(); ?>"></a>
			<h1><?php $this->title() ?></h1>
			<small class="text-gray">
			    <a href="<?php $this->author->permalink(); ?>"><?php $this->author(); ?></a> · <?php $this->date(); ?>
			</small>
		</div>
		<div class="article clearfix">
			<?php $this->content(); ?>
		</div>
	</div>

	<div class="box">
	<?php $this->need('comments.php'); ?>
	</div>

</div>


<?php $this->need('sidebar.php'); ?>

</div>
</div>

<?php $this->need('footer.php'); ?>
