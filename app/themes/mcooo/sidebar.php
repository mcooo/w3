<?php  ?>
<div class="col-lg-4 col-md-12">
    <?php if (!empty($this->config->sidebarBlock) && in_array('ShowRecentPost', $this->config->sidebarBlock)): ?>
    <div class="box">
	    <div class="box-header">
		    <div class="box-title"><?php __('最新文章'); ?></div>
		</div>
		<div class="p-3">
        <?php 
		
		
		F3::postRecent()->parse('<div class="cell-item"><a href="{permalink}">{title}</a></div>');
		
		Widget::postRecent()->parse('<div class="cell-item"><a href="{permalink}">{title}</a></div>');
		
		
		
		widget('Widget_Contents_Post_Recent')
            ->parse('<div class="cell-item"><a href="{permalink}">{title}</a></div>'); ?>
		</div>
    </div>
    <?php endif; ?>

    <?php if (!empty($this->config->sidebarBlock) && in_array('ShowRecentComments', $this->config->sidebarBlock)): ?>
    <div class="box">
	    <div class="box-header">
		    <div class="box-title"><?php __('最近回复'); ?></div>
		</div>
        <div class="comment-recent">
        <?php widget('Widget_Comments_Recent')->to($comments);


            Widget::commentsRecent()->to($comments)

		?>
        <?php while($comments->next()): ?>
                <div class="cell">
                    <div class="cell-left">
			            <a href="<?php $comments->author->permalink(); ?>">
						    <div class="cell-img">
				            <img src="<?php $comments->author->avatar(); ?>">
							</div>
				        </a>
			        </div>
                    <div class="cell-right">
				        <div class="cell-title">
					        <a href="<?php $comments->permalink() ?>"><?php $comments->title() ?></a>
				        </div>
                        <div class="cell-excerpt">
					        <?php $comments->excerpt(35, '...'); ?>
				        </div>
                    </div>
                </div>
        <?php endwhile; ?>
    </div>
	</div>
    <?php endif; ?>

    <?php if (!empty($this->config->sidebarBlock) && in_array('ShowOther', $this->config->sidebarBlock)): ?>
    <div class="box">
	    <div class="box-header">
		    <div class="box-title"><?php __('其它'); ?></div>
		</div>
		<div class="p-3">
            <?php if($this->auth->hasLogin()): ?>
				<div class="cell-item"><a href="<?php $this->config->ucenterUrl(); ?>"><?php __('用户中心'); ?></a></div>
                <div class="cell-item"><a href="<?php logout_url(); ?>"><?php __('退出'); ?></a></div>
            <?php else: ?>
                <div class="cell-item"><a href="<?php login_url(); ?>"><?php __('登录'); ?></a></div>
				<?php if($this->config->allowRegister): ?><div class="cell-item"><a href="<?php $this->config->registerUrl(); ?>"><?php __('注册'); ?></a></div><?php endif; ?>
            <?php endif; ?>
		</div>
    </div>
    <?php endif; ?>

</div><!-- end #sidebar -->