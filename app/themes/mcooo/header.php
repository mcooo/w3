<?php ?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="<?php theme( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="renderer" content="webkit">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title><?php _title([
            'category'  =>  __('分类 %s 下的文章'),
            'search'    =>  __('包含关键字 %s 的文章'),
            'tag'       =>  __('标签 %s 下的文章'),
            'author'    =>  __('%s 发布的文章')
        ], '', ''); ?></title>

    <link rel="stylesheet" href="<?php theme_url(); ?>bootstrap.min.css">
    <link rel="stylesheet" href="<?php theme_url(); ?>style.css">

    <?php w3header(); ?>
</head>
<body>
