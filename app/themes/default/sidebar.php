<?php  ?>
<div class="col-lg-4 col-md-12">
    <?php if (!empty($this->config->sidebar_block) && in_array('recent_Post', $this->config->sidebar_block)): ?>
    <div class="box">
	    <div class="box-header">
		    <div class="box-title"><?php _e('最新文章'); ?></div>
		</div>
		<div class="p-3">
        <?php 
		$this->widget('Contents\Rows', 'type=post')
            ->parse('<div class="cell-item"><a href="{permalink}">{title}</a></div>'); ?>
		</div>
    </div>
    <?php endif; ?>

    <?php if (!empty($this->config->sidebar_block) && in_array('recent_comments', $this->config->sidebar_block)): ?>
    <div class="box">
	    <div class="box-header">
		    <div class="box-title"><?php _e('最近回复'); ?></div>
		</div>
        <div class="comment-recent">
        <?php $this->widget('Comment\Recent')->to($comments); ?>
        <?php while($comments->next()): ?>
                <div class="cell">
                    <div class="cell-left">
			            <a href="<?php $comments->author->permalink(); ?>">
						    <div class="cell-img">
				            <img src="<?php $comments->author->avatar(); ?>">
							</div>
				        </a>
			        </div>
                    <div class="cell-right">
				        <div class="cell-title">
					        <a href="<?php $comments->permalink() ?>"><?php $comments->title() ?></a>
				        </div>
                        <div class="cell-excerpt">
					        <?php $comments->excerpt(35, '...'); ?>
				        </div>
                    </div>
                </div>
        <?php endwhile; ?>
    </div>
	</div>
    <?php endif; ?>

    <?php if (!empty($this->config->sidebar_block) && in_array('other', $this->config->sidebar_block)): ?>
    <div class="box">
	    <div class="box-header">
		    <div class="box-title"><?php _e('其它'); ?></div>
		</div>
		<div class="p-3">
            <?php if($this->auth->hasLogin()): ?>
				<div class="cell-item"><a href="<?php $this->adminUrl(); ?>"><?php _e('用户中心'); ?></a></div>
                <div class="cell-item"><a href="<?php $this->logoutUrl(); ?>"><?php _e('退出'); ?></a></div>
            <?php else: ?>
                <div class="cell-item"><a href="<?php $this->loginUrl(); ?>"><?php _e('登录'); ?></a></div>
				<?php if($this->config->allowRegister): ?><div class="cell-item"><a href="<?php $this->registerUrl(); ?>"><?php _e('注册'); ?></a></div><?php endif; ?>
            <?php endif; ?>
		</div>
    </div>
    <?php endif; ?>

</div><!-- end #sidebar -->