<?php  ?>

<div id="comments">
    <?php $this->comments()->to($comments); ?>
    <?php if ($comments->have()): ?>
	
<div class="box-header">
		    <div class="box-title"><?php $this->commentsNum(__('暂无评论'), __('仅有一条评论'), __('已有 %d 条评论')); ?></div>
		</div>
    
    <?php $comments->listComments(['before' =>  '<ol class="comment-list mmm">']); ?>

    <?php $comments->pagination(); ?>
    
    <?php endif; ?>

    <?php if($this->allow('comment')): ?>
    <div id="<?php $this->respondId(); ?>" class="respond">
    	<h3 id="response"><?php __('添加新评论'); ?></h3>
    	<form method="post" action="<?php $this->commentUrl() ?>" id="comment-form" role="form">
            <?php if($this->auth->hasLogin()): ?>
    		<div class="comment-author"><?php __('登录身份: '); ?><a href="<?php $this->adminUrl(); ?>"><?php $this->auth->nickName(); ?></a>. <a href="<?php $this->logoutUrl(); ?>" title="Logout"><?php __('退出'); ?> &raquo;</a>
			</div>
            <?php endif; ?>
    		<div class="comment-textarea">
                <textarea name="text" id="textarea" class="textarea" required ></textarea>
            </div>
            <div class="comment-bar">
                <button type="submit" class="submit"><?php __('提交评论'); ?></button>
				<?php $comments->cancelReply(); ?>
            </div>
    	</form>
    </div>
    <?php else: ?>
    <h3><?php __('评论已关闭'); ?></h3>
    <?php endif; ?>
</div>
