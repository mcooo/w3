﻿
;(function($, window, document, undefined) {
	var pluginName = 'menu';
	var defaults = {
		item     :   null,
        cont     :   null,
		init: function() {},
		enter: function() {},
		leave: function() {}
			
	};
	var menu = function(element, config) {
		    this.config = $.extend({}, defaults, config);
			this.$menu = $(element);
			this.$item = $(this.config.item, this.$menu);
			this.init();
			return true
		};
	menu.prototype = {
		init: function() {
			var t = this, c = this.config;
            this.$item.on("mouseenter", function() {
                t.enter($(this).find(c.cont))
            }).on("mouseleave", function() {
                t.leave($(this).find(c.cont))
            }),

			this.config.init();
		},
        enter: function(e) {
            var t = this, n = e.data("openTimer"), h = e.css('height', 'auto').outerHeight(!0);
            clearTimeout(n);
            var a = e.data("closeTimer");
            clearTimeout(a),
            e.css('height', 0).show(),
			this.config.enter(e);
            setTimeout(function() {
				e.css('height', h);
            }, 100)
        },
        leave: function(e) {
            var t = this, n = e.data("openTimer");
			this.config.leave(e);
            clearTimeout(n),
            n = setTimeout(function() {
				e.css('height', 0);
                var t = e.data("closeTimer");
                clearTimeout(t),
                t = setTimeout(function() {
                    e.hide();
                }, 100),
                e.data("closeTimer", t)
            }, 100),
            e.data("openTimer", n)
        }

	}
	$.fn[pluginName] = function(options) {
		var args = arguments;
		if (options === undefined || typeof options === 'object') {
			return this.each(function() {
				if (!$.data(this, 'plugin_' + pluginName)) {
					$.data(this, 'plugin_' + pluginName, new menu(this, options))
				}
			})
		} else if (typeof options === 'string' && options[0] !== '_' && options !== 'init') {
			var returns;
			this.each(function() {
				var instance = $.data(this, 'plugin_' + pluginName);
				if (instance instanceof menu && typeof instance[options] === 'function') {
					returns = instance[options].apply(instance, Array.prototype.slice.call(args, 1))
				}
			});
			return returns !== undefined ? returns : this
		}
	}
})(jQuery, window, document);


        $('.cola-header').menu({
		    item     :   '.header-nav-dropdown',
            cont     :   '.dropdown',
			enter: function() {
				
				$(".header-navCont", this.$menu).addClass("hoverItem");
			},
			leave: function() {
				
				$(".header-navCont", this.$menu).removeClass("hoverItem");
			},
			
        });