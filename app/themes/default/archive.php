<?php
/**
 * 这是 F3 系统的一套皮肤
 * 
 * @package mcooo
 * @author edikud
 * @version 1.2
 * @link http://mcooo.com
 */

?>

<?php $this->template('header'); ?>
<?php $this->template('menu'); ?>

<div class="container">
    <div class="row">
	    <div class="col-lg-8">
            <div class="box post">
	            <div class="box-header pl-0">
		            <div class="box-title"><?php _e('最热文章'); ?></div>
		        </div>
	            <?php while($this->next()): ?>
                <div class="cell">
                    <div class="cell-left">
			            <a href="<?php $this->author->permalink(); ?>">
						    <div class="cell-img">
				            <img src="<?php $this->author->avatar(); ?>">
							</div>
				        </a>
			        </div>
                    <div class="cell-right">
				        <div class="cell-title">
					        <a href="<?php $this->permalink() ?>"><?php $this->title() ?></a>
				        </div>
                        <div class="cell-meta">
					        <a href="<?php $this->author->permalink(); ?>"><?php $this->author->name(); ?></a> 于 <?php $this->date(); ?> 发表
                            <div class="float-right">
			                    <a href="<?php $this->permalink() ?>#comments"><i class="icon-comment"></i> <?php $this->commentsNum('0', '1', '%d'); ?></a>
						    </div>
				        </div>
                    </div>
                </div>
	            <?php endwhile; ?>
            <?php $this->pagination(); ?>
            </div>
	    </div>
<?php $this->template('sidebar'); ?>
	</div>
</div>

<?php $this->template('footer'); ?>
