<?php 

function themeConfig($form) {
    $logoUrl = Html::text('logoUrl')
		    ->label(__('站点 LOGO 地址'))
		    ->description(__('在这里填入一个图片 URL 地址, 以在网站标题前加上一个 LOGO'));
    $form->addInput($logoUrl);

    $categoryIgnore = Html::text('category_ignore')
		    ->label(__('分类mid, 逗号分隔'))
		    ->description(__('忽略某个分类(注意：包括子分类)'));
	
    $form->addInput($categoryIgnore);

    $sidebarBlock = Html::checkbox('sidebar_block', 
    array('recent_Post' => __('显示最新文章'),
    'recent_comments' => __('显示最近回复'),
    'category' => __('显示分类'),
    'archive' => __('显示归档'),
    'other' => __('显示其它杂项')),
    array('recent_Post', 'recent_comments', 'category', 'archive', 'other'))->label(__('侧边栏显示'));
    
    $form->addInput($sidebarBlock);
	
	
    $copyright = Html::textarea('copyright')
		    ->label(__('版权声明'))
		    ->description(__('页脚的版权声明, 允许使用html标签'));
	
    $form->addInput($copyright);

    $beiAnCode = Html::text('beian_code')
		    ->label(__('备案号'))
		    ->description(__('页脚备案号'));
    $form->addInput($beiAnCode);

    $tongJiJs = Html::textarea('tong_ji_js')
		    ->label(__('网站统计Js代码'))
		    ->description(__('直接填入统计代码即可'));
    $form->addInput($tongJiJs);

    $advertisingJs = Html::textarea('advertising_js')
		    ->label(__('广告JS代码'))
		    ->description(__('请填入包括script标签的代码'));
    $form->addInput($advertisingJs);
}


    \add_action('comment.rows', function ($widget) 
	{
        \add_action('call@order2', function ($widget) 
		{
            echo $widget->levels ? $widget->order : $widget->sequence + ($widget->parameter->page ? $widget->parameter->pageSize * ($widget->parameter->page - 1) : 0);
        });	
    });	



//($comments->sequence * $comments->parameter->page);



function threadedComments2($comments) {
        $commentClass = '';
        if ($comments->uid) {
            if ($comments->uid == $comments->ownerId) {
                $commentClass .= ' comment-by-author';
            } else {
                $commentClass .= ' comment-by-user';
            }
        }

?><li id="<?php $comments->theId(); ?>" class="comment-body<?php
    if ($comments->levels > 0) {
        echo ' comment-child';
        $comments->levelsAlt(' comment-level-odd', ' comment-level-even');
    } else {
        echo ' comment-parent';
    }
    $comments->alt(' comment-odd', ' comment-even');
    echo $commentClass;
?>">

<div class="author"><div class="v-tooltip-content"><a href="<?php $comments->permalink(); ?>" class="comment-avatar"><img src="<?php $comments->author->avatar(); ?>"></a></div><div class="info"><a href="<?php $comments->permalink(); ?>" class="name"><?php $comments->author->name(); ?></a><div class="meta"><span><?php $comments->order2(); ?>楼 · <?php //$comments->date($comments->vars->commentDateFormat); ?></span></div></div></div>

    <div class="comment-content" >
    <?php $comments->content(); ?>
    </div>
    <div class="comment-reply">
        <?php $comments->reply(); ?>
    </div>
    <?php if ($comments->children) { ?>
    <div class="comment-children">
        <?php $comments->threadedComments(); ?>
    </div>
    <?php } ?>
</li>
<?php } ?>