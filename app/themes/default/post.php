<?php  ?>
<?php $this->template('header'); ?>
<?php $this->template('menu'); ?>

<div class="container">
<div class="row">
	<div class="col-lg-8">
        <div class="breadCrumbs">
            <li><a class="indexHref" href="<?php $this->config( 'siteUrl' ); ?>">首页</a>&nbsp;&gt;&nbsp;</li>
            <?php //widget('Theme_Breadcrumbs', array('archive'=> $this))->parse('<li><a href="{permalink}">{name}</a>&nbsp;&gt;&nbsp;</li>'); ?>
			

            <?php if($this->is('single')): ?><li><a href="<?php $this->permalink(); ?>"><?php $this->title() ?></a>&nbsp;&gt;&nbsp;</li><?php endif; ?>
        </div>
	<div class="box">
		<div class="cell">
		    <a class="float-right cell-img" href="<?php $this->author->permalink(); ?>" ><img src="<?php $this->author->avatar(); ?>"></a>
			<h1><?php $this->title() ?></h1>
			<small class="text-gray">
			    <a href="<?php $this->author->permalink(); ?>"><?php $this->author->name(); ?></a> · <?php $this->date(); ?>
			</small>
		</div>
		<div class="article clearfix">
			<?php $this->content(); ?>
		</div>
	</div>

	<div class="box">
	<?php $this->template('comments'); ?>
	</div>

    <ul class="post-near">
        <li>上一篇: <?php $this->thePrev('%s', '没有了'); ?></li>
        <li>下一篇: <?php $this->theNext('%s', '没有了'); ?></li>
    </ul>

</div>


<?php //$this->_view('sidebar.php'); ?>

</div>
</div>

<?php $this->template('footer'); ?>
