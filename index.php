﻿<?php

/** 载入配置支持 */
if (!defined('W3_ROOT_DIR') && !@include_once 'config.inc.php') {
    file_exists('./install.php') ? header('Location: install.php') : print('Missing Config File');
    exit;
}

/** 开启 22*/
\W3\Base::widget('W3\Start');