<?php

namespace Manager;

use Base\Attachs as Manager;

!defined('W3_ROOT_DIR') AND exit;

/**
 * 附件编辑管理基类
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */

class Attachs extends Manager
{
    /**
     * 插入附件
     *
     * @access public
     * @param array $attachs 附件结构数组
     * @return integer
     */
    public function addAttachs(array $values)
    {
        /** 构建插入结构 */
        $attach = [
			'uid'           =>  isset($values['uid']) ? intval($values['uid']) : $this->auth->uid,
            'id'            =>  empty($values['id']) ? 0 : intval($values['id']),
            'type'          =>  empty($values['type']) ? 'post' : $values['type'],
            'name'          =>  empty($values['name']) ? '' : $values['name'],
            'ext'           =>  empty($values['ext']) ? '' : $values['ext'],			
            'size'          =>  empty($values['size']) ? 0 : intval($values['size']),	
            'path'          =>  empty($values['path']) ? '' : $values['path'],			
            'created'       =>  empty($values['created']) ? $this->config->time : $values['created'],
            'isImage'       =>  !empty($values['isImage']) && 1 == $values['isImage'] ? 1 : 0,
        ];

        if (!empty($values['aid'])) {
            $attach['aid'] = $values['aid'];
        }

        return $this->db
		    ->insert('table.attachs')
		    ->rows($attach)
			->lastId();
    }

    /**
     * 更新附件
     *
     * @access public
     * @param array $aid 附件aid
     * @param array $attachs 附件结构数组
     * @return integer
     */
    public function updateAttachs($aid, array $values)
    {
        /** 构建插入结构 */
        $update = [
			'uid'           =>  isset($values['uid']) ? intval($values['uid']) : $this->auth->uid,
            'id'            =>  empty($values['id']) ? 0 : intval($values['id']),
            'type'          =>  empty($values['type']) ? 'post' : $values['type'],
            'name'          =>  empty($values['name']) ? '' : $values['name'],
            'ext'           =>  empty($values['ext']) ? '' : $values['ext'],			
            'size'          =>  empty($values['size']) ? 0 : intval($values['size']),	
            'path'          =>  empty($values['path']) ? '' : $values['path'],			
            'created'       =>  empty($values['created']) ? $this->config->time : $values['created'],
            'isImage'       =>  !empty($values['isImage']) && 1 == $values['isImage'] ? 1 : 0,
        ];
		
        $attach = [];
        foreach ($values as $key => $val) 
		{
            if (array_key_exists($key, $update)) {
                $attach[$key] = $update[$key];
            }
        }

        return $this->db
		    ->update('table.attachs')
		    ->where('aid = ?', $aid)
		    ->rows($attach)
		    ->affected();
    }

    /**
     * 删除附件
     *
     * @access public
     * @param int $aid 附件aid
     * @return integer
     */
    public function deleteAttachs($aid)
    {
		return $this->db
		    ->delete('table.attachs')
		    ->where('aid = ?', $aid)
			->affected();
    }

}

