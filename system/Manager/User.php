<?php

namespace Manager;

use W3\Html;

!defined('W3_ROOT_DIR') AND exit;

/**
 * 用户编辑管理基类
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */
class User extends Users
{
    /**
     * 增加用户
     *
     * @access public
     * @param array $values 用户结构数组
     * @return integer
     */
    public function addUser(array $values)
    {
        /** 插入数据 */
		return $this->addUsers($values);
    }

    /**
     * 更新用户
     *
     * @access public
     * @param int $uid 用户uid
     * @param array $values 用户结构数组
     * @return integer
     */
    public function updateUser($uid, array $values)
    {
        /** 更新数据 */
        return $this->updateUsers($uid, $values);
    }

    /**
     * 删除用户
     *
     * @access public
     * @param int $uid 用户uid
     * @return integer
     */
    public function deleteUser($uid)
    {
		$deleteRows = false;
		
        $user = $this->db
		    ->select('table.users.uid')
		    ->from('table.users')
			->where('table.users.uid = ?', $uid)
			->limit(1)
		    ->fetch();

        $masterUserId = $this->db
			->select('MIN(uid) as masterUserId')
		    ->from('table.users')
			->Object()
			->masterUserId;

        if (empty($user) || $masterUserId == $uid || $uid == $this->auth->uid)
			
			return ;

		$max = 0;
		
        while (1) 
		{
            $arrlist = $this->db
				->select('table.contents.cid', 'table.contents.type')
				->from('table.contents')
			    ->where('table.contents.uid = ? AND table.contents.cid > ?', $uid, $max)
				->limit(1000)
				->order('table.contents.uid', 'ASC')
				->get();
				
            if (empty($arrlist)) {
                break;
            }

            foreach ($arrlist as $value) 
			{
				$value['cid'] > $max && $max = $value['cid'];
		        'post' == $value['type'] && $this->widget('Manager\Post')->deletePost($value['cid']);
		        'page' == $value['type'] && $this->widget('Manager\Page')->deletePage($value['cid']);
		    }
        }	
			
        $deleteRows = $this->deleteUsers($uid);
			
		return $deleteRows;
    }
	
    /**
     * 生成表单
     *
     * @access public
     * @param string $action 表单动作
     * @return Form
     */
    public function form()
    {
        /** 构建表格 */
        $form = Html::form();

        /** 用户名称 */
        $name = Html::text('name')
		    ->label(__('用户名') . ' *')
		    ->description(__('此用户名将作为用户登录时所用的名称.'). '<br />' . __('请不要与系统中现有的用户名重复.'));
        $form->addInput($name);

        /** 电子邮箱地址 */
        $mail = Html::text('mail')
		    ->label(__('电子邮箱地址') . ' *')
		    ->description(__('电子邮箱地址将作为此用户的主要联系方式.') . '<br />' . __('请不要与系统中现有的电子邮箱地址重复.'));
        $form->addInput($mail);

        /** 用户昵称 */
        $nickName = Html::text('nickName')
		    ->label(__('用户昵称'))
		    ->description(__('用户昵称可以与用户名不同, 用于前台显示.') . '<br />' . __('如果你将此项留空, 将默认使用用户名.'));
        $form->addInput($nickName);

        /** 用户密码 */
        $password = Html::text('password')
		    ->type('password')
		    ->label(__('用户密码'))
		    ->description(__('为此用户分配一个密码.') . '<br />' . __('建议使用特殊字符与字母、数字的混编样式,以增加系统安全性.'));
        $form->addInput($password);

        /** 用户密码确认 */
        $confirm = Html::text('confirm')
		    ->type('password')
		    ->label(__('用户密码确认'))
		    ->description(__('请确认你的密码, 与上面输入的密码保持一致.'));
        $form->addInput($confirm);

        /** 用户组 */
        $group = Html::select('group', $this->auth->groupNames)
		    ->label(__('用户组'))
		    ->description(__('不同的用户组拥有不同的权限.'));
        $form->addInput($group);

        /** 个人主页 */
        $url = Html::text('url')->label(__('个人主页'));
        $form->addInput($url);

        /** 签名 */
        $sign = Html::textarea('sign')
		    ->label(__('签名'))
			->style('min-height: 80px');
        $form->addInput($sign);

        /** 简介 */
        $intro = Html::textarea('intro')
		    ->label(__('简介'))
			->style('min-height: 120px');
        $form->addInput($intro);

        /** 用户主键 */
        $uid = Html::hidden('uid');
        $form->addInput($uid);

        /** 提交按钮 */
        $submit = Html::submit();
        $form->set($submit);

        if ($this->uid) {
            $submit->value(__('编辑用户'));
            $name->value($this->name);
            $nickName->value($this->nickName);
            $mail->value($this->mail);
            $group->value($this->group);
            $uid->value($this->uid);
            $url->value($this->url);
            $sign->value($this->sign);
            $intro->value($this->intro);
        } else {
            $submit->value(__('增加用户'));
        }

        # 判断用户nickName是否存在
		$form->register('nickNameExists', function ($nickName, $uid){
            $select = $this->db
			    ->select('uid')
		        ->from('table.users')
			    ->where('table.users.nickName = ?', $nickName)
			    ->fetch();
			
			return empty($select) || $uid && $uid == $select['uid'];
        });

        // 判断用户mail是否存在
		$form->register('mailExists', function ($mail, $uid){
            $select = $this->db
			    ->select('uid')
		        ->from('table.users')
			    ->where('table.users.mail = ?', $mail)
			    ->fetch();
				
			return empty($select) || $uid && $uid == $select['uid'];
        });

        // 判断用户name是否存在
		$form->register('nameExists', function ($name){
            return !$this->db
			    ->select(1)
		        ->from('table.users')
                ->where('table.users.name = ?', $name)
			    ->count();
        });

        // 判断用户是否存在
		$form->register('userExists', function ($uid){
            return $this->db
			    ->select(1)
		        ->from('table.users')
                ->where('table.users.uid = ?', $uid)
			    ->count();
        });


        /** 给表单增加规则 */
        $nickName->addRule('nickNameExists', __('昵称已经存在'), $form->getParam('uid'));
        $nickName->addRule('xss', __('请不要在昵称中使用特殊字符'));
        $mail->addRule('required', __('必须填写电子邮箱'));
        $mail->addRule('mailExists', __('电子邮箱地址已经存在'), $form->getParam('uid'));
        $mail->addRule('email', __('电子邮箱格式错误'));
        $password->addRule('min', __('为了保证账户安全, 请输入至少六位的密码'), 6);
        $confirm->addRule('confirm', __('两次输入的密码不一致'), 'password');
        $url->addRule('url', __('个人主页地址格式错误'));
        $sign->addRule('xss', __('请不要在签名中使用特殊字符'));
        $intro->addRule('xss', __('请不要在简介中使用特殊字符'));
		
		/** 增加用户 */
        if (!$this->uid) {
            $name->addRule('required', __('必须填写用户名称'));
            $name->addRule('xss', __('请不要在用户名中使用特殊字符'));
            $name->addRule('nameExists', __('用户名已经存在'));
            $password->label(__('用户密码') . ' *');
            $confirm->label(__('用户密码确认') . ' *');
            $password->addRule('required', __('必须填写密码'));
        }

		/** 更新用户 */
        if ($this->uid) {
            $name->disabled();
            $uid->addRule('required', __('用户主键不存在'));
            $uid->addRule('userExists', __('用户不存在'));
        }
		
		/** 插件接口 */
        return \do_action('user_form', $this, false, $form);
    }
	
    /**
     * 生成表单
     *
     * @access public
     * @return Form
     */
    public function loginForm()
    {
		
        /** 构建表格 */
        $form = Html::form();
		
        /** 用户昵称 */
        $name = Html::text('name')
		    ->label(__('用户名') . ' *');
        $form->addInput($name);

        /** 密码 */
        $password = Html::password('password')
		    ->label(__('密码') . ' *');
        $form->addInput($password);

        /** 提交按钮 */
        $submit = Html::submit(__('登录'));
        $form->set($submit);

        /** 给表单增加规则 */
        $name->addRule('required', __('请输入用户名或邮箱.'));
        $password->addRule('required', __('请输入密码'));
		
		/** 插件接口 */
        return \do_action('login_form', $this, false, $form);
    }
	
    /**
     * 生成表单
     *
     * @access public
     * @return Form
     */
    public function registerForm()
    {
        /** 构建表格 */
        $form = Html::form();
		
        /** 用户昵称 */
        $name = Html::text('name')
		    ->label(__('用户名') . ' *');
        $form->addInput($name);

        /** 密码 */
        $mail = Html::text('mail')
		    ->label(__('邮箱') . ' *');
        $form->addInput($mail);

        /** 密码 */
        $password = Html::password('password')
		    ->label(__('密码') . ' *');
        $form->addInput($password);
		
        /** 提交按钮 */
        $submit = Html::submit(__('注册'));
        $form->set($submit);

        // 判断用户mail是否存在
		$form->register('mailExists', function ($mail){
            $select = $this->db
			    ->select(1)
		        ->from('table.users')
			    ->where('table.users.mail = ?', $mail)
			    ->fetch();

            return !$select;
        });

        // 判断用户name是否存在
		$form->register('nameExists', function ($name){
            return !$this->db
			    ->select(1)
		        ->from('table.users')
                ->where('table.users.name = ?', $name)
			    ->count();
        });

        /** 给表单增加规则 */
        $name->addRule('required', __('必须填写用户名称'));
        $name->addRule('min', __('用户名至少包含2个字符'), 2);
        $name->addRule('max', __('用户名最多包含32个字符'), 32);
        $name->addRule('xss', __('请不要在用户名中使用特殊字符'));
        $name->addRule('nameExists', __('用户名已经存在'));
		
        $mail->addRule('required', __('必须填写电子邮箱'));
        $mail->addRule('mailExists', __('电子邮箱地址已经存在'));
        $mail->addRule('email', __('电子邮箱格式错误'));
        $mail->addRule('max', __('电子邮箱最多包含64个字符'), 64);
		
        $password->addRule('required', __('必须填写密码'));
        $password->addRule('min', __('为了保证账户安全, 请输入至少六位的密码'), 6);
        $password->addRule('max', __('为了便于记忆, 密码长度请不要超过十八位'), 18);
        //$password->addRule('confirm', __('两次输入的密码不一致'), 'password');
		
		/** 插件接口 */
        return \do_action('register_form', $this, false, $form);
    }
	
    /**
     * 仅仅输出域名和路径
     *
     * @return string
     */
    protected function ___domainPath(): string
    {
        $parts = parse_url($this->url);
        return ($parts['host'] ?? '') . ($parts['path'] ?? null);
    }

}
