<?php

namespace Manager;

use W3\Util;
use W3\Html;

!defined('W3_ROOT_DIR') AND exit;

/**
 * 内容编辑管理基类
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */

class Page extends Contents
{
    /**
     * 插入内容
     *
     * @access public
     * @param array $values 内容结构数组
     * @return integer
     */
    public function addPage(array $values)
    {
		/** 计算图片数，和非图片文件数 */
        $numArr = $this->widget('Manager\Attach')->findNums('page', 0, $this->auth->uid);
		
		if(!empty($numArr)) { 
		    $values['imagesNum'] = $numArr[0];
			$values['filesNum'] = $numArr[1];
		}
		
        /** 发布一个新内容 */
        $insertId = $this->addContents($values);

        /** 同步附件 */
		if(!empty($numArr)) { 
		    $this->widget('Manager\Attach')->sync('page', $insertId, $this->auth->uid);
		}

		return $insertId;
    }

    /**
     * 更新内容
     *
     * @access public
     * @param int $cid 内容cid
     * @param array $content 内容结构数组
     * @return integer
     */
    public function updatePage($cid, array $values)
    {
        $page = $this->db
			->select('table.contents.cid')
		    ->from('table.contents')
			->where('table.contents.cid = ? AND table.contents.type = ?', $cid, 'page')
			->limit(1)
			->fetch();
		
        if (empty($page))
			
			return ;

		/** 计算图片数，和非图片文件数 */
        $numArr = $this->widget('Manager\Attach')->findNums('page', $cid, $this->auth->uid);
		
		if(!empty($numArr)) { 
		    $values['imagesNum'] = $numArr[0];
			$values['filesNum'] = $numArr[1];
		}
		
        /** 更新数据 */
        $updateRows = $this->updateContents($cid, $values);
		
		return $updateRows;
    }

    /**
     * 删除内容
     *
     * @access public
     * @param int $cid 内容cid
     * @return integer
     */
    public function deletePage($cid)
    {
        $deleteRows = false;

        $page = $this->db
			->select('table.contents.cid')
		    ->from('table.contents')
			->where('table.contents.cid = ? AND table.contents.type = ?', $cid, 'page')
			->limit(1)
			->fetch();
		
        if (empty($page))
			
			return ;
		
		$commentManager = $this->widget('Manager\Comment');
		
        while (1) 
		{
            $arrlist = $this->db
				->select('table.comments.coid')
				->from('table.comments')
			    ->where('table.comments.cid = ?', $cid)
				->limit(1000)
				->get();
				
            if (empty($arrlist)) {
                break;
            }
				
            foreach ($arrlist as $value) 
		    {
		        $commentManager->deleteComment($value['coid']);
		    }
        }		

        /** 删除附件 */
        $this->widget('Manager\Attach')->remove('page', $cid);
		
        /** 解除首页关联 */
        if ($cid == $this->config->frontPage) {
            $this->widget('Manager\Options')->updateOptions('frontPage', '');
        }
			
		/** 删除文章 */
		$deleteRows = $this->deleteContents($cid);

		return $deleteRows;
    }
	
    /**
     * 生成表单
     *
     * @access public
     * @param string $action 表单动作
     * @return Form
     */
    public function form()
    {
        /** 构建表格 */
        $form = Html::form();
		
        /** 主键 */
        $cid = Html::hidden('cid');
        $form->addInput($cid);
		
        /** 标题 */
        $title = Html::text('title')
		    ->label(__('标题'));
        $form->addInput($title);

        /** 网址缩略名 */
        $alias = Html::text('alias')
		    ->label(__('网址缩略名'));
        $form->addInput($alias);

        /** 文章 */
        $text = Html::textarea('text')
		    ->label(__('文章内容'))
			->style('min-height: 240px;');
        $form->addInput($text);

        /** 摘要 */
        $intro = Html::textarea('intro')
		    ->label(__('摘要'))
			->style('min-height: 120px;');
        $form->addInput($intro);
		
		/** 状态 */
		if($this->parameter->status) {
            $status = Html::select('status', $this->parameter->status, 'publish')
		        ->label(__('状态'));
            $form->addInput($status);
		}

        /** 排序 */
        $order = Html::text('order')
		    ->label(__('排序'));
        $form->addInput($order);		
		
		/** 文章图标 */
        $pic = Html::file('pic')
		    ->label(__('文章图标'))
		    ->description('支持外链');
        $form->addInput($pic);
		
		$templates = $this->getTemplates();
		if($templates) {
            $options = array();
            $options[] =  __('不选择');
            $options += $templates;
            $template = Html::select('template', $options)
		        ->label(__('自定义模板'))
		        ->description(__('如果你为此页面选择了一个自定义模板, 系统将按照你选择的模板文件展现它.'));
            $form->addInput($template);
		}

        $allowComment = Html::checkbox('allowComment', ['1'=>'允许评论'], 1)
		    ->only()
		    ->label(__('评论权限'));
        $form->addInput($allowComment);

        /** 提交按钮 */
        $submit = Html::submit();
        $form->set($submit);
		
        if ($this->cid) {

		    if(isset($status)) {
                $status->value($this->status);
		    }
			
		    if(isset($template)) {
                $template->value($this->template);
		    }
			
			$intro->value($this->intro);
			$order->value($this->order);
            $title->value($this->title);
            $alias->value($this->alias);
			$text->value($this->text);
			$allowComment->value($this->allowComment);
            $cid->value($this->cid);
            $pic->value($this->pic);
		    $this->pic && $pic->append('<img style="margin: 5px 0;" class="w3-pic" src="'.$this->icon .'" />');
            $submit->value(__('更新文章'));

        } else {
            $submit->value(__('发布文章'));
        }

		/** 插件接口 */
        return \do_action('page_form', $this, false, $form);

    }
}

