<?php

namespace Manager;

use Base\Comments as Manager;

!defined('W3_ROOT_DIR') AND exit;

/**
 * 评论编辑管理基类
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */
class Comments extends Manager
{
    /**
     * 增加评论
     *
     * @access public
     * @param array $values 评论结构数组
     * @return integer
     */
    public function addComments(array $values)
    {
        /** 构建插入结构 */
        $comment = [
            'cid'           =>  empty($values['cid']) ? 0 : intval($values['cid']),
			'uid'           =>  isset($values['uid']) ? intval($values['uid']) : $this->auth->uid,
            'parent'        =>  empty($values['parent']) ? 0 : intval($values['parent']),
            'ownerId'       =>  empty($values['ownerId']) ? 0 : intval($values['ownerId']),
            'created'       =>  empty($values['created']) ? $this->config->time : $values['created'],
            'ip'            =>  empty($values['ip']) ? $this->request->ip(true) : $values['ip'],
            'text'          =>  empty($values['text']) ? '' : $this->filterText($values['text']),
            'type'          =>  empty($comment['type']) ? 'comment' : $comment['type'],
            'status'        =>  empty($comment['status']) ? 'approved' : $comment['status'],
        ];

        if (!empty($values['coid'])) {
            $comment['coid'] = $values['coid'];
        }
		
        return $this->db
		    ->insert('table.comments')
		    ->rows($comment)
			->lastId();
    }

    /**
     * 更新评论
     *
     * @access public
     * @param int $coid 评论coid
     * @param array $values 评论结构数组
     * @return integer
     */
    public function updateComments($coid, array $values)
    {
        /** 构建插入结构 */
        $update = [
            'cid'           =>  empty($values['cid']) ? 0 : intval($values['cid']),
			'uid'           =>  isset($values['uid']) ? intval($values['uid']) : $this->auth->uid,
            'parent'        =>  empty($values['parent']) ? 0 : intval($values['parent']),
            'ownerId'       =>  empty($values['ownerId']) ? 0 : intval($values['ownerId']),
            'ip'            =>  empty($values['ip']) ? $this->request->ip(true) : $values['ip'],
            'text'          =>  empty($values['text']) ? '' : $this->filterText($values['text']),
            'status'        =>  empty($values['status']) ? 'approved' : $values['status'],
        ];
		
        $comment = [];
        foreach ($values as $key => $val) 
		{
            if (array_key_exists($key, $update)) {
                $comment[$key] = $update[$key];
            }
        }

        /** 更新创建时间 */
        if (!empty($values['created'])) {
            $comment['created'] = $values['created'];
        }

        return $this->db->update('table.comments')
		    ->where('coid = ?', $coid)
			->rows($comment)
			->affected();
    }

    /**
     * 删除数据
     *
     * @access public
     * @param int $coid 评论coid
     * @return integer
     */
    public function deleteComments($coid)
    {
        return $this->db
		    ->delete('table.comments')
		    ->where('coid = ?', $coid)
			->affected();
    }

    /**
     * filterText 
     * 
     * @param mixed $html 
     * @access public
     * @return string
     */
    public function filterText($html)
    {
		/** 插件接口 */
		return \action_exists('comments_filter_text') 
		    ? \do_action('comments_filter_text', $this, false, $html)
			: strip_tags($html, '<p><br>' . $this->config->commentsHtmlTagAllowed);
    }

	/**
	 * 递增数值
	 *
	 * @access public
	 * @param int|string $coid 
	 * @param string     $field  
	 * @param int        $num 
	 * @return false|int 
	 */
	public function incr( $coid, $field, $num = 1 )
	{
		$num = max(1, (int) $num);
		
        /** 更新内容数 */
		return $this->db->update('table.comments')
		    ->where('table.comments.coid = ?', $coid)
		    ->expression($field, "$field + $num")
			->affected();
	}
	
	/**
	 * 递减数值
	 *
	 * @access public
	 * @param int|string $coid 
	 * @param string     $field  
	 * @param int        $num 
	 * @return false|int 
	 */
	public function decr( $coid, $field, $num = 1 )
	{
		$num = max(1, (int) $num);

        /** 更新内容数 $field >= 是防止UNSIGNED类型 数量已经是零 报错 */
		return $this->db->update('table.comments')
		    ->where("table.comments.coid = ? and $field >= ?", $coid, $num)
		    ->expression($field, "$field - $num")
			->affected();
	}
}

