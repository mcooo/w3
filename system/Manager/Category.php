<?php

namespace Manager;

use W3\Html;

!defined('W3_ROOT_DIR') AND exit;

/**
 * 描述性数据编辑管理基类
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */
class Category extends Metas
{
    /**
     * 增加分类
     *
     * @access public
     * @return void
     */
    public function addCategory($values = [])
    {
		/** 不允许修改type */
		$values['type'] = 'category';
		
		/** 计算图片数，和非图片文件数 */
        $numArr = $this->widget('Manager\Attach')->findNums('category', 0, $this->auth->uid);
		
        /** 插入数据 */
        $insertId = $this->addMetas($values);

        /** 同步附件 */
		if(!empty($numArr)) { 
		    $this->widget('Manager\Attach')->sync('category', $insertId, $this->auth->uid);
		}

		return $insertId;
    }

    /**
     * 更新分类
     *
     * @access public
     * @return void
     */
    public function updateCategory($mid, $values = [])
    {
        $category = $this->db
			->select('table.metas.mid')
		    ->from('table.metas')
            ->where('table.metas.mid = ? AND table.metas.type = ?', $mid, 'category')
            ->limit(1)
		    ->fetch();
		
        if (empty($category))
			return ;

		/** 不允许修改type */
		$values['type'] = 'category';
		
        /** 更新数据 */
        $updateRows = $this->updateMetas($mid, $values);

		return $updateRows;
    }

    /**
     * 删除分类
     *
     * @access public
     * @return integer
     */
    public function deleteCategory($mid)
    {
		# 分类中有内容，不允许删除分类
        $category = $this->db
		    ->select('table.metas.count')
		    ->from('table.metas')
            ->where('table.metas.mid = ? AND table.metas.type = ?', $mid, 'category')
			->limit(1)
		    ->fetch();

        if ($category['count'] > 0) {
			return false;
		}
		
		# 分类有下级分类，不允许删除分类
        $sub = $this->db
		    ->select('table.metas.mid')
		    ->from('table.metas')
            ->where('table.metas.type = ? AND table.metas.parent = ?', 'category', $mid)
			->limit(1)
		    ->fetch();

        if (!empty($sub)) {
			return false;
		}

        /** 删除附件 */
        $this->widget('Manager\Attach')->remove('category', $mid);

	    /** 删除分类 */
		$deleteRows = $this->deleteMetas($mid);

		return $deleteRows;
    }
	
    /**
     * 生成表单
     *
     * @access public
     * @param string $action 表单动作
     * @return Form
     */
    public function form()
    {
        /** 构建表格 */
        $form = Html::form();
		
        /** 分类名称 */
        $name = Html::text('name')
		    ->label(__('分类名称') . ' *');
        $form->addInput($name);

        /** 分类缩略名 */
        $alias = Html::text('alias')
		    ->label(__('分类缩略名'))
		    ->description(__('分类缩略名用于创建友好的链接形式, 建议使用字母, 数字, 下划线和横杠.'));
        $form->addInput($alias);

        /** 父级分类 */
        $options = [0 => __('不选择')];
        $parents = $this->widget('Category\Rows');
        while ($parents->next()) {
            $options[$parents->mid] = ($parents->level ? '&nbsp;&nbsp;&nbsp;&nbsp;|--' : '') . str_repeat('--', $parents->level) . $parents->name;
        }
        $parent = Html::select('parent', $options, $this->request->parent)
		    ->label(__('父级分类'))
		    ->description(__('此分类将归档在您选择的父级分类下.'));
        $form->addInput($parent);

		/** 分类图标 */
        $pic = Html::file('pic')
		    ->label(__('分类图标'))
		    ->description('支持外链');
        $form->addInput($pic);
		
        /** 分类排序 */
        $order = Html::text('order', 99)
		    ->label(__('分类排序'))
		    ->description(__('请输入整数数字.'));
        $form->addInput($order);

        /** 分类描述 */
        $description = Html::text('description')
		    ->label(__('分类描述'))
		    ->description(__('此文字用于描述分类, 在有的主题中它会被显示.'));
        $form->addInput($description);

        /** 分类主键 */
        $mid = Html::hidden('mid');
        $form->set($mid);

        /** 提交按钮 */
        $submit = Html::submit();
        $form->set($submit);

        if ($this->mid) {
            $name->value($this->name);
            $alias->value($this->alias);
            $parent->value($this->parent);
			$order->value($this->order);
            $description->value($this->description);
            $mid->value($this->mid);
            $pic->value($this->pic);
		    $this->pic && $pic->append('<img style="margin: 5px 0;" class="w3-pic" src="'.$this->icon .'" />');
            $submit->value(__('编辑分类'));

        } else {
            $submit->value(__('增加分类'));
        }

        //  检查上级分类是否合法
		$form->register('parentCheck', function ($parent, $mid){
		    return (!$mid || $mid != $parent) && (!$parent || $this->db
			    ->select()
		        ->from('table.metas')
                ->where('table.metas.mid = ? and table.metas.type = ?', $parent, 'category')
			    ->count());
        });

        //  检查分类是否有文章
		$form->register('countCheck', function ($parent){
		    return !($this->parent != $parent && $this->count);
        });

        //  检查分类是否有下级
		$form->register('lowerCheck', function ($parent, $mid){
		    return !($this->parent != $parent && $this->db
			    ->select()
		        ->from('table.metas')
                ->where('table.metas.type = ? and table.metas.parent = ?', 'category', $mid)
			    ->count());
        });

        /** 给表单增加规则 */
        $name->addRule('required', __('必须填写分类名称'));
		$name->addRule('max', __('分类名称最多包含32个字符'), 32);
        $name->addRule('xss', __('请不要在分类名称中使用特殊字符'));
        $alias->addRule('xss', __('请不要在缩略名中使用特殊字符'));
		$pic->addRule('xss', __('请不要在分类图标中使用特殊字符'));
		
	    $parent->addRule('parentCheck', __('父级分类不合法'), $form->getParam('mid'));

        if ($this->mid) {
			$parent->addRule('lowerCheck', __('分类有下级分类，请先删除下级分类'), $form->getParam('mid'));
			$parent->addRule('countCheck', __('分类中有文章不能修改上级分类，请先删除文章'));
        }

		/** 插件接口 */
        return \do_action('category_form', $this, false, $form);
    }

}
