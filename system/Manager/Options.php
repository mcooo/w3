<?php

namespace Manager;

use W3\Json;
use W3\Widget;

!defined('W3_ROOT_DIR') AND exit;

/**
 * 全局选项编辑管理基类
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */
class Options extends Widget
{
    /**
     * 更新数据库已添加的选项的值
     *
     * @param string $name  选项名
     * @param mixed $value  选项值 (插入数据库之前, 数组将被进行 JSON 编码)
     * @return bool 如果值未更新，则为False；如果值已更新，则为true
     */
    public function updateOptions($name, $value)
    {
	    $name = trim($name);
	
	    if (empty($name)) {
		    return false;
	    }
	
        $this->config->{$name} = $value;
	
	    if(is_array($value)) {
		    $value = Json::encode($value);
	    }else{
		    is_null($value) && $value = '';
	    }

        return $this->db
		    ->update('table.options')
		    ->where('name = ?', $name)
	        ->rows(['value' => $value])
		    ->affected();
    }

    /**
     * 添加数据库一个新选项
     *
     * @param string $name  要添加的选项的名称
     * @param mixed $value  选项值 (插入数据库之前, 数组将被进行 JSON 编码)
     * @return bool 如果未添加选项，则为False；如果添加了选项，则为true。
     */
    public function addOptions($name, $value = '')
    {
	    $name = trim($name);
	
	    if (empty($name)) {
		    return false;
	    }
	
	    if(isset($this->config->{$name})) {
		    return $this->update($name, $value);
	    }

        $this->config->{$name} = $value;

	    if(is_array($value)) {
		    $value = Json::encode($value);
	    }else{
		    is_null($value) && $value = '';
	    }

        return $this->db
		    ->insert('table.options')
		    ->rows(['name' => $name, 'value' => $value])
		    ->affected();
    }

    /**
     * 按名称删除数据库选项
     *
     * @param string $name 要删除的选项的名称
     * @return bool 如果选项已成功删除，则为True。失败为False
     */
    public function deleteOptions($name)
    {
	    $name = trim($name);
	
	    if (empty($name)) {
		    return false;
	    }	
	
	    $this->config->{$name} = NULL;
		
        return $this->db
		    ->delete('table.options')
		    ->where('name = ?', $name)
		    ->affected();
    }
}
