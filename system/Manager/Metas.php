<?php

namespace Manager;

use W3\Util;
use Base\Metas as Manager;

!defined('W3_ROOT_DIR') AND exit;

/**
 * 描述性数据编辑管理基类
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */

class Metas extends Manager
{
    /**
     * 插入一条记录
     *
     * @access public
     * @param array $values 记录结构数组
     * @return integer
     */
    public function addMetas(array $values)
    {
        /** 构建插入结构 */
        $meta = [
            'parent'        =>  empty($values['parent']) ? 0 : intval($values['parent']),
			'name'          =>  empty($values['name']) ? '' : htmlspecialchars($values['name']),
            'type'          =>  empty($values['type']) ? 'tag' : $values['type'],
			
			/** 避免冲突 */
			'alias'         =>  Util::alias(),
            'order'         =>  empty($values['order']) ? 0 : intval($values['order']),
			'template'      =>  empty($values['template']) ? '' : $values['template'],
            'count'         =>  empty($values['count']) ? 0 : intval($values['count']),
            'description'   =>  empty($values['description']) ? '' : $values['description'],
            'pic'           =>  empty($values['pic']) ? '' : $values['pic'],
        ];

        if (!empty($values['mid'])) {
            $meta['mid'] = $values['mid'];
        }

		/** 首先插入部分数据 */
        $insertId = $this->db
		    ->insert('table.metas')
		    ->rows($meta)
			->lastId();
			
        /** 更新缩略名 */
        if ($insertId > 0 )

			$this->applyAlias(empty($values['alias']) ? $insertId : $values['alias'], $insertId);
			
        return $insertId;
    }

    /**
     * 更新记录
     *
     * @access public
     * @param $mid 记录mid
     * @param array $values 记录更新结构数组
     * @return integer
     */
    public function updateMetas($mid, array $values)
    {
        /** 构建插入结构 */
        $update = [
            'parent'        =>  empty($values['parent']) ? 0 : intval($values['parent']),
			'name'          =>  empty($values['name']) ? '' : htmlspecialchars($values['name']),
            'type'          =>  empty($values['type']) ? 'tag' : $values['type'],
			'alias'         =>  empty($values['alias']) ? '' : $values['alias'],
            'order'         =>  empty($values['order']) ? 0 : intval($values['order']),
			'template'      =>  empty($values['template']) ? '' : $values['template'],
            'count'         =>  empty($values['count']) ? 0 : intval($values['count']),
            'description'   =>  empty($values['description']) ? '' : $values['description'],
            'pic'           =>  empty($values['pic']) ? '' : $values['pic'],
        ];
		
        $meta = [];
        foreach ($values as $key => $val) 
		{
            if (array_key_exists($key, $update)) {
                $meta[$key] = $update[$key];
            }
        }

		/** 检测更新缩略名 */
        array_key_exists('alias', $values) 
		    && $meta['alias'] = $this->applyAlias(empty($values['alias']) ? $mid : $values['alias'], $mid);
		
        $updateRows = $this->db
		    ->update('table.metas')
		    ->where('table.metas.mid = ?', $mid)
		    ->rows($meta)
		    ->affected();

        return $updateRows;
    }
	
    /**
     * 删除记录
     *
     * @access public
     * @param $mid 记录mid
     * @return integer
     */
    public function deleteMetas($mid)
    {
        return $this->db
		    ->delete('table.metas')
		    ->where('table.metas.mid = ?', $mid)
			->affected();
    }
	
    /**
     * 应用缩略名
     *
     * @access public
     * @param string $alias 缩略名
     * @param mixed $mid 记录mid
     * @return string
     */
    public function applyAlias($alias, $mid)
    {
		/** 插件接口 */
        if (\action_exists('metas_alias')) {
            return \do_action('metas_alias', $this, false, $alias, $mid);
        }
		
		$count = 1;
		$org_alias = $alias;
		$pass = true;
		
        /** 判断是否在数据库中已经存在 */
        while (true) 
		{
			$row = $this->db
				->select('mid')
			    ->from('table.metas')
                ->where('table.metas.alias = ?', $alias)
			    ->fetch();
					
			if($row){
				if($row['mid'] != $mid){
                    $alias = $count > 1  ? $org_alias . '-' . $count : $alias . '-' . $count;
                    $count ++;
			    } else {
					$pass = false;
					break;
				}
			} else {
				break;
			}
        }

		$pass && $this->db
		    ->update('table.metas')
		    ->where('table.metas.mid = ?', $mid)
			->rows(['alias'=>$alias])
			->affected();

        return $alias;
    }
	
	
	/**
	 * 递增数值
	 *
	 * @access public
	 * @param int|string $mid 
	 * @param string     $field  
	 * @param int        $num 
	 * @return false|int 
	 */
	public function incr( $mid, $field, $num = 1 )
	{
		$num = max(1, (int) $num);
		
        /** 更新内容数 */
		return $this->db->update('table.metas')
		    ->where('table.metas.mid = ?', $mid)
		    ->expression($field, "$field + $num")
			->affected();
	}
	
	/**
	 * 递减数值
	 *
	 * @access public
	 * @param int|string $mid 
	 * @param string     $field  
	 * @param int        $num 
	 * @return false|int 
	 */
	public function decr( $mid, $field, $num = 1 )
	{
		$num = max(1, (int) $num);

        /** 更新内容数 $field >= 是防止UNSIGNED类型 数量已经是零 报错 */
		return $this->db->update('table.metas')
		    ->where("table.metas.mid = ? and $field >= ?", $mid, $num)
		    ->expression($field, "$field - $num")
			->affected();
	}
}
