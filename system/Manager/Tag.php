<?php

namespace Manager;

use W3\Util;
use W3\Json;
use W3\Html;
use W3\Element\Form;
use W3\Exception;

!defined('W3_ROOT_DIR') AND exit;

/**
 * 标签编辑管理基类
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */
class Tag extends Metas
{
    /**
     * 插入标签
     *
     * @access public
     * @return void
     */
    public function addTag($values = [])
    {
		/** 不允许修改type */
		$values['type'] = 'tag';

		return $this->addMetas($values);
    }

    /**
     * 更新标签
     *
     * @access public
     * @return void
     */
    public function updateTag($mid, $values = [])
    {
        $tag = $this->db
			->select('table.metas.mid')
		    ->from('table.metas')
            ->where('table.metas.mid = ? AND table.metas.type = ?', $mid, 'tag')
            ->limit(1)
		    ->fetch();
		
        if (empty($tag))
			
			return ;
		
		/** 不允许修改type */
		$values['type'] = 'tag';
		
        /** 更新数据 */
        $updateRows = $this->updateMetas($mid, $values);
		
		/** 更新后的数据 */
        $tag = $this->db
			->select()
		    ->from('table.metas')
            ->where('table.metas.mid = ?', $mid)
            ->limit(1)
		    ->fetch();
			
		/** 查找所有cid, 逐个更新 */
		$max = 0;
        while (1) 
		{
            $arrlist = $this->db
				->select()
				->from('table.relate')
			    ->where('table.relate.mid = ? and table.relate.cid > ?', $mid, $max)
				->limit(1000)
				->order('table.relate.cid', 'ASC')
				->get();
				
            if (empty($arrlist)) {
                break;
            }

            foreach ($arrlist as $value) 
			{
				$value['cid'] > $max && $max = $value['cid'];
			    $content = $this->db
				    ->select('tagsJson')
			        ->from('table.contents')
				    ->where('table.contents.cid = ?', $value['cid'])
				    ->fetch();
				
			    if (isset($content['tagsJson'])) {
				    $tagsJson = Json::decode($content['tagsJson']);
				
				    if (isset($tagsJson[$mid])) {
					    $tagsJson[$mid]['name'] = $tag['name'];
					    $tagsJson[$mid]['alias'] = $tag['alias'];
					
					    $this->db
					        ->update('table.contents')
					        ->where('table.contents.cid = ?', $value['cid'])
					        ->rows(['tagsJson' => Json::encode($tagsJson)])
					        ->affected();
                    }
                }
		    }
        }
		
		return $updateRows;
    }

    /**
     * 删除标签
     *
     * @access public
     * @return void
     */
    public function deleteTag($mid)
    {
		$deleteRows = false;
		
        $tag = $this->db
			->select('table.metas.mid')
		    ->from('table.metas')
            ->where('table.metas.mid = ? AND table.metas.type = ?', $mid, 'tag')
            ->limit(1)
		    ->fetch();

        if (empty($tag))
			
			return ;

		/** 查找所有cid, 逐个更新 */
		$max = 0;
        while (1) 
		{
            $arrlist = $this->db
				->select()
				->from('table.relate')
			    ->where('table.relate.mid = ? AND table.relate.cid > ?', $mid, $max)
				->limit(1000)
				->order('table.relate.cid', 'ASC')
				->get();
				
            if (empty($arrlist)) {
                break;
            }

            foreach ($arrlist as $value) 
			{
				$value['cid'] > $max && $max = $value['cid'];
			    $content = $this->db
				    ->select('tagsJson')
			        ->from('table.contents')
				    ->where('table.contents.cid = ?', $value['cid'])
				    ->fetch();
				
			    if (isset($content['tagsJson'])) {
				    $tagsJson = Json::decode($content['tagsJson']);
				
				    if (isset($tagsJson[$mid])) {
						
					    unset($tagsJson[$mid]);
					    $this->db
					        ->update('table.contents')
					        ->where('table.contents.cid = ?', $value['cid'])
					        ->rows(['tagsJson' => Json::encode($tagsJson)])
					        ->affected();
                    }
                }
				
			    /** 删除关系数据表内容 */
			    $this->db
			        ->delete('table.relate')
				    ->where('table.relate.mid = ? and table.relate.cid = ?', $mid, $value['cid'])
				    ->affected();
		    }
        }
			
        $deleteRows = $this->deleteMetas($mid);

		return $deleteRows;
    }

    /**
     * 生成表单
     *
     * @access public
     * @param object $object object
     * @return Form
     */
    public function form(): Form
    {
        /** 构建表格 */
        $form = Html::form();

        /** 标签主键 */
        $mid = Html::hidden('mid');
        $form->addInput($mid);

        /** 标签名称 */
        $name = Html::text('name')
		    ->label(__('标签名称') . ' *')
		    ->description(__('这是标签在站点中显示的名称.可以使用中文,如 "地球".'));
        $form->addInput($name);

        /** 标签缩略名 */
        $alias = Html::text('alias')
		    ->label(__('标签缩略名'))
		    ->description(__('标签缩略名用于创建友好的链接形式, 如果留空则默认使用标签名称.'));
        $form->addInput($alias);

        /** 提交按钮 */
        $submit = Html::submit();
        $form->set($submit, 'submit');
		
        if ($this->mid) {
            $name->value($this->name);
            $alias->value($this->alias);
            $mid->value($this->mid);
            $submit->value(__('编辑标签'));
        } else {
            $submit->value(__('增加标签'));
        }

        // 判断标签名称是否存在
		$form->register('nameExists', function ($name, $mid){
            $select = $this->db
			    ->select('mid')
		        ->from('table.metas')
		        ->where('table.metas.type = ? and table.metas.name = ?', 'tag', $name)
			    ->fetch();
			
			return empty($select) || $mid && $mid == $select['mid'];
        });
		
        // 判断标签是否存在
		$form->register('tagExists', function ($mid){
            return $this->db
			    ->select(1)
		        ->from('table.metas')
		        ->where('table.metas.type = ? and table.metas.mid = ?', 'tag', $mid)
			    ->count();
        });

        /** 给表单增加规则 */
        $name->addRule('required', __('必须填写标签名称'));
        $name->addRule('nameExists', __('标签名称已经存在'), $form->getParam('mid'));
		$name->addRule('max', __('标签名称最多包含32个字符'), 32);
        $name->addRule('xss', __('请不要标签名称中使用特殊字符'));
        $alias->addRule('xss', __('请不要在缩略名中使用特殊字符'));
		
		/** 验证更新模式表单输入项 */
        if ($this->mid) {
            $mid->addRule('required', __('标签主键不存在'));
            $mid->addRule('tagExists', __('标签不存在'));
        }
		
		/** 插件接口 */
        return \do_action('tag_form', $this, false, $form);
    }

}
