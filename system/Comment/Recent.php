<?php

namespace Comment;

use Base\Comments;

/**
 * 最近评论组件
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */
class Recent extends Comments
{
    /**
     * 初始化
     *
     * @access protected
     * @return void
     */
    protected function init()
    {
        $this->parameter([
		
		    'pageSize' => $this->config->commentsListSize,
			'cid' => 0,
			'ignoreAuthor' => false
			
		], true);

    }	

    /**
     * 执行函数
     *
     * @access public
     * @return void
     */
    public function execute()
    {
        $select = $this->select()
		    ->limit($this->parameter->pageSize)
            ->where('table.comments.status = ?', 'approved')
            ->order('table.comments.coid', 'DESC');

        if ($this->parameter->cid) {
            $select->where('cid = ?', $this->parameter->cid);
        }

        if ($this->config->commentsShowCommentOnly) {
            $select->where('type = ?', 'comment');
        }

        /** 忽略作者评论 */
        if ($this->parameter->ignoreAuthor) {
            $select->where('ownerId <> authorId');
        }

        $select->get([$this, 'push']);
    }
}
