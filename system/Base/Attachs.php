<?php

namespace Base;

use W3\Widget;
use W3\Db\Query;
use function do_action;

/**
 * 附件基类
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */

class Attachs extends Widget
{
    /**
     * 获取查询对象
     *
     * @return Query
     * @throws Exception
     */
    public function select(...$args): Query
    {
		return do_action( 
		    $this->parameter->main . '.select',
			$this,
			false,
			$this->db->select(...$args)->from('table.attachs')
		);
    }
	
    /**
     * 通用过滤器
     *
     * @access public
     * @param array $value 需要过滤的行数据
     * @return array
     */
    public function filter(array $value)
    {
		/** 插件接口 */
        return do_action('attachs_filter', $this, false, $value);
    }

}

