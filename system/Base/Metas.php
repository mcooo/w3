<?php

namespace Base;

use W3\Widget;
use W3\Router;
use W3\Db\Query;
use function do_action;

/**
 * 描述性数据基类
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */

class Metas extends Widget
{
    /**
     * 获取查询对象
     *
     * @return Query
     * @throws Exception
     */
    public function select(...$args): Query
    {
		return do_action( 
		    $this->parameter->main . '.select',
			$this,
			false,
			$this->db->select(...$args)->from('table.metas')
		);
    }
	
    /**
     * 将每行的值压入堆栈
     *
     * @access public
     * @param array $value 每行的值
     * @return array
     */
    public function push(array $value)
    {
        $value = $this->filter($value);
        return parent::push($value);
    }
	
    /**
     * 通用过滤器
     *
     * @access public
     * @param array $value 需要过滤的行数据
     * @return array
     */
    public function filter(array $value)
    {
		if (isset($value['type'], $value['mid'], $value['alias'])) {	

            $value['permalink'] = Router::buildUrl($value['type'], $value);
		}

		/** 插件接口 */
		$value = do_action('metas_filter', $this, false, $value);
		
        return $value;
    }
	
    /**
     * 锚点id
     *
     * @access protected
     * @return string
     */
    protected function ___theId()
    {
        return $this->type . '-' . $this->mid;
    }
	
    protected function ___icon(): string
    {
		$icon = $this->pic;
		if($icon){
		    strpos($icon, '://') === FALSE && $icon = $this->cdnUrl($icon, false);
		}else{
			$icon = $this->config->staticUrl('images/no-image.png', false);
		}
        return $icon;
    }
}
