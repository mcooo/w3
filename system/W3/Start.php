<?php

namespace W3;

use function add_action;

!defined('W3_ROOT_DIR') AND exit;

/**
 * 开启框架模块/初始化全局选项 
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */
class Start extends Widget
{
    /**
     * @param int $components
     */
    protected function initComponents(int &$components)
    {
        $components = self::INIT_CONFIG;
    }
	
    /**
     * 初始化
     *
     * @access protected
     * @return void
     */
    protected function init()
    {
		# 定义变量默认数据
        $this->parameter([

			# 设置主体
			'main' => 'start',	
			
		], true);
		
		# 映射类别名
		Loader::aliases([
		
		    'Loader'=>'W3\Loader',
			'Router'=>'W3\Router',
			'Json'=>'W3\Json',
			'Util'=>'W3\Util',
			'Html'=>'W3\Html',

		]);

        # 定义全局变量默认数据
        $this->config->merge([
			
            # 定义默认upload目录
            'uploadDir' => W3_ROOT_DIR . 'app/uploads/',
			
            # 定义根目录
            'rootUrl' => $this->request->rootUrl(true),
			
			# 默认的路由规则原始数据列表  【顺序不同权重不一样, 路由规则列表顺序不可随意变动】
			'originalRoutes' => [

                ['index /', 'Module\Cms\Index', 'view'],
                ['login /login', 'Module\Cms\Login', 'view'],
                ['logout /logout', 'Module\Cms\Logout', 'view'],
                ['register /register', 'Module\Cms\Register', 'view'],
                ['upload /upload', 'W3\Uploader', 'action'],
				
				# 后台默认路由规则 【注意：必须依照此路由规则添加后台模块路由, 不可随意修改】
                ['admin /admin/?{permalink:.*}', 'Module\Admin\Home', 'action'],
				
                ['tag /tag/{alias:[^/]+}', 'Module\Cms\Tag', 'view'],
                ['author /author/{uid:\d+}', 'Module\Cms\Author', 'view'],
                ['search /search/?{keywords:[^/?]+}', 'Module\Cms\Search', 'view'],
                ['post /post/{cid:\d+}', 'Module\Cms\Single', 'view'],
                ['page /page/{alias:[^/]+}.html', 'Module\Cms\Single', 'view'],
                ['category /{alias:[^/]+}', 'Module\Cms\Category', 'view'],

                ['comment_page {permalink:.*}/comment-page-{commentPage:\d+}', 'Module\Cms\Single', 'view'],

                ['error404 /404', 'Module\Cms\Error', 'view'],
                ['feedback {permalink:.*}/comment', 'Module\Cms\Feedback', 'action'],
			],
			
        ]);
		
		# 数据库查询全局选项
		$list = $this->db
		    ->select()
		    ->from('table.options')
			->where('table.options.uid = ?', 0)
			->get();
			
		# 导入全局选项变量
        foreach ($list as $item) 
		{
            $this->config->{$item['name']} = $item['value'];
        }
		
        # 动态判断皮肤
        $this->config->theme = is_dir( W3_TEMPLATE_DIR . $this->config->theme ) ? $this->config->theme : 'default';
		
        # 皮肤目录
        $this->config->themeDir = W3_TEMPLATE_DIR . $this->config->theme . DIRECTORY_SEPARATOR;
		
        # 支持皮肤变量重载
        if (!empty($this->config->{'theme:' . $this->config->theme})) {

            # 解析变量
            if ($themeOptions = Json::decode($this->config->{'theme:' . $this->config->theme})) {
				
                # 覆盖变量
				$this->config->merge($themeOptions);
            }
        }
		
		# 插件列表解码
	    $this->config->plugins = !empty($this->config->plugins) ? Json::decode($this->config->plugins) : [];		
		
        # 路由列表解码
        $this->config->routes = !empty($this->config->routes) ? Json::decode($this->config->routes) : [];		

		# 保留原始数据
		$this->config->originalSiteUrl = $this->config->siteUrl;
		
        # 增加对SSL连接的支持
        if ($this->request->ssl() && 0 === strpos($this->config->siteUrl, 'http://')) {
            $this->config->siteUrl = substr_replace($this->config->siteUrl, 'https', 0, 4);
        }

		# 增加后台初始化模块
        add_action('admin.start', function () 
		{
			# 初始化模块
			$this->widget('W3\Admin');
			
			# 提示框响应支持
			$this->doNotice();
        });
		
		# 增加前台初始化模块
        add_action('start', function () 
		{
			# 初始化模块
			$this->widget('W3\Cms');
			
			# 提示框响应支持
			$this->doNotice();
        });
		
		# 全局通用函数
		$start = Start::class;
        add_action( 'call@goBack',             [$start, '_goBack'] );		
        add_action( 'call@import',             [$start, '_import'] );
        add_action( 'call@themeTitle',         [$start, '_title'] );
        add_action( 'call@themeKeywords',      [$start, '_keywords'] );
        add_action( 'call@themeDescription',   [$start, '_description'] );
        add_action( 'call@pagination',         [$start, '_pagination'] );		
        add_action( 'call@themeFile',          [$start, '_themeFile'] );
        add_action( 'call@asset',              [$start, '_asset'] );		
        add_action( 'call@layout',             [$start, '_layout'] );			
        add_action( 'call@config',             [$start, '_config'] );
        add_action( 'call@assetUrl',           [$start, '_assetUrl'] );
        add_action( 'call@siteUrl',            [$start, '_siteUrl'] );
        add_action( 'call@uploadUrl',          [$start, '_uploadUrl'] );
        add_action( 'call@cdnUrl',             [$start, '_cdnUrl'] );
        add_action( 'call@loginUrl',           [$start, '_loginUrl'] );
        add_action( 'call@logoutUrl',          [$start, '_logoutUrl'] );
        add_action( 'call@registerUrl',        [$start, '_registerUrl'] );
        add_action( 'call@adminUrl',           [$start, '_adminUrl'] );
        add_action( 'call@notFound',           [$start, '_notFound'] );
        add_action( 'call@throwJson',          [$start, '_throwJson'] );
        add_action( 'call@redirect',           [$start, '_redirect'] );
        add_action( 'call@notice',             [$start, '_notice'] );
        add_action( 'call@doNotice',           [$start, '_doNotice'] );		
        add_action( 'call@allowedFileTypes',   [$start, '_allowedFileTypes'] );
		
        # 定义变量数据
        $this->config->merge([

            # 定义模板url
            'templateUrl' => $this->asset(W3_TEMPLATE_DIR),
			
            # 定义皮肤url
            'themeUrl' => $this->asset(W3_TEMPLATE_DIR . $this->config->theme . '/'),
			
            # 定义插件url
            'pluginUrl' => $this->asset(W3_PLUGIN_DIR),
			
            # 可以配置单独的 CDN 域名：比如：//static.domain.com/
            'assetUrl' => $this->asset('system/static'),
			
            # 定义路径前缀
            'index' => $this->config->rewrite ? $this->config->rootUrl : rtrim($this->config->rootUrl, '/') . '/index.php',
        ]);

        # 语言包初始化
        if ($this->config->lang && $this->config->lang != 'zh_ch') {
            $file = W3_ROOT_DIR . 'system/lang/' . $this->config->lang . '.php';
            if (file_exists($file)) {
                Lang::instance()->set(include $file, $this->config->lang);
            }
        }

        # cookie初始化
        $prefix = md5($this->config->rootUrl);
        $parsed = parse_url($this->config->rootUrl);

        /** 在路径后面强制加上斜杠 */
        $path = empty($parsed['path']) ? '/' : rtrim($parsed['path'], '/') . '/';

		Cookie::prefix($prefix);
		Cookie::path($path);
		
        # 初始化回执
        $this->response
		    ->charset($this->config->charset)
			->type($this->config->contentType);
		
        # 初始化时区
        Timer::setTimezoneOffset($this->config->timezone);
		
		# time变量
		$this->config->time = Timer::instance()->time();

		# 设置路由路径前缀
		Router::prefix($this->config->index);
		
        # 注册默认路由规则
        Router::map($this->config->originalRoutes);

        # 路由变量初始化
        if ($routes = $this->config->routes) {
			
			$rules = Router::export();
			$merge = [];
            foreach ($routes as $route => $pattern) 
		    {
				if(isset($rules[$route])) {
					$merge[] = ["$route $pattern", $rules[$route]->widget, $rules[$route]->action];
				}
		    }
			
			$merge && Router::map($merge);
        }

		$pathinfo = $this->request->pathinfo();
		$adminUrl = Router::ruleUrl('admin');
		$indexUrl = Router::ruleUrl('index');

		# 自定义首页功能
		if($this->config->frontPage && ($indexUrl == $pathinfo || 0 === strpos($pathinfo, '/comment-page-'))){
			
			Router::map(
			    'index /',
				'Module\Cms\Single',
				'view',
				null,
				['main' => 'index' , 'front' => true],
				['cid' => $this->config->frontPage]
			);
			
			Router::map(
			    'comment_page /comment-page-{commentPage:\d+}',
				'Module\Cms\Single',
				'view',
				'index',
				['main' => 'index' , 'front' => true],
				['cid' => $this->config->frontPage]
			);

		}
		
		# 后台支持
		if($adminUrl == $pathinfo || 0 === strpos($pathinfo, $adminUrl . '/')) {
		    
			$this->parameter->main = 'admin.start';
		}

        # 刷新任何现有输出
        if (ob_get_length() > 0) {
            $this->response->write(ob_get_clean());
        }
		
        # 启动缓冲区
        ob_start();

        # 初始化插件/执行插件应用
        Plugin::init($this->config->plugins);

		# 初始化错误处理
		Error::init();
    }

    public function execute()
    {
		# 增加tag_page
        if(!Router::exists('tag_page'))
		{
			$post = Router::get('tag');
			$url = $post->url;
			$action = $post->action;
			$widget = $post->widget;
			$method = $post->method ? $post->method . ' ' : '';
		    Router::map("tag_page $method$url/page/{page:\d+}", $widget, $action, 'tag');
        };

		# 增加search_page
        if(!Router::exists('search_page'))
		{
			$post = Router::get('search');
			$url = $post->url;
			$action = $post->action;
			$widget = $post->widget;
			$method = $post->method ? $post->method . ' ' : '';
		    Router::map("search_page $method$url/page/{page:\d+}", $widget, $action, 'search');
        };
		
		# 增加author_page
        if(!Router::exists('author_page'))
		{
			$post = Router::get('author');
			$url = $post->url;
			$action = $post->action;
			$widget = $post->widget;
			$method = $post->method ? $post->method . ' ' : '';
		    Router::map("author_page $method$url/page/{page:\d+}", $widget, $action, 'author');
        };

		# 增加index_page
        if(!Router::exists('index_page'))
		{
			$post = Router::get('index');
			$url = rtrim($post->url, '/');
			$action = $post->action;
			$widget = $post->widget;
			$method = $post->method ? $post->method . ' ' : '';
		    Router::map("index_page $method$url/page/{page:\d+}", $widget, $action, 'index');
        };		
		
		# 增加category_page
        if(!Router::exists('category_page'))
		{
			$post = Router::get('category');
			$url = $post->url;
			$action = $post->action;
			$widget = $post->widget;
			$method = $post->method ? $post->method . ' ' : '';
		    Router::map("category_page $method$url/page/{page:\d+}", $widget, $action, 'category');
        };

        # 路由调度
        Router::dispatch();

		# 输出缓冲
        add_action( $this->parameter->main . '.after', function () 
		{
			# 停止框架并输出当前响应
            if (!$this->response->sent()) {
                $this->response->write(ob_get_clean())->send();
            }
        });
	}

    /**
     * 导入路由配置
     *
     * @access public
     * @param mixed $widget	 所属调用的组件
     * @param mixed  $url  路由地址
     * @param string $method 请求类型 
     * @return false|array
     */
    public static function _import($widget, $url, $method = NULL)
    {
		$rule = Router::match($method ?? Request::instance()->method(), $url);
		$current = Router::current();
		
		if ($rule) {
			if($rule->name != $current->name && $current->widget == $rule->widget){
				
				$widget->request->setParams($rule->params);
				$widget->parameter($rule->vars);
				return $rule;
			}
		}
		return false;
    }

    /**
     * 输出标题
     *
     * @param mixed $widget	 所属调用的组件
     * @param mixed $defines
     * @param string $before
     * @param string $end
     * @return string
     */
    public static function _title($widget, $defines = NULL, $before = ' &raquo; ', $end = '')
    {
	    $title = $widget->parameter->title;
	    $type = $widget->parameter->main;
        if ($title) {
            $define = '%s';
            if (is_array($defines) && !empty($defines[$type])) {
                $define = $defines[$type];
            }

            echo $before . sprintf($define, $title) . $end;
        }
	}

    /**
     * 输出关键字
     *
     * @param mixed $widget	 所属调用的组件
     * @param string $split
     * @param string $default
     * @return string
     */
    public static function _keywords($widget, $split = ',', $default = '')
    {
	    $keywords  = $widget->parameter->keywords;
		echo empty($keywords)
			? $default 
			: str_replace(',', $split, htmlspecialchars($keywords));
	}

    /**
     * 输出描述
     *
     * @param mixed $widget	 所属调用的组件
     * @param string $default
     * @return string
     */
    public static function _description($widget, $default = '')
    {
	    $description  = $widget->parameter->description;
		echo empty($description) 
			? $default 
			: htmlspecialchars($description);
	}

    /**
     * 输出分页
     *
     * @param mixed $widget	 所属调用的组件
     * @param array $template 展现配置信息
     * @param boolean $return 是否返回
     * @return void
     */
    public static function _pagination($widget, $template = [], $echo = true)
    {
		if(is_bool($template)){
		    $echo = $template;
			$template = [];
		}
			
        extract($widget->parameter->many('page', 'pageSize', 'total', 'pageUrl'));

        if ($total <= $pageSize) return;
			
        $result = Pager::make($page, $pageSize, $total, $pageUrl)
			->config($template)
		    ->html();

        if ($echo) {
            echo $result;
        } else {
            return $result;
        }
	}

    /**
     * 允许上传的文件类型
     *
     * @return array
     */
    public static function _allowedFileTypes($widget): array
    {
        $attachmentTypesResult = [];
		$attachmentTypes = $widget->config->attachmentTypes;
		
        if (null != $attachmentTypes) {
            $attachmentTypes = str_replace(
                ['@image@', '@media@', '@doc@'],
                [
                    'gif,jpg,jpeg,png,tiff,bmp,webp', 'mp3,mp4,mov,wmv,wma,rmvb,rm,avi,flv,ogg,oga,ogv',
                    'txt,doc,docx,xls,xlsx,ppt,pptx,zip,rar,pdf'
                ],
                $attachmentTypes
            );

            $attachmentTypesResult = array_unique(array_map('trim', preg_split("/(,|\.)/", $attachmentTypes)));
        }

        return $attachmentTypesResult;
    }

    /**
     * 获取皮肤文件
     *
     * @param mixed $widget	 所属调用的组件
	 * @param string $file
     * @param string $theme
     * @return string
     */
    public static function _themeFile($widget, $file = '', $theme = '')
    {
        if (empty($theme)) {
            $theme = $widget->config->theme;
        }
        return W3_TEMPLATE_DIR . $theme . '/' . trim($file, '/');
	}

    /**
     * 获取相对于应用程序的URL
     *
     * @param mixed $widget	 所属调用的组件
	 * @param string $path
     * @return string
     */
    public static function _asset($widget, $path = '')
    {
        if (!empty($path)) {

            # 搜索 W3_ROOT_DIR
            if (strpos($path, W3_ROOT_DIR) === 0) {
                $path = substr($path, strlen(W3_ROOT_DIR));
            }
            $path = str_replace('\\', '/', $path);
            $path = $widget->config->siteUrl . '/'. trim($path, '/') . '/';
        }
        return $path;
	}

    /**
     * 布局
     *
     * @param mixed $widget	 所属调用的组件
	 * @param boolean $echo 是否返回
     * @return mixed
     */
    public static function _layout($widget, $echo = true)
    {
        if ($echo) {
            echo $widget->layout->render();
        } else {
            return $widget->layout->render();
        }
	}

    /**
     * 全局变量数据  (!)
     *
     * @param mixed $widget	 所属调用的组件
	 * @param string $name  要检索的选项的名称
	 * @param boolean $echo 是否返回
     * @return mixed
     */
    public static function _config($widget, $name, $echo = true)
    {
        $result = $widget->config->{$name};
        if ($echo) {
            echo $result;
        } else {
            return $result;
        }
	}

    /**
     * 检索资源的URL
     *
     * @param mixed $widget	 所属调用的组件
	 * @param string $url  检索网址
	 * @param boolean $echo 是否返回
     * @return mixed
     */
    public static function _assetUrl($widget, $url = '', $echo = true)
    {
		if(is_bool($url)){
			$echo = $url;
			$url = '';
		}
		
        $result = $widget->config->assetUrl . (!empty($url) ? ltrim($url, '/') : '');
        if ($echo) {
            echo $result;
        } else {
            return $result;
        }
	}

    /**
     * 网站地址
     *
     * @param mixed $widget	 所属调用的组件
	 * @param string $url  网址
	 * @param boolean $echo 是否返回
     * @return mixed
     */
    public static function _siteUrl($widget, $url = '', $echo = true)
    {
		if(is_bool($url)){
			$echo = $url;
			$url = '';
		}
		
        $result = $widget->config->siteUrl . (!empty($url) ? '/' . trim($url, '/') . '/' : '');
        if ($echo) {
            echo $result;
        } else {
            return $result;
        }
	}

    /**
     * cdn链接
     *
     * @param mixed $widget	 所属调用的组件
	 * @param boolean $echo 是否返回
     * @return mixed
     */
    public static function _cdnUrl($widget, $url = '', $echo = true)
    {
		if(is_bool($url)){
			$echo = $url;
			$url = '';
		}

        $result = $widget->config->cdnUrl . (!empty($url) ? '/' . trim($url, '/') : '');
        if ($echo) {
            echo $result;
        } else {
            return $result;
        }
	}

    /**
     * 上载文件链接
     *
     * @param mixed $widget	 所属调用的组件
	 * @param boolean $echo 是否返回
     * @return mixed
     */
    public static function _uploadUrl($widget, $echo = true)
    {
        $result = Router::buildUrl('upload');
        if ($echo) {
            echo $result;
        } else {
            return $result;
        }
	}

    /**
     * 登录链接
     *
     * @param mixed $widget	 所属调用的组件
	 * @param boolean $echo 是否返回
     * @return mixed
     */
    public static function _loginUrl($widget, $echo = true)
    {
        $result = Router::buildUrl('login');
        if ($echo) {
            echo $result;
        } else {
            return $result;
        }
	}

    /**
     * 退出链接
     *
     * @param mixed $widget	 所属调用的组件
	 * @param boolean $echo 是否返回
     * @return mixed
     */
    public static function _logoutUrl($widget, $echo = true)
    {
        $result = Router::buildUrl('logout');
        if ($echo) {
            echo $result;
        } else {
            return $result;
        }
	}

    /**
     * 注册链接
     *
     * @param mixed $widget	 所属调用的组件
	 * @param boolean $echo 是否返回
     * @return mixed
     */
    public static function _registerUrl($widget, $echo = true)
    {
        $result = Router::buildUrl('register');
        if ($echo) {
            echo $result;
        } else {
            return $result;
        }
	}

    /**
     * 后台链接
     *
     * @param mixed $widget	 所属调用的组件
	 
	 * @param string $url  网址: tag/1 | tag/1?tid=2
	 * @param string|[] $extra 附加参数
	 * @param boolean $echo 是否返回
     * @return mixed
     */
    public static function _adminUrl($widget, $url = '', $extra = '', $echo = true)
    {
		$num = func_num_args();

        switch ($num) 
		{
            case 2:
			    if(is_bool($url)){
					$echo = $url;
                    $url = ''; 
				}

            break;
            case 3:
			    if(is_bool($extra)){
					$echo = $extra;
				    $extra = ''; 
				}

            break;
        }

        $result = Router::buildUrl('admin', ['permalink' => $url], $extra);
        if ($echo) {
            echo $result;
        } else {
            return $result;
        }
	}

    /**
     * 当找不到URL时，发送HTTP 404响应
     *
     * @param mixed $widget	 所属调用的组件
	 * @param string $msg
     * @return mixed
     */
    public static function _notFound($widget, $msg = NULL)
    {
        !isset($msg) && $msg = 
            '<h1>404 Not Found</h1>'.
            '<h3>The page you have requested could not be found.</h3>'.
            str_repeat(' ', 512);
		
        $widget->response
            ->clear()
            ->code(404)
            ->write($msg)
            ->send();
		exit();
	}

    /**
     * 发送JSON响应
     *
     * @param mixed $widget	 所属调用的组件
     * @param mixed $data JSON data
     * @param int $code HTTP status code
     * @param bool $encode Whether to perform JSON encoding
     * @param string $charset Charset
     * @param int $option Bitmask Json constant such as JSON_HEX_QUOT
     * @throws Exception
     */
    public static function _throwJson(
	    $widget, 
        $data,
        $code = 200,
        $encode = true,
        $charset = 'utf-8',
        $option = NULL
	){
        $widget->response->json($data, $code, $encode, $charset, $option);
	}

    /**
     * 返回来路
     *
     * @param string|null $suffix 附加地址
     * @param string|null $default 默认来路
     */
    public static function _goBack($widget, ?string $suffix = null, ?string $default = null)
    {
        //获取来源  
        $referer = $widget->request->referer($default);

        //判断来源
        if (!empty($referer)) {
            // ~ fix Issue 38
            if (!empty($suffix)) {
                $parts = parse_url($referer);
                $myParts = parse_url($suffix);

                if (isset($myParts['fragment'])) {
                    $parts['fragment'] = $myParts['fragment'];
                }

                if (isset($myParts['query'])) {
                    $args = [];
                    if (isset($parts['query'])) {
                        parse_str($parts['query'], $args);
                    }

                    parse_str($myParts['query'], $currentArgs);
                    $args = array_merge($args, $currentArgs);
                    $parts['query'] = http_build_query($args);
                }

                $referer = Util::buildUrl($parts);
            }
        }
		
		$widget->redirect($referer ?? '/');
    }

    /**
     * 将当前请求重定向到其他URL
     *
     * @param mixed $widget	 所属调用的组件
     * @param string $url URL
     * @param int $code HTTP状态代码
     * @return void
     */
    public static function _redirect($widget, $url, $code = 303)
    {
        $widget->response
            ->clear()
            ->code($code)
            ->header('Location', $url)
            ->send();
	    exit();
	}

    /**
     * 提示框
     *
     * @param mixed $widget	 所属调用的组件
     * @param string|array $msg 响应消息|消息数据
     * @param string|null $type 提示类型
     * @return void
     */
    public static function _notice($widget, $value, ?string $type = 'success')
    {
        $notice = [];
		$notice['type'] = $type;
		$notice['value'] = is_array($value) ? array_values($value) : [$value];
		$notice = Json::encode($notice);
		strlen($notice) > 2048 && 
		$notice = '{ "type": "error", "value": [ "cookie存储数据超过最大字符数" ] }';
        Cookie::set(
            '__w3_notice',
            $notice
        );
	}
	
    /**
     * 提示框响应
     *
     * @return void
     */
    public static function _doNotice($widget)
    {
		if(!Cookie::has('__w3_notice')){
			return ;
		}

		# 加载jquery
		add_script('jquery', $widget->assetUrl('js/jquery-1.10.2.min.js', false), true);
		
		# 加载cookie
		add_script('cookie', $widget->assetUrl('js/jquery.cookie.js', false), ['jquery'], true);
		
		$prefix = Cookie::prefix();
		$path = Cookie::path();
		$tip = __('提示');
		
        $js = <<<EOF
$(document).ready(function() {
    var prefix = '$prefix',
	    //(new Function("return " + $.cookie(prefix + '__w3_notice')))(),
        notice = $.parseJSON($.cookie(prefix + '__w3_notice')),
        path = '$path',
		svg = {
			success : '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check-circle"><path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path><polyline points="22 4 12 14.01 9 11.01"></polyline></svg>',
			notice : '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-alert-circle"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="12"></line><line x1="12" y1="16" x2="12.01" y2="16"></line></svg>',
			error : '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-circle"><circle cx="12" cy="12" r="10"></circle><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>',
		};
        if (!!notice && 'success|notice|error'.indexOf(notice.type) >= 0) {
			var p = $('<div class="w3-message w3-fade w3-' + notice.type + '">'
			        + '   <div class="w3-message-content">'
                    + '       <div class="w3-message-tip">'+ svg[notice.type] +'</div>'
                    + '       <div class="w3-message-body">' + notice.value.join('</br>') + '</div>'
                    + '   </div>'
					+ '</div>');
            p.prependTo(document.body);
			
			var width = p.outerWidth();
			p.width(width + 20).show().css("marginLeft",- Math.round(width/2));

            setTimeout(function () {
		        p.addClass('w3-show');
                setTimeout(function () {
		            p.removeClass('w3-show');
                }, 5000);
            }, 300);
        }
        if (0 && notice.highlight) {
			for(var i=0;i<5;i++){
                $('#' + notice.highlight).fadeOut(300).fadeIn(300);
			}
        }
        $.cookie(prefix + '__w3_notice', null, {path : path});
	});
EOF;
		
		# 加载script
		add_script(
		   'notice', $js, 
	        ['jquery'],
		    true
		);

        $css = <<<EOF
.w3-fade {
  -webkit-transition: opacity 0.15s linear;
  -o-transition: opacity 0.15s linear;
  transition: opacity 0.15s linear;
}
.w3-fade:not(.w3-show) {
  opacity: 0;
}
.w3-message{display: none;
   position:fixed;
   left: 50%;
   top: 0;
   z-index: 99;
 }
.w3-message-content{
    position: relative;
    display: flex;
	margin: 0.6rem auto;
    max-width: 100%;
	min-width: 160px;
    font-size: 0.875rem;
    background-color: #fff;
    background-clip: padding-box;
    border: 0 solid transparent;
    box-shadow: 0 0.25rem 0.5625rem -0.0625rem rgb(0 0 0 / 3%), 0 0.275rem 1.25rem -0.0625rem rgb(0 0 0 / 5%);
    border-radius: 0.3125rem;
 }
.w3-message.w3-fade .w3-message-content {
  -webkit-transition: -webkit-transform 0.3s ease-out;
  transition: -webkit-transform 0.3s ease-out;
  -o-transition: transform 0.3s ease-out;
  transition: transform 0.3s ease-out;
  transition: transform 0.3s ease-out, -webkit-transform 0.3s ease-out;
  -webkit-transform: translate(0, -50px);
      -ms-transform: translate(0, -50px);
          transform: translate(0, -50px);
}
.w3-message.w3-show .w3-message-content {
  -webkit-transform: none;
      -ms-transform: none;
          transform: none;
}
.w3-message-tip {
	display: flex;
    align-items: center;
    padding: 0.5rem;
    color: #fff;
    background-color: #42d697;
    background-clip: padding-box;
    border-bottom: 0 solid rgba(0,0,0,.05);
    border-top-left-radius: 0.3125rem;
    border-bottom-left-radius: 0.3125rem;
}

.w3-message-body{display: flex;
   padding: 0.75rem;
   word-wrap: break-word;
   font-size: 16px;
   color: #293951;
 }

.w3-error .w3-message-tip{background: #f12559;color: #fef6f8; }
.w3-notice .w3-message-tip{background: #fcd53b;color: #fefcf6; }
.w3-success .w3-message-tip{background: #39bd7e;color: #f5fefb; }

EOF;
		
        add_style('notice', $css);
	}
}