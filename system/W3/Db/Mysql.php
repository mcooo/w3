<?php

namespace W3\Db;

use W3\Config;
use PDO;

/**
 * mysql class
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */
class Mysql extends Connector
{
    /**
     * 对象引号过滤
     *
     * @access public
     * @param string $string
     * @return string
     */
    public function quote(string $string): string
    {
        return '`' . $string . '`';
    }

    /**
     * 判断适配器是否可用
     *
     * @access public
     * @return boolean
     */
    public static function isAvailable(): bool
    {
        return parent::isAvailable() && in_array('mysql', PDO::getAvailableDrivers());
    }
	
    /**
     * 数据库连接函数
     *
     * @param $type master | slaver
     * @throws Db_Exception
     * @return PDO
     */
    public function connect(Config $config): PDO
	{
        $pdo = new PDO(
			!empty($config->dsn)
				? $config->dsn
				: "mysql:dbname={$config->dbname};host={$config->host};port={$config->port}",
				$config->user,
				$config->password
		);
			
        $pdo->setAttribute(\PDO::ATTR_TIMEOUT, 1);

		# 使用客户端的缓冲区
        $pdo->setAttribute(\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
		
		# 错误报告模式并不是强制和必须的，但建议你还是添加它。
		# 通过这种方式，脚本在出问题的时候不会被一个致命错误终止，而是抛出PDO Exceptions，这就给了开发者机会去捕获这个错误
	    $pdo->setAttribute(\PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		# 若使用php 5.3.6+, 请在在PDO的DIRECTORY_SEPARATORN中指定charset属性, 在DIRECTORY_SEPARATORN中指定charset是无效的, 同时set names <charset>的执行是必不可少的。
        $pdo->exec("SET NAMES '{$config->charset}'");
		return $pdo;
    }
}
