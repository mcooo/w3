<?php

namespace W3;

!defined('W3_ROOT_DIR') AND exit;

/**
 * 全局统计组件
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */
class Stat extends Widget
{
    /**
     * 初始化
     *
     * @access protected
     * @return void
     */
    protected function init()
    {	
		# 定义变量默认数据
        $this->parameter([

			# 设置主体
			'main' => 'stat'
			
		], true);
	}

    /**
     * 获取所有的附件数目
     *
     * @access protected
     * @return integer
     */
    protected function ___attachsNum()
    {
        return $this->db->select(['COUNT(*)' => 'num'])
		    ->from('table.attachs')
            ->object()
			->num;
    }
	
    /**
     * 获取所有的用户数目
     *
     * @access protected
     * @return integer
     */
    protected function ___usersNum()
    {
        return $this->db->select(['COUNT(*)' => 'num'])
		    ->from('table.users')
            ->object()
			->num;
    }

    /**
     * 获取已发布的文章数目
     *
     * @return integer
     */
    protected function ___publishedPostNum(): int
    {
        return $this->db->select(['COUNT(*)' => 'num'])
		    ->from('table.contents')
			->where('table.contents.type = ?', 'post')
			->where('table.contents.status = ?', 'publish')
            ->object()
			->num;
    }

    /**
     * 获取待审核的文章数目
     *
     * @return integer
     */
    protected function ___waitingPostNum(): int
    {
        return $this->db->select(['COUNT(*)' => 'num'])
		    ->from('table.contents')
			->where('table.contents.type = ?', 'post')
			->where('table.contents.status = ?', 'waiting')
            ->object()
			->num;
    }

    /**
     * 获取草稿文章数目
     *
     * @return integer
     */
    protected function ___draftPostNum(): int
    {
        return $this->db->select(['COUNT(*)' => 'num'])
		    ->from('table.contents')
			->where('table.contents.type = ?', 'post')
			->where('table.contents.status = ?', 'draft')
            ->object()
			->num;
    }

    /**
     * 获取当前用户已发布的文章数目
     *
     * @return integer
     */
    protected function ___myPublishedPostNum(): int
    {
        return $this->db->select(['COUNT(*)' => 'num'])
		    ->from('table.contents')
			->where('table.contents.type = ?', 'post')
			->where('table.contents.status = ?', 'publish')
			->where('table.contents.uid = ?', $this->auth->uid)
            ->object()
			->num;
    }

    /**
     * 获取当前用户待审核文章数目
     *
     * @return integer
     */
    protected function ___myWaitingPostNum(): int
    {
        return $this->db->select(['COUNT(*)' => 'num'])
		    ->from('table.contents')
			->where('table.contents.type = ?', 'post')
			->where('table.contents.status = ?', 'waiting')
			->where('table.contents.uid = ?', $this->auth->uid)
            ->object()
			->num;
    }

    /**
     * 获取当前用户草稿文章数目
     *
     * @return integer
     */
    protected function ___myDraftPostNum(): int
    {
        return $this->db->select(['COUNT(*)' => 'num'])
		    ->from('table.contents')
			->where('table.contents.type = ?', 'post')
			->where('table.contents.status = ?', 'draft')
			->where('table.contents.uid = ?', $this->auth->uid)
            ->object()
			->num;
    }

    /**
     * 获取当前用户已发布的文章数目
     *
     * @return integer
     */
    protected function ___currentPublishedPostNum(): int
    {
        return $this->db->select(['COUNT(*)' => 'num'])
		    ->from('table.contents')
			->where('table.contents.type = ?', 'post')
			->where('table.contents.status = ?', 'publish')
			->where('table.contents.uid = ?', $this->request->filter('int')->uid)
            ->object()
			->num;
    }

    /**
     * 获取当前用户待审核文章数目
     *
     * @return integer
     */
    protected function ___currentWaitingPostNum(): int
    {
        return $this->db->select(['COUNT(*)' => 'num'])
		    ->from('table.contents')
			->where('table.contents.type = ?', 'post')
			->where('table.contents.status = ?', 'waiting')	
			->where('table.contents.uid = ?', $this->request->filter('int')->uid)
            ->object()
			->num;
    }

    /**
     * 获取当前用户草稿文章数目
     *
     * @return integer
     */
    protected function ___currentDraftPostNum(): int
    {
        return $this->db->select(['COUNT(*)' => 'num'])
		    ->from('table.contents')
			->where('table.contents.type = ?', 'post')
			->where('table.contents.status = ?', 'draft')
			->where('table.contents.uid = ?', $this->request->filter('int')->uid)
            ->object()
			->num;
    }

    /**
     * 获取已发布页面数目
     *
     * @return integer
     */
    protected function ___publishedPageNum(): int
    {
        return $this->db->select(['COUNT(*)' => 'num'])
		    ->from('table.contents')
			->where('table.contents.type = ?', 'page')
			->where('table.contents.status = ?', 'publish')			
            ->object()
			->num;
    }

    /**
     * 获取草稿页面数目
     *
     * @return integer
     */
    protected function ___draftPageNum(): int
    {
        return $this->db->select(['COUNT(*)' => 'num'])
		    ->from('table.contents')
			->where('table.contents.type = ?', 'page')
			->where('table.contents.status = ?', 'draft')
            ->object()
			->num;
    }

    /**
     * 获取当前用户草稿页面数目
     *
     * @return integer
     */
    protected function ___myDraftPageNum(): int
    {
        return $this->db->select(['COUNT(*)' => 'num'])
		    ->from('table.contents')
			->where('table.contents.type = ?', 'page')
			->where('table.contents.status = ?', 'draft')
			->where('table.contents.uid = ?', $this->auth->uid)
            ->object()
			->num;
    }

    /**
     * 获取当前用户待审核页面数目
     *
     * @return integer
     */
    protected function ___myWaitingPageNum(): int
    {
        return $this->db->select(['COUNT(*)' => 'num'])
		    ->from('table.contents')
			->where('table.contents.type = ?', 'page')
			->where('table.contents.status = ?', 'waiting')
			->where('table.contents.uid = ?', $this->auth->uid)
            ->object()
			->num;
    }

    /**
     * 获取当前用户草稿文章数目
     *
     * @return integer
     */
    protected function ___currentDraftPageNum(): int
    {
        return $this->db->select(['COUNT(*)' => 'num'])
		    ->from('table.contents')
			->where('table.contents.type = ?', 'page')
			->where('table.contents.status = ?', 'draft')
			->where('table.contents.uid = ?', $this->request->filter('int')->uid)
            ->object()
			->num;
    }

    /**
     * 获取当前用户草稿文章数目
     *
     * @return integer
     */
    protected function ___currentWaitingPageNum(): int
    {
        return $this->db->select(['COUNT(*)' => 'num'])
		    ->from('table.contents')
			->where('table.contents.type = ?', 'page')
			->where('table.contents.status = ?', 'waiting')
			->where('table.contents.uid = ?', $this->request->filter('int')->uid)
            ->object()
			->num;
    }

    /**
     * 获取当前用户草稿文章数目
     *
     * @return integer
     */
    protected function ___waitingPageNum(): int
    {
        return $this->db->select(['COUNT(*)' => 'num'])
		    ->from('table.contents')
			->where('table.contents.type = ?', 'page')
			->where('table.contents.status = ?', 'waiting')
            ->object()
			->num;
    }


    /**
     * 获取当前用户草稿文章数目
     *
     * @return integer
     */
    protected function ___hiddenPageNum(): int
    {
        return $this->db->select(['COUNT(*)' => 'num'])
		    ->from('table.contents')
			->where('table.contents.type = ?', 'page')
			->where('table.contents.status = ?', 'hidden')
            ->object()
			->num;
    }

    /**
     * 获取当前用户待审核文章数目
     *
     * @return integer
     */
    protected function ___myHiddenPageNum(): int
    {
        return $this->db->select(['COUNT(*)' => 'num'])
		    ->from('table.contents')
			->where('table.contents.type = ?', 'page')
			->where('table.contents.status = ?', 'hidden')
			->where('table.contents.uid = ?', $this->auth->uid)
            ->object()
			->num;
    }

    /**
     * 获取当前用户草稿文章数目
     *
     * @return integer
     */
    protected function ___currentHiddenPageNum(): int
    {
        return $this->db->select(['COUNT(*)' => 'num'])
		    ->from('table.contents')
			->where('table.contents.type = ?', 'page')
			->where('table.contents.status = ?', 'hidden')
			->where('table.contents.uid = ?', $this->request->filter('int')->uid)
            ->object()
			->num;
    }

    /**
     * 获取当前用户草稿文章数目
     *
     * @return integer
     */
    protected function ___hiddenPostNum(): int
    {
        return $this->db->select(['COUNT(*)' => 'num'])
		    ->from('table.contents')
			->where('table.contents.type = ?', 'post')
			->where('table.contents.status = ?', 'hidden')
            ->object()
			->num;
    }

    /**
     * 获取当前用户待审核文章数目
     *
     * @return integer
     */
    protected function ___myHiddenPostNum(): int
    {
        return $this->db->select(['COUNT(*)' => 'num'])
		    ->from('table.contents')
			->where('table.contents.type = ?', 'post')
			->where('table.contents.status = ?', 'hidden')
			->where('table.contents.uid = ?', $this->auth->uid)
            ->object()
			->num;
    }

    /**
     * 获取当前用户草稿文章数目
     *
     * @return integer
     */
    protected function ___currentHiddenPostNum(): int
    {
        return $this->db->select(['COUNT(*)' => 'num'])
		    ->from('table.contents')
			->where('table.contents.type = ?', 'post')
			->where('table.contents.status = ?', 'hidden')
			->where('table.contents.uid = ?', $this->request->filter('int')->uid)
            ->object()
			->num;
    }

    /**
     * 获取当前显示的评论数目
     *
     * @return integer
     */
    protected function ___publishedCommentsNum(): int
    {
        return $this->db->select(['COUNT(*)' => 'num'])
		    ->from('table.comments')
			->where('table.comments.status = ?', 'approved')
            ->object()
			->num;
    }

    /**
     * 获取当前待审核的评论数目
     *
     * @return integer
     */
    protected function ___waitingCommentsNum(): int
    {
        return $this->db->select(['COUNT(*)' => 'num'])
		    ->from('table.comments')
			->where('table.comments.status = ?', 'waiting')
            ->object()
			->num;
    }

    /**
     * 获取当前垃圾评论数目
     *
     * @return integer
     */
    protected function ___spamCommentsNum(): int
    {
        return $this->db->select(['COUNT(*)' => 'num'])
		    ->from('table.comments')
			->where('table.comments.status = ?', 'spam')
            ->object()
			->num;
    }

    /**
     * 获取当前用户显示的评论数目
     *
     * @return integer
     */
    protected function ___myPublishedCommentsNum(): int
    {
        return $this->db->select(['COUNT(*)' => 'num'])
		    ->from('table.comments')
			->where('table.comments.status = ?', 'approved')
			->where('table.comments.ownerId = ?', $this->auth->uid)
            ->object()
			->num;
    }

    /**
     * 获取当前用户待审核的评论数目
     *
     * @return integer
     */
    protected function ___myWaitingCommentsNum(): int
    {
        return $this->db->select(['COUNT(*)' => 'num'])
		    ->from('table.comments')
			->where('table.comments.status = ?', 'waiting')
			->where('table.comments.ownerId = ?', $this->auth->uid)
            ->object()
			->num;
    }

    /**
     * 获取当前用户垃圾评论数目
     *
     * @return integer
     */
    protected function ___mySpamCommentsNum(): int
    {
        return $this->db->select(['COUNT(*)' => 'num'])
		    ->from('table.comments')
			->where('table.comments.status = ?', 'spam')
			->where('table.comments.ownerId = ?', $this->auth->uid)
            ->object()
			->num;
    }

    /**
     * 获取当前文章的评论数目
     *
     * @return integer
     */
    protected function ___currentCommentsNum(): int
    {
        return $this->db->select(['COUNT(*)' => 'num'])
		    ->from('table.comments')
			->where('table.comments.cid = ?', $this->request->filter('int')->cid)
            ->object()
			->num;
    }

    /**
     * 获取当前文章显示的评论数目
     *
     * @return integer
     */
    protected function ___currentPublishedCommentsNum(): int
    {
        return $this->db->select(['COUNT(*)' => 'num'])
		    ->from('table.comments')
			->where('table.comments.cid = ?', $this->request->filter('int')->cid)
			->where('table.comments.status = ?', 'approved')
            ->object()
			->num;
    }

    /**
     * 获取当前文章待审核的评论数目
     *
     * @return integer
     */
    protected function ___currentWaitingCommentsNum(): int
    {
        return $this->db->select(['COUNT(*)' => 'num'])
		    ->from('table.comments')
			->where('table.comments.cid = ?', $this->request->filter('int')->cid)
			->where('table.comments.status = ?', 'waiting')
            ->object()
			->num;
    }

    /**
     * 获取当前文章垃圾评论数目
     *
     * @return integer
     */
    protected function ___currentSpamCommentsNum(): int
    {
        return $this->db->select(['COUNT(*)' => 'num'])
		    ->from('table.comments')
			->where('table.comments.cid = ?', $this->request->filter('int')->cid)
			->where('table.comments.status = ?', 'spam')
            ->object()
			->num;
    }

    /**
     * 获取分类数目
     *
     * @return integer
     */
    protected function ___categoriesNum(): int
    {
        return $this->db->select(['COUNT(*)' => 'num'])
		    ->from('table.metas')
			->where('table.metas.type = ?', 'category')
            ->object()
			->num;
    }

    /**
     * 获取标签数目
     *
     * @return integer
     */
    protected function ___tagsNum(): int
    {
        return $this->db->select(['COUNT(*)' => 'num'])
		    ->from('table.metas')
			->where('table.metas.type = ?', 'tag')
            ->object()
			->num;
    }
	
}
