<?php

namespace W3;

/**
 * 纯数据抽象组件
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */
abstract class Widget extends Base
{
    /**
     * init db
     */
    protected const INIT_DB = 0b0001;

    /**
     * init auth widget
     */
    protected const INIT_AUTH = 0b0010;
	
    /**
     * init Config
     */
    protected const INIT_CONFIG = 0b0100;

    /**
     * init Config
     */
    protected const INIT_LAYOUT = 0b1000;


    /**
     * init all
     */
    protected const INIT_ALL = 0b1111;

    /**
     * init none
     */
    protected const INIT_NONE = 0;
	
    /**
     * 全局寄存器
     *
     * @access public
     * @var W3\Config
     */
    public $config;
	
    /**
     * 数据库对象
     *
     * @access public
     * @var W3\Db
     */
    public $db;

    /**
     * 当前登录用户
     *
     * @access public
     * @var W3\Auth
     */
    public $auth;
	
    /**
     * 版面布局类
     *
     * @var Element
     */
    public $layout;
	
    /**
     * init method
     */
    protected function boot()
    {
        $components = self::INIT_ALL;

        $this->initComponents($components);

        if ($components != self::INIT_NONE) {
            $this->db = Db::instance();
        }

        if ($components & self::INIT_AUTH) {
            $this->auth = $this->widget('W3\Auth');
        }

        if ($components & self::INIT_CONFIG) {
            $this->config = Config::instance();
        }

        if ($components & self::INIT_LAYOUT) {
            $this->layout = Element::make();
        }

		$this->init();
    }

    /**
     * @param int $components
     */
    protected function initComponents(int &$components)
    {
    }
	
    /**
     * 初始化
     *
     * @access protected
     * @return void
     */
    protected function init()
    {
    }
	
    protected function require(string $file)
    {
        require $file;
	}
}
