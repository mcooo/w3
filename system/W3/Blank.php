<?php

namespace W3;

/**
 * 处理空对象
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */
class Blank
{
    /**
     * 单例句柄
     *
     * @access protected
     * @var Blank
     */
    protected static $_instance;
	
    /**
     * 单例实例
     *
     * @return Blank
     */
    public static function instance(): Blank
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     * 创建新实例
     *
     * @access public
     * @return Blank
     */
    public static function make(): Blank
    {
        return new static();
    }
	
    public function __get($key)
    {
        return;
    }

    /**
     * 所有方法请求直接返回
     *
     * @access public
     * @param string $name 方法名
     * @param array $args 参数列表
     * @return void
     */
    public function __call(string $name, array $args)
    {
        return;
    }
}
