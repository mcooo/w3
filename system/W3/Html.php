<?php

namespace W3;

use W3\Element\A;
use W3\Element\Div;
use W3\Element\File;
use W3\Element\I;
use W3\Element\Img;
use W3\Element\Label;
use W3\Element\Span;
use W3\Element\Form;
use W3\Element\Table;
use W3\Element\Button;
use W3\Element\Checkbox;
use W3\Element\Csrf;
use W3\Element\Input;
use W3\Element\Radio;
use W3\Element\Select;
use W3\Element\Textarea;
use W3\Element\Search;

class Html
{
    /**
     * @param string|null $name
     * @param string|null $value
     *
     * @return W3\Element\Table
     */
    public static function table($name = NULL, $value = NULL)
    {
		return Table::make($name, $value);
    }
	
    /**
     * @param string|null $name
     * @param string|null $value
     *
     * @return W3\Element\Search
     */
    public static function search($name = NULL, $value = NULL)
    {
		return Search::make($name, $value);
    }

    /**
     * @param string|null $href
     * @param string|null $text
     *
     * @return W3\Element\A
     */
    public static function a(?string $href = null, ?string $contents = null)
    {
		return A::make($href, $contents);
    }

    /**
     * @param string|null $href
     * @param string|null $text
     *
     * @return W3\Element\I
     */
    public static function i($contents = null, $class = NULL)
    {
		return I::make($content, $class);
    }

    /**
     * @param string|null $type
     * @param string|null $text
     *
     * @return W3\Element\Button
     */
    public static function button($contents = null, $class = null)
    {
        return Button::make($contents, $class);
    }

    /**
     * @param string|null $name
     * @param bool $checked
     * @param string|null $value
     *
     * @return W3\Element\Checkbox
     */
    public static function checkbox($name = NULL, array $options = [], $checked = NULL)
    {
        return Checkbox::make($name, $options, $checked);
    }

    /**
     * @param \Spatie\Html\HtmlElement|string|null $contents
     *
     * @return W3\Element\Div
     */
    public static function div($contents = null, $class = NULL)
    {
        return Div::make($content, $class);
    }

    /**
     * @param string|null $type
     * @param string|null $name
     * @param string|null $value
     *
     * @return W3\Element\Input
     */
    public static function input($name = NULL, $value = NULL)
    {
        return Input::make($name, $value);
    }

    /**
     * @param string|null $name
     * @param string|null $value
     *
     * @return W3\Element\Input
     */
    public static function email($name = '', $value = '')
    {
        return Input::make($name, $value)->type('email');
    }

    /**
     * @param string|null $name
     * @param string|null $value
     *
     * @return W3\Element\Input
     */
    public static function date($name = '', $value = '')
    {
        return Input::make($name, $value)->type('date');
    }

    /**
     * @param string|null $name
     * @param string|null $value
     *
     * @return W3\Element\Input
     */
    public static function time($name = '', $value = '')
    {
        return Input::make($name, $value)->type('time');
    }

    /**
     * @param string|null $name
     * @param string|null $value
     *
     * @return W3\Element\Input
     */
    public static function hidden($name = null, $value = null)
    {
        return Input::make($name, $value)->hidden();
    }
	
    /**
     * @param string $tag
     *
     * @return W3\Element
     */
    public static function element($tag, $contents = null)
    {
        return Element::make($tag, $content);
    }

    /**
     * @param string $method
     * @param string|null $action
     *
     * @return W3\Element\Form
     */
    public static function form(?string $action = null, ?int $class = null)
    {
        return Form::make($action, $class);
    }

    /**
     * @param string|null $src
     * @param string|null $alt
     *
     * @return W3\Element\Img
     */
    public static function img($src = null, $class = NULL)
    {
        return Img::make($src, $class);
    }

    /**
     * @param \Spatie\Html\HtmlElement|iterable|string|null $contents
     * @param string|null $for
     *
     * @return W3\Element\Label
     */
    public static function label($contents = null, $class = NULL)
    {
		return Label::make($content, $class);
    }

    /**
     * @param string $email
     * @param string|null $text
     *
     * @return W3\Element\A
     */
    public static function mailto($email, $text = null)
    {
        return A::make('mailto:'.$email, $text);
    }

    /**
     * @param string|null $name
     * @param iterable $options
     * @param string|iterable|null $value
     *
     * @return W3\Element\Select
     */
    public static function select($name = NULL, array $options = NULL, $checked = NULL)
    {
        return Select::make($name, $options, $checked);
    }

    /**
     * @param string|null $value
     *
     * @return W3\Element\Input
     */
    public static function password($name = '', $value = '')
    {
        return Input::make($name, $value)->type('password');
    }

    /**
     * @param string|null $name
     * @param bool $checked
     * @param string|null $value
     *
     * @return W3\Element\Radio
     */
    public static function radio($name = NULL, array $options = NULL, $checked = NULL)
    {
        return Radio::make($name, $options, $checked);
    }

    /**
     * @param \Spatie\Html\HtmlElement|string|null $contents
     *
     * @return W3\Element\Span
     */
    public static function span($contents = null, $class = NULL)
    {
        return Span::make($contents, $class);
    }

    /**
     * @param string|null $text
     *
     * @return W3\Element\Button
     */
    public static function submit($text = null)
    {
		return Button::make($text)->type('submit');
    }

    /**
     * @param string|null $text
     *
     * @return W3\Element\Button
     */
    public static function reset($text = null)
    {
		return Button::make($text)->type('reset');
    }

    /**
     * @param string $number
     * @param string|null $text
     *
     * @return W3\Element\A
     */
    public static function tel($number, $text = null)
    {
        return A::make('tel:'.$number, $text);
    }

    /**
     * @param string|null $name
     * @param string|null $value
     *
     * @return W3\Element\Input
     */
    public static function text($name = null, $value = null)
    {
		return Input::make($name, $value)->type('text');
    }

    /**
     * @param string|null $name
     *
     * @return W3\Element\File
     */
    public static function file($name = NULL, $value = NULL)
    {
        return File::make($name, $value);
    }

    /**
     * @param string|null $name
     * @param string|null $value
     *
     * @return W3\Element\Textarea
     */
    public static function textarea($name = null, $value = null)
    {
        return Textarea::make($name, $value);
    }

    /**
     * @return W3\Element\Input
     */
    public static function token()
    {
		return Input::make('_token', $value)->hidden();
    }



}
