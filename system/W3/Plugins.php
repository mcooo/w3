<?php

namespace W3;

use W3\Element\Form;

/**
 * 插件接口
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */
interface Plugins
{
    /**
     * 执行应用程序
     * @access public
     * @return void
     */
    public static function execute();
	
    /**
     * 启用插件方法,如果启用失败,直接抛出异常
     *
     * @static
     * @access public
     * @return void
     * @throws Plugins_Exception
     */
    public static function activate();

    /**
     * 禁用插件方法,如果禁用失败,直接抛出异常
     *
     * @static
     * @access public
     * @return void
     * @throws Plugins_Exception
     */
    public static function deactivate();

    /**
     * 获取插件配置面板
     *
     * @static
     * @access public
     * @param Form $form 配置面板
     * @return void
     */
    public static function config(Form $form);
}
