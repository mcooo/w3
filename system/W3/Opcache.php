<?php

namespace W3;

/**
 * Opcache操作封装类
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */
class Opcache
{

    protected static $_enabled = NULL;
	
    public static function enabled()
    {
        return extension_loaded('Zend OPcache') && ini_get('opcache.enable');
    }
	
    public static function isAvailable()
    {
        return self::enabled();
    }
    #  获取配置信息
    public static function config()
    {
        if (!self::enabled()) {
            return FALSE;
        }
        return opcache_get_configuration();
    }
    #  获取状态信息
    public static function status()
    {
        if (!self::enabled()) {
            return FALSE;
        }
        return opcache_get_status();
    }
    #  指定某脚本文件字节码缓存失效
    public static function invalid($script)
    {
        if (!self::enabled()) {
            return FALSE;
        }
        return opcache_invalidate($script, true);
    }
    #  重置或清除整个字节码缓存数据
    public static function clean()
    {
        if (!self::enabled()) {
            return FALSE;
        }
        return opcache_reset();
    }
    #  无需运行，就可以编译并缓存脚本
    public static function compile($file)
    {
        if (!self::enabled()) {
            return FALSE;
        }
        return opcache_compile_file($file);
    }
    #  判断某个脚本是否已经缓存到Opcache
    public static function isCached($script)
    {
        if (!self::enabled()) {
            return FALSE;
        }
        return opcache_is_script_cached($script);
    }
}