<?php

namespace W3;

/**
 * 日期处理
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */
class Timer
{
    /**
     * 期望时区偏移
     *
     * @access public
     * @var integer
     */
    public static $timezoneOffset = 0;

    /**
     * 服务器时区偏移
     *
     * @access public
     * @var integer
     */
    public static $serverTimezoneOffset = 0;

    /**
     * 当前的服务器时间戳
     *
     * @access public
     * @var integer
     */
    public static $serverTimeStamp;

    /**
     * 可以被直接转换的时间戳
     *
     * @access public
     * @var integer
     */
    public $timeStamp = 0;

    /**
     * @var string
     */
    public $year;

    /**
     * @var string
     */
    public $month;

    /**
     * @var string
     */
    public $day;

    /**
     * 可以被直接转换的时间戳
     *
     * @access public
     * @var integer
     */
    public $lang = [
		'month_ago'=>'月前',
		'day_ago'=>'天前',
		'hour_ago'=>'小时前',
		'minute_ago'=>'分钟前',
		'second_ago'=>'秒前',
	];

    /**
     * 单例句柄
     *
     * @access protected
     * @var Timer
     */
    protected static $instance;
	
    /**
     * 单例实例
     *
     * @return Timer
     */
    public static function instance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public static function make(?int $time = NULL)
    {
        return new static($time);
    }

    /**
     * 初始化参数
     *
     * @access public
     * @param integer $time 时间戳
     */
    public function __construct(?int $time = NULL)
    {
        $this->timeStamp = (NULL === $time ? self::time() : $time)
		    + (self::$timezoneOffset - self::$serverTimezoneOffset);
		
        $this->year = date('Y', $this->timeStamp);
        $this->month = date('m', $this->timeStamp);
        $this->day = date('d', $this->timeStamp);
    }

    /**
     * 设置当前期望的时区偏移
     *
     * @access public
     *
     * @param integer $offset
     *
     * @return void
     */
    public static function setTimezoneOffset(int $offset)
    {
        self::$timezoneOffset = $offset;
        self::$serverTimezoneOffset = idate('Z');
    }

    /**
     * 获取格式化时间
     *
     * @access public
     * @param string $format 时间格式
     * @return string
     */
    public function format($format): string
    {
        return date($format, $this->timeStamp);
    }

    /**
     * 获取国际化偏移时间
     *
     * @access public
     * @return string
     */
    public function word(): string
    {
        return $this->dateWord($this->timeStamp, self::time() + (self::$timezoneOffset - self::$serverTimezoneOffset));
    }

    /**
     * 获取GMT时间
     *
     * @deprecated
     * @return int
     */
    public static function gmtTime(): int
    {
        return self::time();
    }

    /**
     * 获取服务器时间
     *
     * @return int
     */
    public static function time(): int
    {
        return self::$serverTimeStamp ?: (self::$serverTimeStamp = time());
    }
	
	
    /**
     * 词义化时间
     *
     * @access private
     * @param string $from 起始时间
     * @param string $now 终止时间
     * @return string
     */
    private function dateWord($timestamp, $now)
    {
        $between = $now - $timestamp;
		
	    if($between > 31536000) {
		    return date('Y-n-j', $timestamp);
	    } elseif($between > 2592000) {
		    return floor($between / 2592000).$this->lang['month_ago'];
	    } elseif($between > 86400) {
		    return floor($between / 86400).$this->lang['day_ago'];
	    } elseif($between > 3600) {
		    return floor($between / 3600).$this->lang['hour_ago'];
	    } elseif($between > 60) {
		    return floor($between / 60).$this->lang['minute_ago'];
	    } else {
		    return $between.$this->lang['second_ago'];
	    }
    }
	
}
