<?php

namespace W3\Element;

use W3\Element;

/**
 * 抽象类
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */

abstract class Group extends Element 
{
    /**
     * 表单容器
     *
     * @access public
     * @var Element
     */
    public $container;

    /**
     * 表单标题
     *
     * @access public
     * @var Element
     */
    protected $label;
	
    /**
     * 表单描述
     *
     * @access private
     * @var string
     */
    protected $description;

    /**
     * 表单消息
     *
     * @access protected
     * @var string
     */
    protected $message;	
	
    /**
     * 单例唯一id
     *
     * @access protected
     * @var integer
     */
    protected static $uniqueId = 0;
	
    /**
     * 表单验证器
     *
     * @access public
     * @var array
     */
    public $rules = [];

    /**
     * 表单名称
     *
     * @access public
     * @var string
     */
    public $name;

    /**
     * 表单值
     *
     * @access public
     * @var mixed
     */
    public $value;
	
    /**
     * 创建表单标题
     *
     * @access public
     * @param string $value 标题字符串
     * @return Form_Element
     */
    public function label($value)
    {
        /** 创建标题元素 */
        if (empty($this->label)) {
            $this->label = Label::make(NULL, 'form-label');
            $this->preppend($this->label);
			
			if('input' == $this->tag){
			    ++ self::$uniqueId;
			    $id =  '__form-label_' . self::$uniqueId;
			    $this->label->for($id);
			    $this->id($id);
			}
        }

        $this->label->reset()->set($value);
        return $this;
    }	
	
    /**
     * 设置提示信息
     *
     * @access public
     * @param string $message 提示信息
     * @return Form_Element
     */
    public function message($message)
    {
        if (empty($this->message)) {
            $this->message = Div::make(NULL, 'form-message');
            $this->append($this->message);
        }

        $this->message->reset()->set($message);
        return $this;
    }
	
    /**
     * 设置描述信息
     *
     * @access public
     * @param string $description 描述信息
     * @return Form_Element
     */
    public function description($description)
    {
        /** 创建描述元素 */
        if (empty($this->description)) {
            $this->description = Div::make(NULL, 'form-description');
            $this->append($this->description);
        }

        $this->description->reset()->set($description);
        return $this;
    }
	
    /**
     * 增加验证器
     *
     * @access public
     * @return Form_Element
     */
    public function addRule()
    {
        $this->rules[] = func_get_args();
        return $this;
    }
	
    public function render() 
	{
		return NULL !== $this->container 
		    ? $this->container->set(parent::render())->render() 
			: parent::render();
    }
}