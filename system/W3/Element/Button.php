<?php

namespace W3\Element;

/**
 * Button 帮手类
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */

class Button extends Group
{
    /**
     * 构造函数
     *
     * @access public
     * @param string $name 表单输入项名称
     * @param mixed $value 表单默认值
     * @param array $options 选择项
     * @return void
     */
    public function __construct($value = NULL, $class = NULL)
    {
		$this
		    ->close(false)
		    ->tag('button')
			->type('button')
			->attribute('data-loading-text', 'loading..')
			->addClass($class ?? 'btn btn-primary');
		
        /** 初始化表单值 */
        if (NULL !== $value) {
            $this->value($value);
        }
    }
	
	# submit|reset|button
    public function type($type = 'button')
    {
		$this->attribute('type', $type);
		return $this;
    }
	
    /**
     * @param bool $disabled
     *
     * @return static
     */
    public function disabled($disabled = true)
    {
        return $disabled
            ? $this->attribute('disabled', 'disabled')
            : $this->removeAttribute('disabled');
    }
	
    /**
     * 设置表单元素值
     *
     * @access public
     * @param mixed $value 表单元素值
     * @return Form_Element
     */
    public function value($value)
    {
        $this->value = $value;
		$this->set($value);
        return $this;
    }
}
