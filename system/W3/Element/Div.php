<?php

namespace W3\Element;

use W3\Element;

/**
 * Div 帮手类
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */

class Div extends Element
{
    /**
     * 构造函数
     *
     * @access public
     * @param string $name 表单输入项名称
     * @param mixed $value 表单默认值
     * @param array $options 选择项
     * @return void
     */
    public function __construct($element = NULL, $class = NULL)
    {
		$this->close(false)
		    ->tag('div')
			->set($element)
		    ->addClass($class);
    }
}
