<?php

namespace W3\Element;

use W3\Element;

/**
 * Img 帮手类
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */
class Img extends Element
{

    /**
     * 构造函数
     *
     * @access public
     * @param string $name 表单输入项名称
     * @param mixed $value 表单默认值
     * @param array $options 选择项
     * @return void
     */
    public function __construct($src = NULL, $class = NULL)
    {
		$this->close(true)
		    ->tag('img')
			->src($element)
		    ->addClass($class);
    }

    /**
     * @param string|null $alt
     *
     * @return static
     */
    public function alt($alt)
    {
        return $this->attribute('alt', $alt);
    }

    /**
     * @param string|null $src
     *
     * @return static
     */
    public function src($src)
    {
        return $this->attribute('src', $src);
    }
}
