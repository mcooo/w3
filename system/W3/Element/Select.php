<?php

namespace W3\Element;

use W3\Element;

/**
 * Select 帮手类
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */
class Select extends Group 
{
    /**
     * 选择值
     *
     * @access private
     * @var array
     */
    private $options = [];
	
    /**
     * 构造函数
     *
     * @access public
     * @param string $name 表单输入项名称
     * @param mixed $value 表单默认值
     * @param array $options 选择项
     * @return void
     */
    public function __construct($name = NULL, array $options = [], $checked = NULL)
    {
		$this->name = $name;
		$this->tag('select')
		   ->addClass('custom-control custom-select')
		   ->attribute('name', $name);
		   
        foreach ($options as $value => $label) 
		{
		    $option = Element::make('option', $label)
			    ->attribute('value', $value);
			$this->set($option);
			$this->options[$value] = $option;
        }
		
        /** 初始化表单值 */
        if (NULL !== $checked) {
            $this->value($checked);
        }
    }

    /**
     * setChecked
     *
     * Set form element as checked
     *
     * @param boolean  $checked   Value
     *
     * @return $this;
     */
    public function value($value) 
	{
		$this->value = $value;
		
        foreach ($this->options as $option) 
		{
            $option->removeAttribute('selected');
        }
		
		if(isset($this->options[$value])) {
			$this->value = $value;
		    $this->options[$value]->attribute('selected', 'selected');
		}

        return $this;
    }
	
    /**
     * @param string|null $name
     *
     * @return static
     */
    public function name($name)
    {
        return $this->attribute('name', $name);
    }
	
    /**
     * @param bool $required
     *
     * @return static
     */
    public function required($required = true)
    {
        return $required
            ? $this->attribute('required')
            : $this->removeAttribute('required');
    }
}