<?php

namespace W3\Element;

use W3\Element;

/**
 * Checkbox 帮手类
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */

class Checkbox extends Group 
{

    /**
     * 选择值
     *
     * @access private
     * @var array
     */
    private $options = [];
	
    /**
     * 构造函数
     *
     * @access public
     * @param string $name 表单输入项名称
     * @param mixed $value 表单默认值
     * @param array $options 选择项
     * @return void
     */
    public function __construct($name = NULL, array $options = NULL, $checked = NULL)
    {
		$this->name = $name;

        foreach ($options as $value => $label) 
		{
			++self::$uniqueId;
			$id =  '__checkbox_' . self::$uniqueId;
		    $container = Div::make(null, 'custom-control custom-checkbox');
		    $label = Label::make($label, 'custom-control-label');
			
			# 一般将name设置为name[]
		    $checkbox = Input::make($this->name . '[]', $value)
			    ->id($id)
				->type('checkbox')
				->addClass('custom-control-input');

			$container->set($checkbox)->set($label->for($id));
			$this->options[$value] = $checkbox;
            $this->set($container);
        }
		
        /** 初始化表单值 */
        if (NULL !== $checked) {
            $this->value($checked);
        }
    }
	
	# 单个用only
    public function only() 
	{
        foreach ($this->options as $option) 
		{
            $option->name = str_replace('[]', '', $option->name);
			$option->attribute('name', $option->name);
        }

        return $this;
    }
	
    /**
     * setChecked
     * Set form element as checked
     *
     * @param boolean  $checked   Value
     * @return $this;
     */
    public function value($value) 
	{
		$this->value = $value;
		
		$values = is_array($value) ? $value : [$value];
		
        foreach ($this->options as $option) 
		{
            $option->removeAttribute('checked');
        }
		
        foreach ($values as $value) 
		{
            if (isset($this->options[$value])) {
                $this->options[$value]->attribute('checked', 'true');
            }
        }

        return $this;
    }
}