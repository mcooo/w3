<?php

namespace W3\Element;

use W3\Element;

/**
 * A 帮手类
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */
class A extends Element
{
    /**
     * 构造函数
     *
     * @access public
     * @param string $name 表单输入项名称
     * @param mixed $value 表单默认值
     * @param array $options 选择项
     * @return void
     */
    public function __construct($href = NULL, $text = NULL)
    {
		$this->close(false)
		    ->tag('a')
			->set($text)
		    ->href($href);
    }
	
    /**
     * @param string|null $href
     *
     * @return static
     */
    public function href($href)
    {
        return $this->attribute('href', $href);
    }
	
    /**
     * @param string $value
     *
     * @return static
     */
    public function target($value)
    {
        return $this->attribute('target', $value);
    }
}
