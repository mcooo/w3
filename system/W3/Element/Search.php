<?php

namespace W3\Element;

use W3\Element;

/**
 * Search 帮手类
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */

class Search extends Element
{
    /**
     * 容器
     *
     * @access public
     * @var Element
     */
    public $container;
	
    public $input;

    public $submit;

    public $reset;

    /**
     * 实例化
     *
     * @access public
     * @param mixed $config 配置列表
     */
    public function __construct($name = NULL, $value = NULL)
    {
		$this->close(false)
		    ->tag('div')
			->addClass('input-group');
		
		$this->container = Element::make();

		/** 设置表单标签 */
		$this->input = Input::make($name, $value);

		$this->container->set($this->input);
		
		$html = '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>';
		
		$this->reset = A::make('javascript:;', $html)->addClass('reset')->hide();
		
		$this->container->set($this->reset);
		
        $html = '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>';	
		
		$this->submit = Button::make($html);
		
		$this->container->set($this->submit->type('submit'));

		$this->set($this->container);
    }

    /**
     * 设置表单项默认值
     *
     * @access protected
     * @param mixed $value 表单项默认值
     * @return void
     */
    public function placeholder($value) 
	{
		$this->input->attribute('placeholder', $value);
		$this->input->attribute('aria-label', $value);
		return $this;
    }

}

