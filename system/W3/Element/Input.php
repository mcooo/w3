<?php

namespace W3\Element;

/**
 * Input 帮手类
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */
class Input extends Group 
{
    /**
     * 构造函数
     *
     * @access public
     * @param string $name 表单输入项名称
     * @param mixed $value 表单默认值
     * @param array $options 选择项
     * @return void
     */
    public function __construct($name = NULL, $value = NULL)
    {
		$this->name = $name;

		$this
		    ->close(true)
		    ->tag('input')
			->addClass('form-control')
			->attribute('name', $name)
            ->attribute('type', 'text');
			
        /** 初始化表单值 */
        if (NULL !== $value) {
            $this->value($value);
        }
    }
	
    /**
     * 设置表单项默认值
     *
     * @access protected
     * @param mixed $value 表单项默认值
     * @return void
     */
    public function value($value) 
	{
		$this->value = $value;
        $this->attribute('value', htmlspecialchars($value));
    }
	
    /**
     * @param bool $checked
     *
     * @return static
     */
    public function checked($checked = true)
    {
        return $checked
            ? $this->attribute('checked', 'checked')
            : $this->removeAttribute('checked');
    }

    /**
     * @return static (!)
     */
    public function unchecked()
    {
        return $this->checked(false);
    }

    /**
     * @param bool $disabled
     *
     * @return static
     */
    public function disabled($disabled = true)
    {
        return $disabled
            ? $this->attribute('disabled', 'disabled')
            : $this->removeAttribute('disabled');
    }
	
    /**
     * @param bool $required
     *
     * @return static
     */
    public function required($required = true)
    {
        return $required
            ? $this->attribute('required')
            : $this->removeAttribute('required');
    }
	
    /**
     * @param string|null $name
     *
     * @return static
     */
    public function name($name)
    {
        return $this->attribute('name', $name);
    }
	
    /**
     * @param string|null $type
     *
     * @return static
     */
    public function type($type)
    {
        return $this->attribute('type', $type);
    }
	
    /**
     * @return static
     */
    public function autofocus()
    {
        return $this->attribute('autofocus');
    }

    /**
     * @param string|null $placeholder
     *
     * @return static
     */
    public function placeholder($placeholder)
    {
        return $this->attribute('placeholder', $placeholder);
    }
	
    /**
     * @return static
     */
    public function readonly()
    {
        return $this->attribute('readonly');
    }	
	
    /**
     * @return static
     */
    public function hidden()
    {
        return $this->type('hidden');
    }
}