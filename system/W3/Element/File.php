<?php

namespace W3\Element;

/**
 * File 帮手类
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */

class File extends Input
{
    const ACCEPT_AUDIO = 'audio/*';
    const ACCEPT_VIDEO = 'video/*';
    const ACCEPT_IMAGE = 'image/*';

    public $container;

    public $file;

    public $labelFor;

    public function __construct($name = NULL, $value = NULL)
    {
        parent::__construct($name, $value);

		$this->container = Div::make(null, 'input-group');
		
		$this->file = Input::make('file')->type('file');
		
		$svg = '<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path><polyline points="17 8 12 3 7 8"></polyline><line x1="12" y1="3" x2="12" y2="15"></line></svg>';
		
		$this->labelFor = Label::make($svg . __('上传'), 'btn btn-primary-soft btn-sm w3-upload');
		
		$this->labelFor->set($this->file);
		
		$this->set($this->labelFor);
		
		$this->wrap($this->container);

    }

    /**
     * @param string|null $name
     *
     * @return static
     */
    public function accept($type)
    {
        return $this->file->attribute('accept', $type);
    }

    /**
     * @return static
     */
    public function acceptAudio()
    {
        return $this->file->attribute('accept', self::ACCEPT_AUDIO);
    }

    /**
     * @return static
     */
    public function acceptVideo()
    {
        return $this->file->attribute('accept', self::ACCEPT_VIDEO);
    }

    /**
     * @return static
     */
    public function acceptImage()
    {
        return $this->file->attribute('accept', self::ACCEPT_IMAGE);
    }

    /**
     * @return static
     */
    public function multiple()
    {
        return $this->file->attribute('multiple');
    }
}
