﻿
// 支持连续操作 $submit.button(message).delay(1000).button('reset').delay(1000).location('http://xxxx');
$.fn.location = function(href) {
	return this.each(function() {
		var jthis = $(this);
		jthis.queue(function(next) {
			if (href) {
				window.location = href
			} else if (href == 0) {
				window.location.reload()
			}
			next()
		})
	})
}

// 支持连续操作 jsubmit.button(message).delay(1000).button('reset');
$.fn.button = function(status) {
	return this.each(function() {
		var jthis = $(this);
		jthis.queue(function(next) {
			var loading_text = jthis.data('loading-text');
			if (status == 'loading') {
				jthis.prop('disabled', true).addClass('disabled').data('default-text', jthis.text());
				jthis.html(loading_text)
			} else if (status == 'disabled') {
				jthis.prop('disabled', true).addClass('disabled')
			} else if (status == 'enable') {
				jthis.prop('disabled', false).removeClass('disabled')
			} else if (status == 'reset') {
				jthis.prop('disabled', false).removeClass('disabled');
				if (jthis.data('default-text')) {
					jthis.text(jthis.data('default-text'))
				}
			} else {
				jthis.text(status)
			}
			next()
		})
	})
};

// 表格选择插件
$.fn.tableSelect = function(options) {
	var table = this,
		s = $.extend({
			checkEl: null,
			rowEl: null,
			actionEl: null,
			checkClass: 'checked',
		}, options);
		
	$(this).on('click', 'tr td:first-child', function(e) {

		var t = $(this), el = $(s.checkEl, this);
			
		if (el.length > 0) {
		
			var	checked = el.prop('checked');
			el.prop('checked', !checked);

			if (checked) {
				t.parent().removeClass(s.checkClass)
			} else {
				t.parent().addClass(s.checkClass)
			}
		}
	});

	function selectAll(t) {
		var checked = t.data('selectall');
		if (!checked) {
			t.data("selectall", true);
			$(s.rowEl, table).each(function() {
				var t = $(this),
					el = $(s.checkEl, this).prop('checked', true);
				if (el.length > 0) {
					t.addClass('checked')
				}
			})
		} else {
			t.data("selectall", false);
			$(s.rowEl, table).each(function() {
				var t = $(this),
					el = $(s.checkEl, this).prop('checked', false);
				if (el.length > 0) {
					t.removeClass('checked')
				}
			})
		}
	}
	$(this).on('click', s.actionEl, function(e) {
		
		var t = $(this),
			tip = t.data('confirm'),
			href = t.attr('href'),
			name = t.data('name');
			
		if(name == 'selectall') { 
		    return selectAll(t), !1;
		}

		if(tip) {
		    var num = t.data('num'), deletetext = t.html();
		    if(!num) {
			    t.data('num', 1);
			    t.html(tip);
			    setTimeout(function(){
				    t.data('num', 0);
				    t.html(deletetext);

			    }, 5000);
			    return false;
		    }
		}

        return table.parents("form").attr("action", href).submit(), !1;
	})
};

(function($, undefined) {
	"use strict";
	$.ajaxPrefilter(function(options, origOptions, jqXHR) {
		if (options.iframe) {
			options.originalURL = options.url;
			return "iframe"
		}
	});
	$.ajaxTransport("iframe", function(options, origOptions, jqXHR) {
		var form = null,
			iframe = null,
			name = "iframe-" + $.now(),
			input = $(options.files);
		options.dataTypes.shift();
		options.data = origOptions.data;
		if (input.length) {
			form = $("<form enctype='multipart/form-data' method='post'></form>").hide().attr({
				action: options.originalURL,
				target: name
			});
			
			if (typeof(options.data) === "string" && options.data.length > 0) {
				$.error("data must not be serialized")
			}

            if (options.data) {
                $.each(options.data, function (key, value) {
                    $('<input type="hidden"/>').prop('name', key).val(value).appendTo(form);
                });
            }

			return {
				send: function(headers, completeCallback) {
					iframe = $("<iframe src='about:blank' name='" + name + "' id='" + name + "' style='display:none'></iframe>");
	                input.before(input.clone().val(''));
	                input.appendTo(form);
	                iframe.appendTo("body");
	                form.appendTo("body");
                    form.on('submit', function(e) {

                        //加载事件
                        iframe.on("load", function (e) {
                            var body = $(this).contents().find('body');
		                    completeCallback(200, "OK", {
                                html: body.html(),
				                text: body.text()}
                            );
		
                            setTimeout(function(){
		                        form.off("submit").remove();
		                        iframe.off("load").remove();				
		                    }, 100)
                        });	
                    }).submit();
			    },
			    abort: function() {
					if (iframe !== null) {
		                form.off("submit").remove();
		                iframe.off("load").remove();
					}
				}
			}
		}
	})
})(jQuery);

