<!DOCTYPE html>
<html>
<head>
    <meta charset="<?php $this->config('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="renderer" content="webkit">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title><?php $this->themeTitle(NULL, 'W3 - '); ?></title>
	<link rel="stylesheet" href="<?php $this->assetUrl('/css/admin.css'); ?>" media="screen"/>
    <?php $this->header(); ?>
</head>
<body class="body with-sidebar fixed-header" id="body">
<?php $this->menu()->to($menu);?>
    <header class="header">
	    <nav class="navbar">
	        <div class="navbar-brand">
		        <a href="<?php $this->adminUrl(); ?>" class="title">W3</a>
		    </div>
	        <div class="header-content">
		        <div class="header-items">
			        <div class="item">
					    <a class="drawer" data-open-menu><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg></a>
					    <h2 class="page-head-title"><?php $menu->title(); ?></h2>
                        <?php if($menu->addLink): ?><a class="btn btn-outline-primary btn-sm ml-3" href="<?php $menu->addLink(); ?>"><?php _e('新增'); ?></a><?php endif ?>
					</div> 
			        <div class="item other">
                        <a href="<?php $this->config('siteUrl'); ?>"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg></a>
				        <a href="<?php $this->adminUrl('user/profile'); ?>"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg></a>
				        <a class="logout" title="<?php _e('登出'); ?>" href="<?php $this->logoutUrl(); ?>"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-log-out"><path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path><polyline points="16 17 21 12 16 7"></polyline><line x1="21" y1="12" x2="9" y2="12"></line></svg></a>
                    </div>					
	            </div>
			</div>
	    </nav>
    </header>

	<main class="main">
	    <div class="main-backdrop"><div class="backdrop" data-open-menu></div></div>
	    <div class="sidebar">
		    <div class="sidebar-body">
			    <div class="sidebar-catalog">
                    <ul class="sidebar-accordion">
                        <?php while($menu->next()): ?>
	                    <li class="sidebar-menu-item<?php if($menu->current){ ?> sidebar-menu-open active<?php }; ?>">
					        <a class="sidebar-menu-link" href="javascript:;">
                                <span class="sidebar-menu-icon svg"><?php $menu->icon(); ?></span>
                                <span class="sidebar-menu-title"><?php $menu->name(); ?></span>
                                <span class="sidebar-menu-arrow"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polyline points="6 9 12 15 18 9"></polyline></svg></span>
                            </a>
						    <ul class="sidebar-menu-child">
	                        <?php foreach ($menu->child as $c){ ?>
							    <li class="sidebar-menu-item">
		                            <a class="sidebar-menu-link<?php if($c['current']){ ?> active<?php }; ?>" href="<?php echo $c['url']; ?>" title="<?php echo $c['title']; ?>">
                                        <span class="sidebar-menu-title"><?php echo $c['name']; ?></span>
                                    </a>
	                            </li>
						    <?php }; ?>	
	                        </ul>
					    </li>
                        <?php endwhile; ?>
                    </ul>
			    </div>
		    </div>
	    </div>

        <div class="content">
		    <div class="py-4 px-3 px-md-4">
                <div class="card mb-3 mb-md-4" id="content-layout">
				    <div class="card-body"><?php $this->layout(); ?></div>
                </div>
			</div>

            <footer class="footer mt-auto">
	            <div class="copyright text-muted"><?php _e('由 <a href="http://mcooo.com">%s</a> 强力驱动, 版本 %s', ['w3', $this->config('version', false)]); ?></div>
            </footer>
	    </div>
    </main>

<?php $this->footer(); ?>

<script type="text/javascript">

$(document).ready(function() {

    $('.header,.main-backdrop').on('click', '[data-open-menu]', function() {
        $('#body').toggleClass('with-sidebar');
    });

    var accordion = $('.sidebar-accordion'), pre;
    accordion.on('click', '> .sidebar-menu-item', function() {

		var t = $(this);
		
		if (t.hasClass('active'))
			return;
		
		if(pre === undefined) {
			pre = accordion.find('> .active');
		    pre.find(".sidebar-menu-child").css({'display': 'block'});
		}

        pre.find(".sidebar-menu-child").slideUp('normal');
        t.find('.sidebar-menu-child').stop(true,true).slideToggle("normal");
	
	    pre.removeClass('active');
	    t.addClass('active'); 

		pre = t;
    });
});
</script>
</body>
</html>