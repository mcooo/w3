<?php

namespace Module\Admin;

use Manager\Attach as Manager;
use W3\Html;
use W3\Exception;

!defined('W3_ROOT_DIR') AND exit;

/**
 * 标签编辑组件
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */
class Attach extends Manager
{
    /**
     * 初始化
     *
     * @access protected
     * @return void
     */
    protected function init()
    {
        # 必须为编辑员以上权限
        $this->auth->check('editor');
		
		# 定义变量默认数据
        $this->parameter([

			# 设置主体
			'main' => 'admin.attach',			
			
			# 分页大小
		    'pageSize'=>20,
			
			# 操作列表
		    'operate'=> [
			    'selectall'=>[
				    'name'=>__('全选')
				], 
				'delete'=>[
				    'name'=>__('删除'),
					'data-confirm'=>__('删除不可恢复，确定删除？'),
					'href'=>$this->adminUrl('attach/delete', false)
				]
			],
			
			# 操作列表
		    'tabs'=> []
			
		], true);
	}
	
    /**
     * 绑定动作
     *
     * @access public
     * @return void
     */
    public function action()
    {
		$this->on(!$this->request->action || $this->request->is('action=index'))->index();
		$this->on($this->request->is('action=edit'))->edit();
		$this->on($this->request->isPost() && $this->request->is('action=edit'))->update();
		$this->on($this->request->is('action=add'))->edit();
		$this->on($this->request->isPost() && $this->request->is('action=add'))->add();
		$this->on($this->request->isPost() && $this->request->is('action=delete'))->delete();
		
    }

    public function edit()
    {
        if ($this->request->aid) {

            /** 按tag查询 */
            $this->db
				->select()
		        ->from('table.attachs')
			    ->where('table.attachs.aid = ?', $this->request->filter('int')->aid)
			    ->limit(1)
			    ->fetch([$this, 'push']);

            if (!$this->have()) {
				throw new Exception(__('文件不存在'), 404);
            }
        }
		
        /** 更新模式 */
        if ($this->aid) {
			$this->parameter->title = __('编辑标签: %s', [$this->name]);
        } else {
			$this->parameter->title = __('增加标签');
		}
		
		/** 输出内容 */
		$this->layout->set($this->form(), 'body');
		
		$this->view();
    }
	
    public function index()
    {
        /** 按tag查询 */
        $select = $this->db
			->select()
		    ->from('table.attachs');

        /** 过滤标题 */
        if (NULL != ($keywords = $this->request->filter('search')->keywords)) {
            $args = [];
            $keywordsList = explode(' ', $keywords);
            $args[] = implode(' OR ', array_fill(0, count($keywordsList), 'table.attachs.name LIKE ?'));

            foreach ($keywordsList as $keyword) {
                $args[] = '%' . $keyword . '%';
            }

            call_user_func_array([$select, 'where'], $args);
        }

		/** 计算数目 */
		$total = $select->count();
		$page = $this->request->get('page', 1);
		$pageUrl = $this->request->makeUrl('page=%page%');

        /** 提交查询 */
        $select
		    ->order('table.attachs.aid', 'DESC')
            ->page($page, $this->parameter->pageSize)
			->get([$this, 'push']);

		$this->parameter([
			
			# 当前页数
			'page'         => $page,
			
			# 内容总数 
			'total'        => $total,
			
			# 页面Url 
			'pageUrl'      => $pageUrl
			
		]);

		# 版面布局
		$this->layout->set($this->tabs(false), 'top');
		$this->layout->set($this->tagList(), 'body');
		$this->layout->set($this->pagination(false), 'bottom');

		$this->view();
    }
	
    public function tagList()
    {
		$form = Html::form()
		    ->method('get')
			->addClass('mb-3');
		
		$keywords = Html::search('keywords', htmlspecialchars($this->request->keywords))->placeholder(__('请输入关键字'));
		
        if (!empty($_GET)){
			$keywords
			    ->reset
				->show()
				->title(__('取消筛选'))
				->href($this->adminUrl('page', false));
	    }
		
		$form->set($keywords);
		
		$html = $form->render();

        if ($this->have()) {
			$table = Html::table();
			$table->thead->preppend($this->operate(false));
			$table->thead->preppend('
                <colgroup>
				    <col width="3%" />
					<col width="3%" />
                    <col width="35%" />
					<col width="30%" />
                </colgroup>
			');
			$table->head([
			    '',
				'',
			    __('名称'),
				__('缩略名'),
			]);
			while ($this->next()){
			    $table->body([
				    Html::checkbox('aid', [$this->aid => '']),
					'<a class="badge badge-md badge-pill badge-primary-soft mr-2" href="'.$this->adminUrl('attach', 'tag=' . $this->aid, false).'">'.$this->contentsNum.'</a>',
				    '<a href="' .$this->adminUrl('attachs/edit', 'aid=' . $this->aid, false).'">'.$this->name.'</a>',
					$this->alias,
				]);
			}

			$html .= $table->wrap(Html::form()->removeClass('w3-form'))->render();
		}

		return $html;
	}

    public function add()
    {
		$form = $this->form();
			
		/** 验证格式 */
        if ($form->validate()) {
            $this->goBack();
        }

		/** 取出数据 */
        $tag = $this->request->from(array_keys($form->getInputs()));
		$tag['type'] = 'tag';

		$mid = $this->addTag($tag);
		
        $this->db
		    ->select()
		    ->from('table.attachs')
		    ->where('table.attachs.mid = ?', $mid)
			->fetch([$this, 'push']);
			
        /** 提示信息 */
        $this->notice(__('标签 %s 增加成功', [$this->name]));
		
		$this->redirect($this->adminUrl('tag', false));

	}

    public function update()
    {
		$form = $this->form();
			
		/** 验证格式 */
        if ($form->validate()) {
            $this->goBack();
        }
				
		/** 取出数据 */
        $tag = $this->request->from(array_keys($form->getInputs()));
		$tag['type'] = 'tag';
				
		$this->updateTag($this->mid, $tag);

		// 获取页面偏移的URL Query
        $count = $this->db
		    ->select(1)
		    ->from('table.metas')
			->where('table.metas.mid > ? and table.metas.type = ?', $this->mid, 'tag')
			->count() + 1;

        /** 提示信息 */
        $this->notice(__('标签 %s 更新成功', [$this->name]));
		
		$this->redirect($this->adminUrl('tag', 'page=' . ceil($count / $this->parameter->pageSize), false));
	}

    public function delete()
    {
        $mids = $this->request->filter('int')->getArray('mid');
        $deleteCount = 0;

        foreach ($mids as $mid) 
		{
		    if($this->deleteTag($mid)){ 
				$deleteCount++;
			}
        }
			
        /** 设置提示信息 */
        $this->notice(
		    $deleteCount > 0 
			? __('标签已经删除') 
			: __('没有标签被删除'), $deleteCount > 0 ? 'success' : 'notice'
		);
		
		$this->goBack();
	}

}
