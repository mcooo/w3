<?php

namespace Module\Admin;

use Manager\Options as Manager;
use W3\Html;

!defined('W3_ROOT_DIR') AND exit;

/**
 * 文章阅读设置组件
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */
class Reading extends Manager
{
    /**
     * 初始化
     *
     * @access protected
     * @return void
     */
    protected function init()
    {
		# 定义变量默认数据
        $this->parameter([

			# 设置主体
			'main' => 'admin.reading',	
			
		], true);		
		
        # 必须为管理员以上权限
        $this->auth->check('admin');
	}
	
    /**
     * 绑定动作
     *
     * @access public
     * @return void
     */
    public function action()
    {
		$this->on(!$this->request->action || $this->request->is('action=index'))->index();
		$this->on($this->request->isPost() && (!$this->request->action || $this->request->is('action=index')))->update();
    }	
	
    public function index()
    {
		/** 输出内容 */
		$this->layout->set($this->form(), 'body');

		$this->view();
    }
	
    /**
     * 输出表单结构
     *
     * @access public
     * @return Form
     */
    public function form()
    {
        /** 构建表格 */
        $form = Html::form();

		/** 按page查询 */
        $pages = $this->db
			->select()
		    ->from('table.contents')
			->where('table.contents.type = ?', 'page')
			->where('table.contents.status = ?', 'publish')
			->limit(1000)
			->get();

        /** 父级分类 */
        $options = [0 => __('不选择')];
        foreach ($pages as $page) {
            $options[$page['cid']] = $page['title'];
        }
        $front = Html::select('frontPage', $options, $this->config->frontPage)
		    ->label(__('站点首页'))
		    ->description(__('自定义首页功能.'));
        $form->addInput($front);

        /** 文章日期格式 */
        $postDateFormat = Html::text('postDateFormat', $this->config->postDateFormat)
		    ->label(__('文章日期格式'))
		    ->description(__('此格式用于指定显示在文章归档中的日期默认显示格式.') . '<br />'
            . __('在某些主题中这个格式可能不会生效, 因为主题作者可以自定义日期格式.') . '<br />'
            . __('请参考 <a href="http://www.php.net/manual/zh/function.date.php">PHP 日期格式写法</a>.'))
			->addRule('xss', __('请不要在日期格式中使用特殊字符'));
        $form->addInput($postDateFormat);

        /** 文章列表数目 */
        $postListSize = Html::text('postListSize', $this->config->postListSize)
		    ->label(__('文章列表数目'))
		    ->description(__('此数目用于指定显示在侧边栏中的文章列表数目.'))
			->addRule('int', __('请填入一个数字'));
        $form->addInput($postListSize);

        /** 每页文章数目 */
        $pageSize = Html::text('pageSize', $this->config->pageSize)
		    ->label(__('每页文章数目'))
		    ->description(__('此数目用于全站文章列表输出时每页显示的文章数目.'))
			->addRule('int', __('请填入一个数字'));
        $form->addInput($pageSize);

        /** 提交按钮 */
        $submit = Html::submit(__('保存设置'));
        $form->set($submit);
		
        return $form;
    }

    /**
     * 执行更新动作
     *
     * @access public
     * @return void
     */
    public function update()
    {
		$form = $this->form();

		/** 验证格式 */
        if ($form->validate()) {
            $this->goBack();
        }

        $settings = $this->request->from(array_keys($form->getInputs()));

        foreach ($settings as $name => $value) 
		{
            $this->updateOptions($name, $value);
        }
		
        $this->notice(__('设置已经保存'));
		
		$this->goBack();
    }
}
