<?php

namespace Module\Admin;

use W3\Util;
use Manager\User as Manager;
use W3\Html;

!defined('W3_ROOT_DIR') AND exit;

/**
 * 用户配置组件
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */
class Password extends Manager
{
    /**
     * 初始化
     *
     * @access protected
     * @return void
     */
    protected function init()
    {
		# 定义变量默认数据
        $this->parameter([

			# 设置主体
			'main' => 'admin.password',	
			
		], true);	
		
        # 必须为注册用户以上权限
        $this->auth->check('member');

	}
	
    /**
     * 执行函数
     *
     * @access public
     * @return void
     */
    public function execute()
    {
		$this->push($this->auth->row);
    }
	
    /**
     * 绑定动作
     *
     * @access public
     * @return void
     */
    public function action()
    {
		$this->on(!$this->request->action || $this->request->is('action=index'))->index();
		$this->on($this->request->isPost() && (!$this->request->action || $this->request->is('action=index')))->update();
    }
	
    public function index()
    {
		/** 输出内容 */
		$this->layout->set($this->form(), 'body');
		
		$this->view();
    }

    /**
     * 生成表单
     *
     * @access public
     * @return Form
     */
    public function form()
    {
        /** 构建表格 */
        $form = Html::form();

        /** 用户旧密码 */
        $oldpassword = Html::text('oldpassword')
		    ->label(__('原密码'));
        $form->addInput($oldpassword);

        /** 用户密码 */
        $password = Html::text('password')
		    ->label(__('新密码'))
		    ->description(__('建议使用特殊字符与字母、数字的混编样式,以增加系统安全性.'));
        $form->addInput($password);

        /** 用户密码确认 */
        $confirm = Html::text('confirm')
		    ->label(__('新密码确认'))
		    ->description(__('请确认你的密码, 与上面输入的密码保持一致.'));
        $form->addInput($confirm);

        /** 提交按钮 */
        $submit = Html::submit(__('更新密码'));
        $form->set($submit);

        // 判断原密码是否正确
		$form->register('checkPassword', function ($password){

            return $this->password == Util::passwordHash($password);
        });
		
        $oldpassword->addRule('required', __('必须填写旧密码'));
        $oldpassword->addRule([$this, 'checkPassword'], __('原密码错误'));
        $password->addRule('required', __('必须填写新密码'));
        $password->addRule('min', __('为了保证账户安全, 请输入至少六位的密码'), 6);
        $confirm->addRule('confirm', __('两次输入的密码不一致'), 'password');
		
        return $form;
    }

    /**
     * 更新密码
     *
     * @access public
     * @return void
     */
    public function update()
    {
		$form = $this->form();
		
		/** 验证表单 */
        if ($form->validate()) {
            $this->goBack();
        }

        /** 取出数据 */
        $user = $this->request->from(array_keys($form->getInputs()));
		
        /** 更新数据 */
        $this->updateUser($this->uid, $user);
		
		$this->db
			->select()
		    ->from('table.users')
		    ->where('table.users.uid = ?', $this->uid)
			->limit(1)
			->fetch([$this, 'push']);

        /** 提示信息 */
		$this->notice(__("密码已经成功修改"));
		
		$this->goBack();
    }
}
