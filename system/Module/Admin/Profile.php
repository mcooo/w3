<?php

namespace Module\Admin;

use Manager\User as Manager;
use W3\Html;

!defined('W3_ROOT_DIR') AND exit;

/**
 * 用户配置组件
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */
class Profile extends Manager
{
    /**
     * 初始化
     *
     * @access protected
     * @return void
     */
    protected function init()
    {
		# 定义变量默认数据
        $this->parameter([

			# 设置主体
			'main' => 'admin.profile',	
			
		], true);	
		
        # 必须为注册用户以上权限
		$this->auth->check('member');
	}
	
    /**
     * 执行函数
     *
     * @access public
     * @return void
     */
    public function execute()
    {
		$this->push($this->auth->row);
    }
	
    /**
     * 绑定动作
     *
     * @access public
     * @return void
     */
    public function action()
    {
		$this->on(!$this->request->action || $this->request->is('action=index'))->index();
		$this->on($this->request->isPost() && (!$this->request->action || $this->request->is('action=index')))->update();
    }
	
    public function index()
    {
		/** 输出内容 */
		$this->layout->set($this->form(), 'body');
		
		$this->view();
    }

    /**
     * 更新用户
     *
     * @access public
     * @return void
     */
    public function update()
    {
		$form = $this->form();
		
		/** 验证表单 */
        if ($form->validate()) {
            $this->goBack();
        }

        /** 取出数据 */
        $user = $this->request->from(array_keys($form->getInputs()));
        $user['nickName'] = empty($user['nickName']) ? $user['name'] : $user['nickName'];

        /** 更新数据 */
		$this->updateUser($this->uid, $user);

        /** 提示信息 */
        $this->notice(__('您的资料已经更新'));
		
		$this->goBack();
    }

    /**
     * 生成表单
     *
     * @access public
     * @param string $action 表单动作
     * @return Form
     */
    public function form()
    {
        /** 构建表格 */
        $form = Html::form();

        /** 电子邮箱地址 */
        $mail = Html::text('mail')
		    ->label(__('电子邮箱地址') . ' *')
		    ->description(__('电子邮箱地址将作为此用户的主要联系方式.') . '<br />' . __('请不要与系统中现有的电子邮箱地址重复.'));
        $form->addInput($mail);

        /** 用户昵称 */
        $nickName = Html::text('nickName')
		    ->label(__('用户昵称'))
		    ->description(__('用户昵称可以与用户名不同, 用于前台显示.') . '<br />' . __('如果你将此项留空, 将默认使用用户名.'));
        $form->addInput($nickName);

        /** 个人主页 */
        $url = Html::text('url')->label(__('个人主页'));
        $form->addInput($url);

        /** 签名 */
        $sign = Html::textarea('sign')
		    ->label(__('签名'))
			->style('min-height: 80px');
        $form->addInput($sign);

        /** 简介 */
        $intro = Html::textarea('intro')
		    ->label(__('简介'))
			->style('min-height: 120px');
        $form->addInput($intro);

        /** 用户主键 */
        $uid = Html::hidden('uid');
        $form->addInput($uid);

        /** 提交按钮 */
        $submit = Html::submit();
        $form->set($submit);

        $submit->value(__('编辑用户'));
        $nickName->value($this->nickName);
        $mail->value($this->mail);
        $uid->value($this->uid);
        $url->value($this->url);
        $sign->value($this->sign);
        $intro->value($this->intro);

        // 判断用户nickName是否存在
		$form->register('nickNameExists', function ($nickName, $uid){
            $select = $this->db
			    ->select('uid')
		        ->from('table.users')
			    ->where('table.users.nickName = ?', $nickName)
			    ->fetch();
			
			return empty($select) || $uid && $uid == $select['uid'];
        });

        // 判断用户mail是否存在
		$form->register('mailExists', function ($mail, $uid){
            $select = $this->db
			    ->select('uid')
		        ->from('table.users')
			    ->where('table.users.mail = ?', $mail)
			    ->fetch();
				
			return empty($select) || $uid && $uid == $select['uid'];
        });

        /** 给表单增加规则 */
        $nickName->addRule('nickNameExists', __('昵称已经存在'), $form->getParam('uid'));
        $nickName->addRule('xss', __('请不要在昵称中使用特殊字符'));
        $mail->addRule('required', __('必须填写电子邮箱'));
        $mail->addRule('mailExists', __('电子邮箱地址已经存在'), $form->getParam('uid'));
        $mail->addRule('email', __('电子邮箱格式错误'));
        $url->addRule('url', __('个人主页地址格式错误'));
        $sign->addRule('xss', __('请不要在签名中使用特殊字符'));
        $intro->addRule('xss', __('请不要在简介中使用特殊字符'));
		
        return $form;
    }
}
