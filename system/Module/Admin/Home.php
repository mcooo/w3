<?php

namespace Module\Admin;

use W3\Widget;
use W3\Util;
use W3\Timer;
use W3\Html;
use W3\Exception;

!defined('W3_ROOT_DIR') AND exit;

/**
 * Home 组件
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */
class Home extends Widget
{
    /**
     * 初始化
     *
     * @access protected
     * @return void
     */
    protected function init()
    {
		if(!$this->auth->hasLogin()) {
			
			throw new Exception(__('请求的地址不存在'), 404);

		}
		
		$stat = $this->widget('W3\Stat');
		
		$arrlist = [];
		$arrlist['start'] = [
		    'title'=>__('点击下面的链接快速开始'),
			'priority'=>20
		];
		$this->auth->check('contributor', true) && $arrlist['start'][] = Html::a($this->adminUrl('post/add', false), __('撰写新文章'))->addClass('btn btn-primary mb-2 mr-2');
		$this->auth->check('contributor', true) && $arrlist['start'][] = Html::a($this->adminUrl('page/add', false), __('创建新页面'))->addClass('btn btn-primary mb-2 mr-2');
        $this->auth->check('editor', true) && $arrlist['start'][] = Html::a($this->adminUrl('category/add', false), __('增加分类'))->addClass('btn btn-primary mb-2 mr-2');
		$this->auth->check('admin', true) && $arrlist['start'][] = Html::a($this->adminUrl('general', false), __('系统设置'))->addClass('btn btn-primary mb-2 mr-2');

		$arrlist['userInfo'] = [
		    'title'=>__('我的信息'),
			'priority'=>40,
			[__('登录帐号'), $this->auth->user('name') . '<b class="badge badge-md badge-pill badge-primary-soft ml-1">' . $this->auth->group() .'</b>'],
			[__('内容条数'), $stat->myPublishedPostNum],
			[__('评论条数'), $stat->myPublishedCommentsNum],
			[__('最后活动时间'), Timer::make($this->auth->user('activated'))->format('Y-m-d H:i:s')],
			[__('上次登录'), Timer::make($this->auth->user('logged'))->format('Y-m-d H:i:s')],
			[__('注册时间'), Timer::make($this->auth->user('created'))->format('Y-m-d H:i:s')],
		];
		
		if($this->auth->check('admin', true)){
		    
		    $arrlist['generalInfo'] = [
		        'title'=>__('综合统计'),
			    'priority'=>60,
			    [__('分类总数'), $stat->categoriesNum],
			    [__('文章内容'), $stat->publishedPostNum],
			    [__('附件数目'), $stat->attachsNum],
			    [__('用户数目'), $stat->usersNum],
			    [__('评论数目'), $stat->publishedCommentsNum],
		    ];
	    }
		
		if($this->auth->check('admin', true)){
			
			$isIniGet = function_exists('ini_get');
			
			$arrlist['osInfo'] = [
		        'title'=>__('服务器配置'),
				'priority'=>80,
			    [__('操作系统'), PHP_OS],
			    [__('环境软件'), $this->request->server('SERVER_SOFTWARE')],
			    [__('PHP'), PHP_VERSION],
			    [__('POST上传限制'), $isIniGet ? ini_get('post_max_size') : 'unknown'],
			    [__('文件上传限制'), $isIniGet ? ini_get('upload_max_filesize') : 'unknown'],
			    [__('内存限制'), $isIniGet ? ini_get('memory_limit') : 'unknown'],
			    [__('执行限制'), ($isIniGet ? ini_get('max_execution_time') : 'unknown') . ' s'],
			    [__('安全模式'), $isIniGet ? (ini_get('safe_mode') ? 'yes' : 'no') : 'unknown'],
			    [__('远程访问'), $isIniGet ? (ini_get('allow_url_fopen') ? 'yes' : 'no') : 'unknown'],
		    ];
		}

		$this->auth->check('admin', true) && $arrlist['systemInfo'] = [
		    'title'=>__('系统信息'),
			'priority'=>100,
			[__('程序版本'), 'W3<b class="badge badge-md badge-pill badge-primary-soft ml-1">v' .$this->config->version .'</b>'],
			[__('程序设计'), '<a href="http://mcooo.com" target="_blank">edikud</a>'],
			[__('界面设计'), '<a href="http://mcooo.com" target="_blank">edikud</a>'],
			[__('特别感谢'), '<a href="http://www.typecho.org" target="_blank" title="Typecho team">Typecho team</a> <a href="http://www.xiuno.com" target="_blank" title="Xiuno BBS创始人">老黄@xiuno</a> <a href="http://www.wangeditor.com/" target="_blank" title="W3内置wangEditor编辑器">wangEditor</a>'],
		];

		# 定义变量默认数据
        $this->parameter([

			# 设置主体
			'main' => 'admin.home',
		
            'homeList' => $arrlist 	
			
		], true);
	}

    /**
     * 绑定动作
     *
     * @access public
     * @return void
     */
    public function action()
    {
		$this->on(!$this->request->action || $this->request->is('action=index'))->index();
		
		if($this->request->action && !$this->request->is('action=index')){
			
			throw new Exception(__('请求的地址不存在'), 404);
		}
    }

    public function index()
    {
		/** 输出内容 */
		$this->layout->set($this->indexList(), 'body');
	
		$this->view();
    }
	
    public function indexList()
    {
		$arrlist = $this->parameter->homeList;

		# 按优先级对操作排序
        $arrlist = Util::arrayMultiSort($arrlist, 'priority');

		$list = '';
        foreach ($arrlist as $key=> $columns) 
		{
			if(!isset($columns[1])) continue;
			
			$list .= '<ul class="list-group mb-5">';
			$list .= '<li class="list-group-item active">'.$columns['title'].'</li>';
			
			unset($columns['priority']);
			unset($columns['title']);
			
			if(isset($columns[0]) && is_array($columns[0]))
			{
				foreach ($columns as $column) 
			    {
				    $list .= '<li class="list-group-item">';
				    $name = array_shift($column);
				    $list .= ($name ? '<small class="text-muted mr-2">'.$name.'</small>' : '') . implode('</li><li class="list-group-item">', $column) . '</li>';
				}
			}else{
				
				!empty($columns) && $list .= '<li class="list-group-item">' . implode('', $columns) . '</li>';
				
			}

			$list .= '</ul>';
        }

		return $list;	
    }

}
