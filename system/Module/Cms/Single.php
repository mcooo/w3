<?php

namespace Module\Cms;

use Base\Contents;

!defined('W3_ROOT_DIR') AND exit;

/**
 * 独立页处理
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */
class Single extends Contents
{
    /**
     * 初始化
     *
     * @access protected
     * @return void
     */
    protected function init()
    {
		if(isset($this->request->permalink)){
			
			# 导入路由配置
		    $current = $this->import($this->request->permalink);
			
			if(empty($current)){ 
			    throw new \Exception(__('请求的地址不存在'), 404);
			}
		}

		# select初始化
        $select = $this->db
		    ->select()
		    ->from('table.contents')
			->limit(1);			
			
        /** 如果是单篇文章或独立页面 */
        if (isset($this->request->cid)) {
            $select->where('table.contents.cid = ?', $this->request->filter('int')->cid);
        }

        /** 匹配缩略名 */
        if (isset($this->request->alias)) {
            $select->where('table.contents.alias = ?', $this->request->alias);
        }
		
        $select->get([$this, 'push']);

		# 定义变量默认数据
        $this->parameter([

			# 设置主体
			'main' => $this->type,
			
			# 分页大小
		    'pageSize' => 20,
			
			# 主题文件
            'themeFile' => $this->type,
			
			# 设置单一类型
            'single' => true
			
		], true);
	}

    public function execute()
    {
        if (!$this->have() 
            || (isset($this->request->category) && $this->category != $this->request->category )
            || (isset($this->request->directory) && $this->request->directory != $this->directory)) {

				throw new \Exception(__('请求的地址不存在'), 404);
        }

        /** 设置缩略名 */
        $this->parameter->alias = $this->alias;

        /** 设置模板 */
        if ($this->template) {
            /** 应用自定义模板 */
            $this->parameter->themeFile = $this->template;
        }

    }

}

