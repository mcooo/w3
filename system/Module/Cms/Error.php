<?php

namespace Module\Cms;

use W3\Widget;

!defined('W3_ROOT_DIR') AND exit;

/**
 * 404页面处理
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */
class Error extends Widget
{
    /**
     * 初始化
     *
     * @access protected
     * @return void
     */
    protected function init()
    {
		# 定义变量默认数据
        $this->parameter([

			# 设置主体
			'main' => '404',
			
		    # 设置标题
		    'title' => __('页面没找到'), 
			
			# 设置缩略名
			'alias' => '404', 
			
			# 主题文件
			'themeFile' => '404'
			
		], true);
	}
	
    public function execute()
    {
        /** 设置header */
        $this->response->code(404);
    }

}

