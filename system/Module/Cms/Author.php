<?php

namespace Module\Cms;

use Base\Contents;
use W3\Router;

!defined('W3_ROOT_DIR') AND exit;

/**
 * 处理作者
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */
class Author extends Contents
{
    /**
     * 初始化
     *
     * @access protected
     * @return void
     */
    protected function init()
    {
		# 定义变量默认数据
        $this->parameter([

			# 设置主体
			'main' => 'author',
			
			# 分页大小
		    'pageSize' => 20,
			
			# 主题文件
            'themeFile' => 'author',	
			
		], true);
	}
	
    public function execute()
    {
        $select = $this->db
            ->select()
		    ->from('table.users')
            ->limit(1);

        if (isset($this->request->uid)) {
            $select->where('uid = ?', $this->request->filter('int')->uid);
        }

        if ($this->request->name) {
            $select->where('name = ?', $this->request->filter('strip_tags')->name);
        }

        $user = $select->fetch();
        if (empty($user)) {
			throw new \Exception(__('请求的地址不存在'), 404);
        }

		/** 设置参数 */
		$this->request->setParams('uid', $user['uid']);
	
        $select = $this->db
		    ->select()
		    ->from('table.contents')
		    ->where('table.contents.type = ?', 'post')
			->where('table.contents.status = ?', 'publish')
            ->where('table.contents.uid = ?', $user['uid']);
	
	
        /** 计算数目 */
		$total = $select->count();
		$page = $this->request->get('page', 1);
		$pageUrl = Router::buildUrl('author_page', $user + ['page'=>'%page%']);
		
        $select 
		    ->order('table.contents.cid', 'DESC')
		    ->page($page, $this->parameter->pageSize)
		    ->get([$this, 'push']);

		$this->parameter([
		
		    # 设置关键词
		    'keywords' => $user['nickName'],
			
			# 设置描述
		    'description' => $user['nickName'],
			
			# 设置标题
		    'title' => $user['nickName'],
		
		    # 设置缩略名
		    'alias' => $user['name'],
			
			# 当前页数
			'page' => $page,
			
			# 内容总数
			'total' => $total,
			
			# 页面Url
			'pageUrl' => $pageUrl,

		]);
    }
}

