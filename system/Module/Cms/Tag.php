<?php

namespace Module\Cms;

use Base\Contents;
use W3\Router;
use W3\Exception;

!defined('W3_ROOT_DIR') AND exit;

/**
 * 处理标签
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 
 */
class Tag extends Contents
{
    /**
     * 初始化
     *
     * @access protected
     * @return void
     */
    protected function init()
    {
		# 定义变量默认数据
        $this->parameter([

			# 设置主体
			'main' => 'tag',
			
			# 分页大小
		    'pageSize' => 20,
			
			# 主题文件
            'themeFile' => 'tag',	
			
		], true);
	}

    public function execute()
    {
        $tagSelect = $this->db->select()
		    ->from('table.metas')
            ->where('type = ?', 'tag')
			->limit(1);

        if (isset($this->request->mid)) {
            $tagSelect->where('mid = ?', $this->request->filter('int')->mid);
        }

        if (isset($this->request->alias)) {
            $tagSelect->where('alias = ?', $this->request->alias);
        }
        $tag = $tagSelect->fetch([$this->widget('Category\Rows'), 'filter']);
        if (empty($tag)) {
			throw new Exception(__('标签不存在'), 404);
        }

		/** 设置参数 */
		$this->request->setParams('mid', $tag['mid']);

        /** 计算数目 */
		$total = $tag['count'];
		$page = $this->request->get('page', 1);
		
        /** 设置分页 */
		$tag = array_merge($tag, [
            'alias'  =>  urlencode($tag['alias'])
        ]);
		
		$pageUrl = Router::buildUrl('tag_page', $tag + ['page'=>'%page%']);

		$cids = $this->db->select('table.relate.cid')
		    ->from('table.relate')
		    ->where('table.relate.mid = ?', $tag['mid'])
		    ->page($page, $this->parameter->pageSize)
		    ->get();

		if($cids){
            $select = $this->db->select()->from('table.contents')
			    ->where('table.contents.cid IN ('.implode(',', array_column($cids, 'cid')).')')
			    ->order('table.contents.cid', 'DESC')
			    ->get([$this, 'push']);
		}

		$this->parameter([
		
		    # 设置关键词
		    'keywords' => $tag['name'],
			
			# 设置描述
		    'description' => $tag['description'],
			
			# 设置标题
		    'title' => $tag['name'],
			
			# 当前页数
			'page' => $page,
			
			# 内容总数
			'total' => $total, 
			
			# 页面Url
			'pageUrl' => $pageUrl,
			
			# 设置缩略名
		    'alias' => $tag['name']
			
		]);

    }

}

