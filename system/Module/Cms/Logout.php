<?php

namespace Module\Cms;

use W3\Widget;

!defined('W3_ROOT_DIR') AND exit;

/**
 * 登出组件
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */
class Logout extends Widget
{
    /**
     * 初始化
     *
     * @access protected
     * @return void
     */
    protected function init()
    {
		# 定义变量默认数据
        $this->parameter([

			# 设置主体
			'main' => 'logout',	
			
		], true);
	}
    /**
     * 初始化函数
     *
     * @access public
     * @return void
     */
    public function execute()
    {
		# 登出
        $this->auth->logout();
		
        /** 转向 */
		$this->goBack(null, $this->config->siteUrl);
    }
}
