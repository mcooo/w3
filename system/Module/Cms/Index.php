<?php

namespace Module\Cms;

use Base\Contents;
use W3\Router;

!defined('W3_ROOT_DIR') AND exit;

/**
 * 处理Index
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */
class Index extends Contents
{
    /**
     * 初始化
     *
     * @access protected
     * @return void
     */
    protected function init()
    {
		# 定义变量默认数据
        $this->parameter([

			# 设置主体
			'main' => 'index',
			
			# 分页大小
		    'pageSize' => $this->config->pageSize,
			
			# 主题文件
            'themeFile' => 'index',
			
            # 默认title
            'title' => $this->config->title,
			
            # 默认keywords
            'keywords' => $this->config->keywords,
			
            # 默认description
            'description' => $this->config->description
			
		], true);
		
        add_action('index.select', function ($widget, $select) 
		{
		    return $select
		        ->where('table.contents.type = ?', 'post')
			    ->where('table.contents.status = ?', 'publish');
        });
	}
	
    public function execute()
    {
		# select初始化
		$select = $this->select();
		
		/** 计算数目 */
		$total = $select->count();
		$page = $this->request->get('page', 1);
		$pageUrl = Router::buildUrl('index_page', ['page'=>'%page%']);
		
        $select
		    ->page($page, $this->parameter->pageSize)
		    ->order('table.contents.cid', 'DESC')
			->get([$this, 'push']);
	
		$this->parameter([
		    
			# 当前页数
			'page' => $page,
			
			# 内容总数
			'total' => $total,
			
			# 页面Url
			'pageUrl' => $pageUrl
			
		]);
    }
}

