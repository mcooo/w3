
<form method="post" action="?action=finish" id="form">
    <div class="meta"><span><?php _e('数据库设置'); ?></span></div>
    <ul class="w3-option">
        <li>
            <label for="dbAdapter" class="w3-label"><?php _e('数据库适配器'); ?></label>
            <select name="dbAdapter" id="dbAdapter">
                <?php if (_p('mysql')): ?><option value="mysql"<?php if('mysql' == $adapter): ?> selected="selected"<?php endif; ?>><?php _e('Pdo 驱动 Mysql 适配器') ?></option><?php endif; ?>
                <?php if (_p('sqlite')): ?><option value="sqlite"<?php if('sqlite' == $adapter): ?> selected="selected"<?php endif; ?>><?php _e('Pdo 驱动 SQLite 适配器') ?></option><?php endif; ?>
                <?php if (_p('pgsql')): ?><option value="pgsql"<?php if('pgsql' == $adapter): ?> selected="selected"<?php endif; ?>><?php _e('Pdo 驱动 PostgreSql 适配器') ?></option><?php endif; ?>
            </select>
            <p class="description"><?php _e('请根据您的数据库类型选择合适的适配器'); ?></p>
        </li>
        <li>
            <label class="w3-label" for="dbHost"><?php _e('数据库地址'); ?></label>
            <input type="text" class="text" name="dbHost" id="dbHost" value="<?php _v('dbHost', 'localhost'); ?>"/>
            <p class="description"><?php _e('您可能会使用 "%s"', ['localhost']); ?></p>
        </li>
        <li>
            <label class="w3-label" for="dbPort"><?php _e('数据库端口'); ?></label>
            <input type="text" class="text" name="dbPort" id="dbPort" value="<?php _v('dbPort', '3306'); ?>"/>
            <p class="description"><?php _e('如果您不知道此选项的意义, 请保留默认设置'); ?></p>
        </li>
        <li>
            <label class="w3-label" for="dbUser"><?php _e('数据库用户名'); ?></label>
            <input type="text" class="text" name="dbUser" id="dbUser" value="<?php _v('dbUser', 'root'); ?>" />
            <p class="description"><?php _e('您可能会使用 "%s"', ['root']); ?></p>
        </li>
        <li>
            <label class="w3-label" for="dbPassword"><?php _e('数据库密码'); ?></label>
            <input type="password" class="text" name="dbPassword" id="dbPassword" value="<?php _v('dbPassword'); ?>" />
        </li>
        <li>
            <label class="w3-label" for="dbName"><?php _e('数据库名'); ?></label>
            <input type="text" class="text" name="dbName" id="dbName" value="<?php _v('dbName', 'cms'); ?>" />
            <p class="description"><?php _e('请您指定数据库名称'); ?></p>
        </li>
        <li>
            <label class="w3-label" for="dbCharset"><?php _e('数据库编码'); ?></label>
            <select name="dbCharset" id="dbCharset">
                <option value="utf8"<?php if (_r('dbCharset') == 'utf8'): ?> selected<?php endif; ?>>utf8</option>
                <option value="utf8mb4"<?php if (_r('dbCharset') == 'utf8mb4'): ?> selected<?php endif; ?>>utf8mb4</option>
            </select>
            <p class="description"><?php _e('选择 utf8mb4 编码至少需要 MySQL 5.5.3 版本'); ?></p>
        </li>
        <li>
            <label class="w3-label" for="dbEngine"><?php _e('数据库引擎'); ?></label>
            <select name="dbEngine" id="dbEngine">
                <option value="MyISAM"<?php if (_r('dbEngine') == 'MyISAM'): ?> selected<?php endif; ?>>MyISAM</option>
                <option value="InnoDB"<?php if (_r('dbEngine') == 'InnoDB'): ?> selected<?php endif; ?>>InnoDB</option>
            </select>
        </li>
        <li>
            <label class="w3-label" for="dbPrefix"><?php _e('数据库前缀'); ?></label>
            <input type="text" class="text" name="dbPrefix" id="dbPrefix" value="<?php _v('dbPrefix', 'cl_'); ?>" />
            <p class="description"><?php _e('默认前缀是 "cl_"'); ?></p>
        </li>
    </ul>

    <script>
    var _select = document.getElementById('dbAdapter');
    _select.onchange = function() {
        setTimeout("window.location.href = 'install.php?action=start&dbAdapter=" + this.value + "'; ",0);
    }
    </script>
    <div class="meta"><span><?php _e('创建您的管理员帐号'); ?></span></div>
    <ul class="w3-option">
        <li>
            <label class="w3-label" for="userUrl"><?php _e('网站地址'); ?></label>
            <input type="text" name="userUrl" id="userUrl" class="text" value="<?php _v('userUrl', _u()); ?>" />
            <p class="description"><?php _e('这是程序自动匹配的网站路径, 如果不正确请修改它'); ?></p>
        </li>
        <li>
            <label class="w3-label" for="userName"><?php _e('用户名'); ?></label>
            <input type="text" name="userName" id="userName" class="text" value="<?php _v('userName', 'admin'); ?>" />
            <p class="description"><?php _e('请填写您的用户名'); ?></p>
        </li>
        <li>
            <label class="w3-label" for="userPassword"><?php _e('登录密码'); ?></label>
            <input type="text" name="userPassword" id="userPassword" class="text" value="<?php _v('userPassword', \W3\Util::randstring(5)); ?>" />
            <p class="description"><?php _e('请填写您的登录密码, 如果留空系统将为您随机生成一个'); ?></p>
        </li>
        <li>
            <label class="w3-label" for="userMail"><?php _e('邮件地址'); ?></label>
            <input type="text" name="userMail" id="userMail" class="text" value="<?php _v('userMail', 'webmaster@yourdomain.com'); ?>" />
            <p class="description"><?php _e('请填写一个您的常用邮箱'); ?></p>
        </li>
    </ul>

    <p class="submit"><button type="submit" class="btn btn-primary" id="submit"><?php _e('确认, 开始安装 &raquo;'); ?></button></p>
</form>
