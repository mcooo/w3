

    <h1><?php _e('安装成功'); ?></h1>
	<br>
        <div class="message">
            <?php if (isset($_REQUEST['userName']) && isset($_REQUEST['userPassword'])): ?>
                <?php _e('您的用户名是'); ?>: <strong class="mono"><?php echo htmlspecialchars(_r('userName')); ?></strong><br>
                <?php _e('您的密码是'); ?>: <strong class="mono"><?php echo htmlspecialchars(_r('userPassword')); ?></strong>
            <?php endif;?>
        </div>

        <div class="success">
            <p><?php _e('您可以将下面两个链接保存到您的收藏夹'); ?>:</p>
            <ul>
            <?php $loginUrl = './index.php/login'; ?>
            <li><a href="<?php echo $loginUrl; ?>" target="_blank"><?php _e('点击这里访问您的控制面板'); ?></a></li>
            <li><a href="./index.php" target="_blank"><?php _e('点击这里查看您的 Blog'); ?></a></li>
        </ul>
    </div>

    <p><?php _e('亲，请牢记以上信息，您可以登陆后台修改密码及网站设置。^_^'); ?></p>
    <p><?php _e('希望您能尽情享用 W3 带来的乐趣!'); ?></p>

