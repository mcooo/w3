
<?php $err = 0; ?>

    <div class="meta"><span><?php _e('网站运行环境检测'); ?></span></div>
	
	<table class="tb">
		<tr>
			<th width="200">检查项目</th>
			<th width="250">推荐配置</th>
			<th>当前配置</th>
		</tr>
		<tr>
			<td>服务器</td>
			<td>Apache/2.2.x-Linux</td>
			<td><?php echo trim(preg_replace(['#PHP\/[\d\.]+#', '#\([\w]+\)#'], '', $_SERVER['SERVER_SOFTWARE'])).'-'.PHP_OS;?></td>
		</tr>
		<tr>
			<td>PHP版本</td>
			<td>7.2.x</td>
			<td><?php
				if(version_compare(PHP_VERSION , '7.2')) {
					echo '<i>[√]</i>';
				}else{
					$err = 1;
					echo '<u>[×]</u> (您的PHP版本小于7.2, 无法使用本系统)';
				} ?></td>
		</tr>
		<tr>
			<td>上传限制</td>
			<td>2M</td>
			<td><?php echo function_exists('ini_get') && ini_get('file_uploads') ? ini_get('upload_max_filesize') : 'unknow'; ?></td>
		</tr>
		<tr>
			<td>磁盘空间</td>
			<td>10M+</td>
			<td><?php echo function_exists('disk_free_space') ? get_byte(disk_free_space(W3_ROOT_DIR)) : 'unknow'; ?></td>
		</tr>
		<!--<tr>
			<td>mysql扩展</td>
			<td>必须开启</td>
			<td><?php
				if(extension_loaded('mysql')) {
					echo '<i>开启[√]</i>';
				}else{
					//$err = 1;
					echo '<u>关闭[×]</u> (关闭将无法使用本系统)';
				} ?></td>
		</tr>-->
		<tr>
			<td>gd扩展</td>
			<td>建议开启</td>
			<td><?php
				$gd  = '';
				if(extension_loaded('gd')) {
					function_exists('imagepng') && $gd .= ' png';
					function_exists('imagejpeg') && $gd .= ' jpg';
					function_exists('imagegif') && $gd .= ' gif';
				}
				echo $gd ? '<i>开启[√]'.$gd.'</i>' : '<u>关闭[×]</u> (关闭将不支持缩略图、水印和验证码)';
			?></td>
		</tr>
		<tr>
			<td>allow_url_fopen</td>
			<td>建议开启</td>
			<td><?php echo ini_get('allow_url_fopen') ? '<i>开启[√]</i>' : '<u>关闭[×]</u> (关闭将不支持远程本地化，在线安装模板和插件)'; ?> </td>
		</tr>

        <?php $allows = ['glob', 'ctype_alnum', 'mb_substr', 'mb_strtoupper', 'mb_strlen', 'mb_strpos']; ?>
		
		<?php 
		    foreach ($allows as $func) 
			{ 
		        $allow = function_exists('glob');
		?>
		
		<tr>
			<td><?php echo $func ?>()</td>
			<td>必须开启</td>
			<td><?php
				if($allow) {
					echo '<i>开启[√]</i>';
				}else{
					$err = 1;
					echo '<u>关闭[×]</u> (关闭将无法使用本系统)';
				} ?></td>
		</tr>
		<?php } ?>
		
		<tr>
			<td>mb_regex_encoding()</td>
			<td>必须开启</td>
			<td><?php
				if(function_exists('mb_get_info') && function_exists('mb_regex_encoding')) {
					echo '<i>开启[√]</i>';
				}else{
					$err = 1;
					echo '<u>关闭[×]</u> (关闭将无法使用本系统)';
				} ?></td>
		</tr>
		
		
	</table>
	
	<div class="meta"><span><?php _e('目录可写状态'); ?></span></div>
	
	<table class="tb">
		<tr>
			<th width="200">目录名</th>
			<th width="250">需要状态</th>
			<th>当前状态</th>
		</tr>
		<?php
		echo '<tr><td>/</td><td>可写 (*nix系统 0777)</td><td>';
		if(is_writable(W3_ROOT_DIR)) {
			echo '<i>可写[√]</i>';
		}else{
			$err = 1;
			echo '<u>不可写[×]</u>';
		}
		echo '</td></tr>';

	    $dirs = [
		   'app/uploads/',
		   'app/plugins/'
	    ];
		
		foreach($dirs as $dir) {
			$ret = is_writable(W3_ROOT_DIR .$dir);

			echo '<tr><td>'.$dir.'*</td><td>可写 (*nix系统 0777)</td><td>';
			if(!$ret) {
				$err = 1;
				echo '<u>不可写[×]';
			}else{
				echo '<i>可写[√]</i>';
			}
			echo '</u></td></tr>';
		}
		?>
	</table>

		<p class="submit">
			<button class="btn btn-light" onclick="window.location.reload()"><?php echo _e('重新检测');?></button>
			<button class="btn btn-primary" onclick="window.location='install.php?action=start'" <?php echo $err ? 'disabled' : '';?>><?php echo _e('检测完毕, 继续安装 &raquo;');?></button>
		</p>
</div>