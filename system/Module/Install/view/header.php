<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml">
<head lang="zh-CN">
    <meta charset="<?php _e('UTF-8'); ?>" />
    <meta name="renderer" content="webkit">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title><?php _e('W3 安装程序'); ?></title>
    <link rel="stylesheet" type="text/css" href="system/Module/install/css/style.css" />
</head>
<body>
<div class="header">
    <h1 class="title"><?php _e('W3 安装程序'); ?></h1>
    <div class="nav">
        <div class="step <?php if (empty($action)) : ?> current<?php endif; ?>"><span>1</span><?php _e('欢迎使用'); ?></div>
        <div class="step <?php if ($action == 'env') : ?> current<?php endif; ?>"><span>2</span><?php _e('环境检测'); ?></div>
        <div class="step <?php if ($action == 'start') : ?> current<?php endif; ?>"><span>3</span><?php _e('开始安装'); ?></div>
        <div class="step <?php if ($action == 'finish') : ?> current<?php endif; ?>"><span>4</span><?php _e('安装成功'); ?></div>
    </div>

</div>
<div class="container mb-5">