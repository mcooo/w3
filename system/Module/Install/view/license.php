
    <form method="get" action="">
	    <input type="hidden" name="action" value="env">
        <h1><?php _e('欢迎使用 W3'); ?></h1>
		<div class="meta"><span><?php _e('许可及协议'); ?></span></div>
        <div class="licensing">

            <h3><?php _e('许可及协议'); ?></h3>

            <p><?php _e('W3 基于 <a href="http://www.gnu.org/copyleft/gpl.html">GPL</a> 协议发布, 我们允许用户在 GPL 协议许可的范围内使用, 拷贝, 修改和分发此程序.'); ?></p>
            <p><?php _e('在GPL许可的范围内, 您可以自由地将其用于商业以及非商业用途.'); ?></p>
            <p><?php _e('W3 软件由其社区提供支持, 核心开发团队负责维护程序日常开发工作以及新特性的制定.'); ?></p>
            <p><?php _e('如果您遇到使用上的问题, 程序中的 BUG, 以及期许的新功能, 欢迎您在社区中交流或者直接向我们贡献代码.'); ?></p>
            <p><?php _e('对于贡献突出者, 他的名字将出现在贡献者名单中.'); ?></p>


	        <p>警告：按照我国法律，在未取得相关资源（影片、动画、图书、音乐等）授权的情况下，请不要传播任何形式的相关资源（资源数据文件、种子文件、网盘文件、FTP 文件等）！否则后果自负。</p>
        </div>
        <p class="submit">
            <?php if (count($langs) > 1): ?>
                <select onchange="window.location.href='install.php?lang=' + this.value">
                    <?php foreach ($langs as $_lang): ?>
                    <option value="<?php echo $_lang; ?>"<?php if ($lang == $_lang): ?> selected<?php endif; ?>><?php echo $_lang; ?></option>
                    <?php endforeach; ?>
                </select>
            <?php endif; ?>
			<button type="submit" class="btn btn-primary"><?php _e('我准备好了, 开始下一步 &raquo;'); ?></button>
        </p>
    </form>
