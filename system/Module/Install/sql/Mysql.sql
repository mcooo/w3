
# 用户数据表
DROP TABLE IF EXISTS `{pre}users`;
CREATE TABLE `{pre}users` (
  uid int(11) unsigned NOT NULL AUTO_INCREMENT, # 用户编号
  `group` varchar(16) DEFAULT 'visitor', # 用户组 如果要屏蔽，调整用户组即可
  name char(32) NOT NULL DEFAULT '', # 不可以重复 用户名
  nickName char(32) NOT NULL DEFAULT '', # 用户昵称
  password char(32) NOT NULL DEFAULT '', # 密码
  salt varchar(64) NOT NULL DEFAULT '',  # 随机干扰字符，用来混淆密码
  mail char(40) NOT NULL DEFAULT '', # 不可以重复 邮箱
  mobile char(64) NOT NULL DEFAULT '', # 不可以重复 手机号 (短信验证后加密写入)
  url varchar(200) NOT NULL DEFAULT '', # 个人主页
  sign varchar(120) NOT NULL DEFAULT '', # 签名
  intro varchar(200) NOT NULL DEFAULT '', # 简介
  scores int(11) NOT NULL DEFAULT '0', # 积分
  ip int(11) unsigned NOT NULL DEFAULT '0', # 创建文章时用户ip ip2long()，主要用来清理
  created int(11) unsigned NOT NULL DEFAULT '0', # 创建时间 
  logged int(11) unsigned NOT NULL DEFAULT '0', # 最后登录时间
  activated int(11) unsigned NOT NULL DEFAULT '0', # 最后活动时间
  PRIMARY KEY (uid),
  KEY (name),
  KEY (mail),
  KEY (mobile)
) ENGINE= {engine} DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;


# 元数据表  分类/ 标签 / ....
DROP TABLE IF EXISTS {pre}metas;
CREATE TABLE {pre}metas (
  mid int(11) unsigned NOT NULL auto_increment,
  parent int(11) unsigned NOT NULL DEFAULT '0', # 分类上级
  name char(32) NOT NULL DEFAULT '', # 名称
  type char(32) NOT NULL DEFAULT '', # category / tag ...
  alias char(64) NOT NULL DEFAULT '', # 英文别名
  `order` int(11) unsigned NOT NULL DEFAULT '0', # 排序
  template varchar(32) NOT NULL DEFAULT '',
  `count` int(11) NOT NULL DEFAULT '0', # 数量
  description varchar(225) NOT NULL DEFAULT '', # SEO描述
  pic varchar(255) NOT NULL DEFAULT '',			# 图片地址
  PRIMARY KEY (mid),
  UNIQUE KEY (alias),
  KEY (type, name),
  KEY (`order`)
) ENGINE={engine} DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;


# 内容数据表
DROP TABLE IF EXISTS {pre}contents;
CREATE TABLE {pre}contents (
  cid int(11) unsigned NOT NULL auto_increment, # 文章id
  mids char(225) NOT NULL DEFAULT '', # 所属分类 逗号分隔
  title char(225) NOT NULL DEFAULT '',
  uid int(11) unsigned NOT NULL DEFAULT '0', # 用户id
  alias char(64) NOT NULL DEFAULT '', # 英文别名
  type char(32) NOT NULL DEFAULT 'post', #  post / page ...
  `status` char(16) default 'publish',  #  publish公开 / draft草稿 / waiting待审核 / hidden隐藏  / home ... 
  intro varchar(255) NOT NULL DEFAULT '',		# 内容摘要/简介
  pic varchar(255) NOT NULL DEFAULT '',			# 图片地址
  template varchar(32) NOT NULL DEFAULT '',
  text text, # 内容
  created int(11) unsigned NOT NULL DEFAULT '0', # 创建时间
  modified int(11) unsigned NOT NULL DEFAULT '0',  # 修改时间
  commentsNum int(11) unsigned NOT NULL DEFAULT '0', # 评论数   (状态通过的数量)
  allowComment tinyint(1) unsigned NOT NULL DEFAULT '0',  # 是否允许评论
  tagsJson text, # 标签 (json数组)
  imagesNum tinyint(6) NOT NULL DEFAULT '0', # 附件中包含的图片数
  filesNum tinyint(6) NOT NULL DEFAULT '0', # 附件中包含的文件数
  `order` int(11) unsigned NOT NULL DEFAULT '0', # 排序
  PRIMARY KEY (cid),
  UNIQUE KEY (alias), 
  KEY (type, `status`)
) ENGINE={engine} DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;


# 关系数据表
DROP TABLE IF EXISTS {pre}relate;
CREATE TABLE {pre}relate (
  mid int(11) unsigned NOT NULL DEFAULT '0', # 分类 id
  cid int(11) unsigned NOT NULL DEFAULT '0', # 文章id
  PRIMARY KEY (mid, cid)
) ENGINE={engine} DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;


# 评论数据表
DROP TABLE IF EXISTS {pre}comments;
CREATE TABLE {pre}comments (
  coid int(11) unsigned NOT NULL auto_increment,
  cid int(11) unsigned NOT NULL DEFAULT '0', # 文章id
  uid int(11) unsigned NOT NULL DEFAULT '0', # 用户id
  parent int(11) unsigned DEFAULT '0',
  ownerId int(11) unsigned DEFAULT '0',
  created int(11) unsigned NOT NULL DEFAULT '0', # 评论时间
  ip int(11) unsigned NOT NULL DEFAULT '0', # 评论时用户ip ip2long()
  text text, #
  type char(32) NOT NULL DEFAULT 'comment', #  comment ...
  `status` varchar(16) default 'approved',
  PRIMARY KEY (coid),
  KEY (cid),
  KEY (created)
) ENGINE={engine} DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;


# 文章附件表
DROP TABLE IF EXISTS {pre}attachs;
CREATE TABLE {pre}attachs (
  aid int(11) unsigned NOT NULL AUTO_INCREMENT,		# 附件ID
  uid int(11) unsigned NOT NULL DEFAULT '0',		# 用户UID
  id int(11) unsigned NOT NULL DEFAULT '0',		# 对应内容数据id
  type char(32) NOT NULL DEFAULT '',                # type: post / page / category ...  (对应内容数据表type)
  name char(80) NOT NULL DEFAULT '',		# 文件原名
  ext char(10) NOT NULL DEFAULT '',		# 文件扩展名
  size int(10) unsigned NOT NULL DEFAULT '0',	# 文件大小
  path char(150) NOT NULL DEFAULT '',		# 文件路径, 不包含URL前缀 upload_url
  created int(11) unsigned NOT NULL DEFAULT '0',	# 上传时间
  isImage tinyint(1) unsigned NOT NULL DEFAULT '0',	# 是否是图片 (1为图片，0为文件)
  PRIMARY KEY (aid),
  KEY (type, id, uid)      # 每个文章下多个附件
) ENGINE={engine} DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

# 全局选项
DROP TABLE IF EXISTS {pre}options;
CREATE TABLE {pre}options (
  name char(32) NOT NULL DEFAULT '',
  uid int(11) unsigned NOT NULL DEFAULT '0',		# 用户UID
  value text,
  PRIMARY KEY(name, uid)
) ENGINE={engine} DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
