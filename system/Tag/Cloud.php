<?php

namespace Tag;

use W3\Util;
use Base\Metas;

/**
 * 标签云组件
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */
class Cloud extends Metas
{
    /**
     * 初始化
     *
     * @access protected
     * @return void
     */
    protected function init()
    {
		# 定义变量默认数据
        $this->parameter([
			
		    'field' => '*',	
		    'sort' => 'count',
            'desc' => true,
			'offset' => 0,
			'limit' => 0,
			'ignoreZeroCount' => false
			
		], true);
	}
	
    /**
     * 入口函数
     *
     * @throws Db\Exception
     */
    public function execute()
    {
        $select = $this->select($this->parameter->field)
		    ->where('table.metas.type = ?', 'tag')
            ->order('table.metas.' . $this->parameter->sort, $this->parameter->desc ? 'DESC' : 'ASC');

        /** 忽略零数量 */
        if ($this->parameter->ignoreZeroCount) {
            $select->where('count > 0');
        }

        /** 总数限制 */
        if ($this->parameter->limit) {
            $select->limit($this->parameter->limit);
        }

        $select->get([$this, 'push']);
    }

    /**
     * 按分割数输出字符串
     *
     * @param mixed ...$args 需要输出的值
     */
    public function split(...$args)
    {
        array_unshift($args, $this->count);
        echo call_user_func_array([Util::class, 'splitByCount'], $args);
    }
}
