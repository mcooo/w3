<?php

namespace User;

use W3\Util;
use Base\Users;

/**
 * 当前登录用户
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */
class Rows extends Users
{
    /**
     * 初始化
     *
     * @access protected
     * @return void
     */
    protected function init()
    {
		# 定义变量默认数据
        $this->parameter([
			
		    'field' => '*',	
			'name' => '',
			'mail' => '',
			'mobile' => '',
			'uid' => 0,
		    'sort' => 'uid',
            'desc' => true,
			'offset' => 0,
			'limit' => 0
			
		], true);
	}
	
    /**
     * 执行函数,初始化数据
     *
     * @access public
     * @return void
     */
    public function execute()
    {
		$pass = false;
		$select = $this->db
			->select($this->parameter->field)
		    ->from('table.users');		
		
        if ($this->parameter->name) {
            $select->where('name = ?', $this->parameter->name);
			$pass = true;
        }
		
        if ($this->parameter->mail) {
            $select->where('mail = ?', $this->parameter->mail);
			$pass = true;
        }
		
        if ($this->parameter->mobile) {
            $select->where('mobile = ?', $this->parameter->mobile);
			$pass = true;
        }
		
		if($this->parameter->uid){
			if(FALSE !== strpos($this->parameter->uid, ',')){ 
	            $uidList = array_filter(explode(',', $this->parameter->uid), function($val){
	                return is_numeric($val);
	            });
				if($uidList){ 
				    $select->where('uid IN ('.substr( str_repeat( '?,', count( $uidList ) ), 0, - 1 ).')', $uidList);
				    $pass = true;
				} else {
				    $pass = false;
			    }
			}else{
		        $select->where('uid = ?', $this->parameter->uid);
				$pass = true;
			}
		}

        if ($this->parameter->limit > 0) {
            $select->limit($this->parameter->limit);
        }
		
        if ($this->parameter->offset > 0) {
            $select->offset($this->parameter->offset);
        }
		
        if($pass){
			if(isset($uidList)){
			    $list = Util::arrayChangeKey($select->get(), 'mid');
                foreach ($uidList as $v) {
                    isset($list[$v]) && $this->push($list[$v]);
                }
			} else {
			    if($this->parameter->sort){
				    $select->order('table.users.' . $this->parameter->sort, $this->parameter->desc ? 'DESC' : 'ASC');
			    } 
				$select->get([$this, 'push']);
			}
        }
    }
}