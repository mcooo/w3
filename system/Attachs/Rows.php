<?php

namespace Attachs;

use Base\Attachs;

/**
 * 文章相关文件组件
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */
class Rows extends Attachs
{
    /**
     * 初始化
     *
     * @access protected
     * @return void
     */
    protected function init()
    {
		# 定义变量默认数据
        $this->parameter([
		
		    'field' => '*',	
			'uid' => 0,
			'cid' => 0,
		    'sort' => 'aid',
            'desc' => true,
			'offset' => 0,
			'limit' => 0
			
		], true);
	}
	
    /**
     * 执行函数,初始化数据
     *
     * @access public
     * @return void
     */
    public function execute()
    {
		$select = $this->select($this->parameter->field);
		
		if($this->parameter->cid){
		    $select->where('table.attachs.cid = ?', $this->parameter->cid);
		}
		
		if($this->parameter->uid){
			$select->where('table.attachs.uid = ?', $this->parameter->uid);
		}

        if ($this->parameter->limit > 0) {
            $select->limit($this->parameter->limit);
        }

        if ($this->parameter->offset > 0) {
            $select->offset($this->parameter->offset);
        }

	    if($this->parameter->sort){
			$select->order('table.attachs.' . $this->parameter->sort, $this->parameter->desc ? 'DESC' : 'ASC');
		}
		
		$select->get([$this, 'push']);
    }
}
