<?php

namespace Contents\Related;

use Base\Contents;

/**
 * 相关内容组件(根据作者关联)
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */
class Author extends Contents
{
	
    /**
     * 初始化
     *
     * @access protected
     * @return void
     */
    protected function init()
    {
		# 定义变量默认数据
        $this->parameter([
		
		    'field' => '*',
			'type' => '',
			'uid' => 0,
			'cid' => 0,
		    'sort' => 'cid',
            'desc' => true,
			'limit' => 5
			
		], true);
	}
	
    /**
     * 执行函数,初始化数据
     *
     * @access public
     * @return void
     */
    public function execute()
    {
        if ($this->parameter->uid) {

			$this->select($this->parameter->field)
			->where('table.contents.type = ?', $this->parameter->type)
			->where('table.contents.uid = ?', $this->parameter->uid)
			->where('table.contents.status = ?', 'publish')
			->where('table.contents.cid <> ?', $this->parameter->cid)
			->order('table.contents.' . $this->parameter->sort, $this->parameter->desc ? 'DESC' : 'ASC');
            ->limit($this->parameter->limit)
            ->get([$this, 'push']);
        }
    }
}
