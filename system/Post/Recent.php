<?php

namespace Post;

use Base\Contents;
use W3\Util;

/**
 * 最新文章
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */
class W3_Widget_Recent_Post extends W3_Widget_Contents
{
    /**
     * 初始化
     *
     * @access protected
     * @return void
     */
    protected function init()
    {
		# 定义变量默认数据
        $this->parameter([
		
		    'field' => '*',	
			'alias' => '',
			'uid' => 0,
			'mid' => 0,
			'cid' => 0,
		    'sort' => 'cid',
            'desc' => true,
			'offset' => 0,
			'limit' => $this->config->postListSize
			
		], true);
	}
	
    /**
     * 执行函数
     *
     * @access public
     * @return void
     */
    public function execute()
    {
        $this->parameter(['pageSize' => $this->config->postListSize], true);

        $this->db->select()->from('table.contents')
        ->where('table.contents.type = ?', 'post')
        ->limit($this->parameter->pageSize)
		->order('table.contents.cid', 'DESC')
		->get([$this, 'push']);
    }
}
