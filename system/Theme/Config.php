<?php

namespace Theme;

use W3\Widget;
use W3\Element\Form;
use W3\Html;

/**
 * 皮肤配置组件
 *
 * @author edikud
 * @date 2022/10/22
 * @copyright Copyright (c) 2022 W3 (http://www.mcooo.com)
 * @license GNU General Public License 2.0
 */
class Config extends Widget
{
    /**
     * 绑定动作
     *
     * @access public
     * @return void
     */
    public function execute()
    {
        $this->auth->check('admin');
        if (!$this->isExists()) {
			throw new \Exception(__('外观配置功能不存在'), 404);
        }
    }

    /**
     * 配置功能是否存在
     * 
     * @access public
     * @return boolean
     */
    public function isExists(): bool
    {
        $configFile = $this->themeFile('functions.php');
  
        if (file_exists($configFile)) {
            require_once $configFile;
            
            if (function_exists('themeConfig')) {
                return true;
            }
        }
        
        return false;
    }

    /**
     * 配置外观
     *
     * @access public
     * @return Form
     */
    public function form(): Form
    {
        $form = Html::form();
        themeConfig($form);
        $inputs = $form->getInputs();
        
        if (!empty($inputs)) {
            foreach ($inputs as $key => $val)
			{
                $form->getInput($key)->value($this->config->{$key});
            }
        }

        $submit = Html::submit(__('保存设置'));
        $form->set($submit);
        return $form;
    }
}
